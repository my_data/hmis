@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-2">
                @include('partials.sidebar')
            </div>
            <div class="col-md-10">
                <div class="panel panel-default">
                    <div class="panel-heading">Laboratory Tests</div>

                    <div class="panel-body">
                        @if($assigndrugs->count())
                            <table class="table table-bordered table-hover">
                                <thead>
                                <tr>
                                    <th>SN</th>
                                    <th>DRUG NAME</th>
                                    <th>DOSE</th>
                                    <th>DATE OF TREATMENT</th>
                                    <th>DESCRIPTION</th>
                                    <th>CONDUCT</th>
                                </tr>
                                </thead>
                                <tbody>
                                @php $i=1; @endphp
                                @foreach($assigndrugs as $assigndrug)
                                    <tr>
                                        <td>{{ $i++ }}.</td>
                                        <td>{{ $assigndrug->name }}</td>
                                        <td>
                                            {{ $assigndrug->dose }}
                                        </td>
                                        <td>{{ $assigndrug->created_at }}</td>
    <td><a href="/pharmace/patient/{{Request::segment(3)}}/prescription/{{Request::segment(5)}}/drug/{{$assigndrug->medicine_id}}/description"><span class="fa fa-eye">&nbsp;view</span></a></td>
                                        <td>
                                        @if($assigndrug->done==false)
            <form action="/pharmace/patient/{{Request::segment(3)}}/prescription/{{Request::segment(5)}}/drug/{{$assigndrug->medicine_id}}/assign" method="POST">
                                        {{csrf_field()}}
                                        <button type="submit" name="assign" id="assign" class="btn btn-primary">Assign</button>
                                        </form>
                                        @else
                                        <span class="badge">Done</span>
                                        @endif
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        @else
                            <div class="alert alert-info">
                                No any drugs assigned
                            </div>
                        @endif
                        <a href="/pharmace/patient/{{Request::segment(3)}}/prescriptions/" class="fa fa-arrow-left">Back</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
