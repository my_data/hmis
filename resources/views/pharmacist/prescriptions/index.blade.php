@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-2">
                @include('partials.sidebar')
            </div>
            <div class="col-md-10">
                <div class="panel panel-default">
                    <div class="panel-heading">Prescriptions</div>

                    <div class="panel-body">
                        @if($patients->count())
                            <table class="table table-bordered table-hover">
                                <thead>
                                <tr>
                                    <th>PNO.</th>
                                    <th>FULL NAME</th>
                                    <th>REGISTED</th>
                                </tr>
                                </thead>
                                <tbody>
                           
                                @foreach($patients as $patient)

                                    <tr>
                                       
                                 
                                                <td>
                                                @php 
                                                $id= $patient->id;
                                                $endstr = substr($id,-0);
                                                if(strlen($endstr) % 2 != 0)
                                                $endstr=$endstr."0";
                                                $chunks = str_split($endstr, 2);

                                                $result = implode('-', $chunks);
                                                if($id <100 )
                                                 $result = "00-00-".$result;
                                                 elseif($id >=100 && $id < 9999)
                                                $result = "00-".$result;
                                                @endphp
                                                {{ $result }}
                                            </td>
                                                   <td>
                                            <a href="/pharmace/patient/{{$patient->id}}/prescriptions">
                                                {{ $patient->first_name }}
                                                {{ $patient->middle_name }}
                                                {{ $patient->last_name }}
                                            </a>
                                        </td>
                                        <td>{{$patient->created_at->toDayDateTimestring()}}</td>
                                    </tr>

                                @endforeach
                                </tbody>
                            </table>
                        @else
                            <div class="alert alert-info">
                                No prescriptions
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
