@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-2">
                @include('partials.sidebar')
            </div>
            <div class="col-md-10">
                <div class="panel panel-default">
                    <div class="panel-heading"><h3>Medication descriptions</h3></div>

                    <div class="panel-body">
                        @if($assigndrug->description)
                        <textarea disabled="true" class="form-control" name="" id="" cols="30" rows="10">{{$assigndrug->description}}</textarea>
                        @else
                            <div class="alert alert-info">
                                No description
                            </div>
                        @endif
                           <a class="fa fa-arrow-left" href="../..">Back</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
