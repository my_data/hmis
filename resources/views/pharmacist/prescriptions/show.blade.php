@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-2">
                @include('partials.sidebar')
            </div>
            <div class="col-md-10">
                <div class="panel panel-default">
                    <div class="panel-heading">Treatments</div>

                    <div class="panel-body">
                        @if($prescriptions->count())
                            <table class="table table-bordered table-hover">
                                <thead>
                                <tr>
                                    <th>SN</th>
                                    <th>TREATMENT CODE</th>
                                    <th>DOCTOR ID</th>
                                    <th>DATE</th>
                                    <th>ACTION</th>
                                </tr>
                                </thead>
                                <tbody>
                                @php $i=1; @endphp
                                @foreach($prescriptions as $prescription)
                                    <tr>
                                        <td>{{$i++}}</td>
                                        <td>{{ $prescription->id }}</td>
                                        <td>
                                            {{ $prescription->doctor_id }}
                                        </td>
                                        <td>{{ $prescription->created_at->toDateTimeString() }}</td>
                                        <td>
                                            <a href="/pharmace/patient/{{$patient->id}}/prescription/{{$prescription->id}}">
                                                View
                                            </a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        @else
                            <div class="alert alert-info">
                                No Laboratory tests to conduct
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
