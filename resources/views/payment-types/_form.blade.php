<div class="form-group">
    <label for="name" class="col-sm-2 control-label">Payment Type</label>
    <div class="col-sm-10">
        <input type="text" name="name" id="name" value="{{ $paymentType->name or old('name')}}" class="form-control">
    </div>
</div>

<div class="form-group">
    <label for="amount" class="col-sm-2 control-label">Amount</label>
    <div class="col-sm-10">
        <input type="number" name="amount" id="amount" value="{{ $paymentType->amount or old('amount') }}"
               class="form-control">
    </div>
</div>


