@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-2">
                @include('partials.sidebar')
            </div>
            <div class="col-md-10">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">New System user</h3>
                    </div>

                    <div class="panel-body">
                        @include('errors.list')

                        <form style="width:800px;" action="{{ route('users.store') }}" method="POST" class="form-horizontal" role="form">
                            {{ csrf_field() }}

                            @include('users._form')


                            <div class="form-group">
                                <div class="col-sm-4 col-sm-offset-4">
                                    <button type="submit" class="btn btn-primary">Register</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
