@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-2">
                @include('partials.sidebar')
            </div>
            <div class="col-md-10">

                <div class="btn-group btn-group-justified">
                    <a href="{{ route('users.index') }}" class="btn btn-primary">All</a>
                    <a href="{{ route('users.administrators') }}" class="btn btn-primary">Administrators</a>
                    <a href="{{ route('users.doctors') }}" class="btn btn-primary">Doctors</a>
                    <a href="{{ route('users.pharmacists') }}" class="btn btn-primary">Pharmacists</a>
                    <a href="{{ route('users.laboratory-attendants') }}" class="btn btn-primary">Laboratory Attendants</a>
                    <a href="{{ route('users.receptionists') }}" class="btn btn-primary">Receptionists</a>
                    <a href="{{ route('users.imaging-tech') }}" class="btn btn-primary">imaging-tech</a>

                    <a href="{{ route('users.patients') }}" class="btn btn-primary">Patients</a>                    
                </div>
                <br>

                <div class="panel panel-default">
                    <div class="panel-heading">
                        <a href="{{ route('users.create') }}" class="btn btn-primary">Create New</a>
                    </div>

                    <div class="panel-body">
                        @if($users->count())
                            <table id="example" class="table table-bordered table-striped table-hover">
                                <thead>
                                <tr>
                                    <th class="text-right">#</th>
                                    <th>Full name</th>
                                    <th>Email</th>
                                    <th>Phone number</th>
                                    <th>Gender</th>
                                    <th>Roles</th>
                                    <th>Edit</th>
                                    <th>Delete</th>
                                </tr>
                                </thead>
                                <tbody>
                                @php
                                    $i = 1;
                                @endphp
                                @foreach($users as $user)
                                    <tr>
                                        <td class="text-right">{{ $i++ }}.</td>
                                        <td>
                                            <a href="/users/{{ $user->id }}">
                                                {{ $user->first_name }} {{ $user->middle_name }} {{ $user->last_name }}
                                            </a>
                                        </td>
                                        <td>{{ $user->email }}</td>
                                        <td>{{ $user->phone }}</td>
                                        <td>{{ $user->gender }}</td>
                                        <td>
                                            @if($user->roles()->count())
                                                @foreach($user->roles as $role)
                                                    <span class="badge">{{ $role->name }}</span>
                                                @endforeach
                                            @endif
                                        </td>
                                        <td>
                                            <a href="/users/{{ $user->id }}/edit"
                                               class="btn btn-primary btn-sm">Edit</a>
                                        </td>
                                        <td>
                                            <a class="btn btn-danger btn-sm" data-toggle="modal"
                                               href='#modal-id'><i class="fa fa-trash"></i> Delete</a>
                                            <div class="modal fade" id="modal-id">
                                                <div class="modal-dialog">
                                                    <form method="POST" action="/users/{{ $user->id }}">
                                                        {{ csrf_field() }}
                                                        {{ method_field('DELETE') }}

                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <button type="button" class="close" data-dismiss="modal"
                                                                        aria-hidden="true">&times;</button>
                                                                <h4 class="modal-title">
                                                                    Delete {{ $user->first_name }} {{ $user->middle_name }} {{ $user->last_name }}</h4>
                                                            </div>
                                                            <div class="modal-body">
                                                                Delete
                                                                permanently {{ $user->first_name }} {{ $user->middle_name }} {{ $user->last_name }}
                                                                ?
                                                            </div>
                                                            <div class="modal-footer">
                                                                <button type="button" class="btn btn-default"
                                                                        data-dismiss="modal">Cancel
                                                                </button>
                                                                <button type="submit" class="btn btn-danger"><i
                                                                            class="fa fa-trash"></i> Delete
                                                                </button>
                                                            </div>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        @else
                            <div class="text-center">
                                <a href="{{ route('users.create') }}" class="btn btn-primary">Create New</a>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
