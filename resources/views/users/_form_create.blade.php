<div class="form-group">
    <label for="first_name" class="col-sm-3 col-sm-offset-1 control-label">First Name (Required)</label>
    <div class="col-sm-6">
        <input type="text" name="first_name" id="first_name" value="{{  old('first_name') }}"
               class="form-control">
    </div>
</div>
<div class="form-group">
    <label for="middle_name" class="col-sm-3 col-sm-offset-1 control-label">Middle Name (Option)</label>
    <div class="col-sm-6">
        <input type="text" name="middle_name" id="middle_name" value="{{ old('middle_name') }}"
               class="form-control">
    </div>
</div>
<div class="form-group">
    <label for="last_name" class="col-sm-3 col-sm-offset-1 control-label">Last Name (Required)</label>
    <div class="col-sm-6">
        <input type="text" name="last_name" id="last_name" value="{{ old('last_name')}}"
               class="form-control">
    </div>
</div>
<div class="form-group">
    <label for="role_id" class="col-sm-3 col-sm-offset-1 control-label">Role (Required)</label>
    <div class="col-sm-6">
        <select name="role_id" id="role_id" class="form-control">
            <option value="">-Select-</option>
            @foreach($roles as $role)
                <option value="{{ $role->id }}">{{ $role->name }}</option>
            @endforeach
        </select>
    </div>
</div>
<div class="form-group">
    <label for="gender" class="col-sm-3 col-sm-offset-1 control-label">Gender (Required)</label>
    <div class="col-sm-6">
        <div class="radio">
            <label>
                <input type="radio" name="gender"  id="gender" 
                       value="Female" {{ isset($user) ? $user->gender == 'Female' ? 'checked' : '' : '' }}>
                Female
            </label>
        </div>
        <div class="radio">
            <label>
                <input type="radio" name="gender" id="gender"
                       value="Male" {{ isset($user) ? $user->gender == 'Male' ? 'checked' : '' : '' }}>
                Male
            </label>
        </div>
    </div>
</div>

<div class="form-group">
    <label for="dob" class="col-sm-3 col-sm-offset-1 control-label">Date of Birth (Option)</label>
    <div class="col-sm-6">
        <input type="date" name="dob" id="dob" value="{{ isset($user->dob) ? $user->dob->format('Y-m-d') : '' }}"
               class="form-control">
    </div>
</div>

<div class="form-group">
    <label for="phone" class="col-sm-3 col-sm-offset-1 control-label">Phone (Option)</label>
    <div class="col-sm-6">
        <input type="tel" name="phone" id="phone" value="{{ old('phone')}}" class="form-control">
    </div>
</div>

<div class="form-group">
    <label for="email" class="col-sm-3 col-sm-offset-1 control-label">Email (Option)</label>
    <div class="col-sm-6">
        <input type="email" name="email" id="email" value="{{ old('email')}}" class="form-control">
    </div>
</div>

<div class="form-group">
    <label for="address" class="col-sm-3 col-sm-offset-1 control-label">Address (Option)</label>
    <div class="col-sm-6">
        <input type="text" name="address" id="address" value="{{old('address') }}"
               class="form-control">
    </div>
</div>
