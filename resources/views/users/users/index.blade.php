@extends('layouts.app2')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-2">
                @include('partials.sidebar')
            </div>
            <div class="col-md-10">

                <div class="btn-group btn-group-justified">
                    <a href="{{ route('users.index') }}" class="btn btn-primary">All</a>
                    <a href="{{ route('users.administrators') }}" class="btn btn-primary">Administrators</a>
                    <a href="{{ route('users.doctors') }}" class="btn btn-primary">Doctors</a>
                    <a href="{{ route('users.pharmacists') }}" class="btn btn-primary">Pharmacists</a>
                    <a href="{{ route('users.laboratory-attendants') }}" class="btn btn-primary">Laboratory Attendants</a>
                    <a href="{{ route('users.receptionists') }}" class="btn btn-primary">Receptionists</a>
                    <a href="{{ route('users.imaging-tech') }}" class="btn btn-primary">imaging-tech</a>

                    <a href="{{ route('users.patients') }}" class="btn btn-primary">Patients</a>                    
                </div>
                <br>

                <div class="panel panel-default">
                    <div class="panel-heading">
                        <a href="{{ route('users.create') }}" class="btn btn-primary">Create New</a>
                    </div>

                    <div class="panel-body">

                            <table id="userTable" class="table table-bordered table-striped table-hover" width="100%">
                                <thead>
                                <tr>
                                    <th class="text-right">#</th>
                                    <th>Full name</th>
                                    <th>Email</th>
                                    <th>Phone number</th>
                                    <th>Gender</th>
                                    <th>Roles</th>
                                    <th>Edit</th>
                                    <th>Delete</th>
                                </tr>
                                </thead>
                                <tbody>

                                </tbody>
                            </table>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    <script type="text/javascript">
        var userTable = $('#userTable').DataTable({
            processing: true,
            serverSide: true,
            ajax: '{{route('users.get-users')}}',
            columns: [
                {data: 'id', name: 'id'},
                {data: 'full_name', name: 'users.last_name'},
                {data: 'email', name: 'users.email'},
                {data: 'phone', name: 'users.phone'},
                {data: 'gender', name: 'users.gender'},
                {data: 'roles', name: 'roles',orderable: false, searchable: false},
                //{data: 'role', name: 'role', orderable: false, searchable: false},
                {data: 'action_edit', name: 'action_edit', orderable: false, searchable: false},
                {data: 'action_delete', name: 'action_delete', orderable: false, searchable: false}
            ],
            select: {
                style: 'multi'
            }
        })

    </script>

@endsection
