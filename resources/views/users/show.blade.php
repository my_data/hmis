@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-2">
                @include('partials.sidebar')
            </div>
            <div class="col-md-10">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">
                            {{ $user->name }}
                        </h3>
                    </div>

                    <div class="panel-body">
                        <table class="table table-bordered table-hover">
                            <tbody>
                            <tr>
                                <th>First Name</th>
                                <td>
                                    {{ $user->first_name }}
                                </td>
                            </tr>
                            <tr>
                                <th>Middle Name</th>
                                <td>{{ $user->middle_name }}</td>
                            </tr>
                            <tr>
                                <th>Last Name</th>
                                <td>{{ $user->last_name }}</td>
                            </tr>
                            <tr>
                                <th>Username</th>
                                <td>
                                    {{ $user->username }}
                                </td>
                            </tr>
                            <tr>
                                <th>password&nbsp;&nbsp;<span class="fa fa-key"></span></th>
                                <td>
                                 <form action="user/{{$user->id}}/resetpassword" method="POST">
                                  {{ csrf_field() }}
                                     <button type="submit" class="btn btn-primary">Reset password</button>
                                 </form>
                                </td>
                            </tr>
                            <tr>
                                <th>Email</th>
                                <td>{{ $user->email }}</td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="panel panel-success">
                    <div class="panel-heading">
                        <h3 class="panel-title">User roles</h3>
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-sm-12">
                                <form action="/users/{{ $user->id }}/roles" method="POST" role="form">
                                    {{ csrf_field() }}
                                    <table class="table table-striped">
                                        <thead style="background:gray;color:white;">
                                        <th>Emp.Id</th>
                                        <th>Names</th>
                                        <th>User name</th>
                                        <th>Admin</th>
                                        <th>Receptionist</th>
                                        <th>Doctor</th>
                                        <th>Pharmacist</th>
                                        <th>Laboratory Tech</th>
                                        <th>Imaging-tech</th>
                                        <th>Action</th>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td>{{ $user->id }}<input type="hidden" name="id" value="{{ $user->id }}">
                                            </td>
                                            <td>
                                                {{ $user->first_name }}  {{ $user->middle_name }}  {{ $user->last_name }}
                                            </td>
                                            <td>{{ $user->username }}</td>
                                            <td><input type="checkbox"
                                                       name="role_admin" {{ $user->hasRole('admin') ? 'checked' : '' }} >
                                            </td>
                                            <td><input type="checkbox"
                                                       name="role_receptionist" {{ $user->hasRole('receptionist') ? 'checked' : '' }} >
                                            </td>
                                            <td><input type="checkbox"
                                                       name="role_doctor" {{ $user->hasRole('doctor') ? 'checked' : '' }} >
                                            </td>
                                            <td><input type="checkbox"
                                                       name="role_pharmacist" {{ $user->hasRole('pharmacist') ? 'checked' : '' }} >
                                            </td>
                                            <td><input type="checkbox"
                                                       name="role_laboratory-attendant" {{ $user->hasRole('laboratory-attendant') ? 'checked' : '' }} >
                                            </td>    
                                            <td><input type="checkbox"
                                                       name="role_imaging_tech" {{ $user->hasRole('imaging') ? 'checked' : '' }} >
                                            </td>
                                            <td>
                                                <button type="submit" class="btn btn-warning">Assign role</button>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
