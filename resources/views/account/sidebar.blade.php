<div class="list-group">
    <a href="/account" class="list-group-item"><i class="fa fa-user fa-fw"></i> Overview</a>
    <a href="/account/settings/password" class="list-group-item"><i
                class="fa fa-lock fa-fw"></i> Change
        Password</a>
</div>