@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-2">
                @include('partials.sidebar')
            </div>
            <div class="col-md-10">

                <div class="panel panel-default">
                    <div class="panel-heading">Account</div>

                    <div class="panel-body">
                        <h2 class="text-center">Change Password</h2>

                        <div class="row">
                            <div class="col-sm-3">
                                @include('account.sidebar')
                            </div>
                            <div class="col-sm-5">
                                @include('errors.list')

                                <form method="POST" action="/account/settings/password">
                                    {{ csrf_field() }}

                                    <div class="form-group">
                                        <label for="current_password">Current Password</label>
                                        <input type="password" name="current_password" id="current_password"
                                               class="form-control">
                                        <p class="help-block">
                                            @if(isset($error))
                                                <span class="text-danger">{{ $error }}</span>
                                            @endif
                                        </p>
                                    </div>

                                    <div class="form-group">
                                        <label for="password">Password</label>
                                        <input type="password" name="password" id="password" class="form-control">
                                    </div>

                                    <div class="form-group">
                                        <label for="password_confirmation">Password Confirmation</label>
                                        <input type="password" name="password_confirmation" id="password_confirmation"
                                               class="form-control">
                                    </div>

                                    <button type="submit" class="btn btn-primary">Change</button>
                                </form>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
