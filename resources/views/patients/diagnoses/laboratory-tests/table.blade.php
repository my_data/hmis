<table class="table table-bordered table-hover">
    <thead>
    <tr>
        <th>SN</th>
        <th>Name</th>
        <th>Result</th>
        <th>Date</th>
    </tr>
    </thead>
    <tbody>
    @php
        $i=1
    @endphp
    @foreach($diagnosis->labTests as $labTest)
        <tr>
            <td class="text-right">{{ $i++ }}.</td>
            <td>{{ $labTest->name }}</td>
            <td>{{ $labTest->result }}</td>
            <td>{{ $labTest->created_at }}</td>
        </tr>
    @endforeach
    </tbody>
</table>