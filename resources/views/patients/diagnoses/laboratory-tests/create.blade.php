@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-2">
                @include('partials.sidebar')
            </div>
            <div class="col-md-10">
               @include('partials.patient.top_nav_bar')
                        <div class="panel panel-default">
                            <div class="panel-heading">
                              @include('errors.list')
                                <h3 class="panel-title">LABORATORY INVESTIGATION(Option)</h3>
                            </div>

                            <div class="panel-body">
<form action="/patients/{{$patient->id}}/appointment/{{$appointment->id}}/diagnoses/{{$diagnosis->id}}/laboratory-tests/store"
                                      method="POST" role="form" class="form-horizontal">
                                    {{ csrf_field() }}
                                    <div class="form-group">
                                    <label for="name" class="col-sm-2 col-sm-offset-1  control-label">Test Name</label>
                                    <div class="col-sm-6">
                                        <select name="lab_testId" id="lab_testId" class="form-control">
                                            <option value="">-Select-</option>
                                            @foreach($labTests as $labTest)
                                                <option value="{{ $labTest->id }}">{{ $labTest->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                     <div class="col-sm-3">
                                        <button type="submit" name="submit" id="submit" class="btn btn-primary btn-sm"><i class="fa fa-plus"></i>&nbsp;Add
                                    </button>  
                                    </div>
                                </div>
                                </form>
                             @php $sum = 0; @endphp
                                <table class="table table-bordered table-condensed">
                                    <tbody>
                                        @php $i = 1 @endphp
                                        @foreach($diagnosis->labTests as $lab_test)
                                            <tr>
                                                <td class="text-right">{{ $i++ }}.</td>
                                                <td>{{ $lab_test->labTest->name }}</td>
                                                <td>Tsh-{{$lab_test->labTest->price }}</td>
                                                  <td>{{$lab_test->labTest->paymentmode->name}}</td>
                                                  @if($paymode=="CASH")
                                                <td>
                                                    <form action="/patients/{{$patient->id}}/diagnoses/{{$diagnosis->id}}/laboratory-tests/remove" method="post">
                                                        {{ csrf_field() }}

                                                        <input type="hidden" name="labTest" value="{{ $lab_test->id }}">

                                                        <button type="submit" class="btn btn-danger btn-sm">&times; Remove</button>
                                                    </form>
                                                </td>
                                                @endif
                                            </tr>
                                        @php $sum+=$lab_test->labTest->price; @endphp
                                        @endforeach
                                        <tr>
                                             <strong>Total cost:Tsh&nbsp;</strong>
                                        {{ $sum }} 
                                        </tr>
                                    </tbody>
                                </table>
                                <br>
                    <div class="row">
    <div class="col-sm-4 col-sm-offset-5">                                     
<a href="/patients/{{ $patient->id }}/appointment/{{$appointment->id}}/diagnoses/{{$diagnosis->id}}/investigations" class="btn btn-primary fa fa-arrow-left">BACK</a>
<a href="/patients/{{ $patient->id }}/appointment/{{ $appointment->id }}/diagnoses/{{ $diagnosis->id }}/investigations" class="btn btn-primary">CLOSE</a>                                                      </div> 
            </div>
           
            </div>
            <div class="panel-footer">
            </div>
            </div>
            </div>
        </div>
    </div>
@endsection

 
