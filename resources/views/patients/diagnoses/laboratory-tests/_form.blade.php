<div class="form-group">
    <label for="name" class="col-sm-2 col-sm-offset-2 control-label">Test Name</label>
    <div class="col-sm-5">
        <select name="lab_testId" id="lab_testId" class="form-control">
            <option value="">-Select-</option>
            @foreach($labTests as $labTest)
                <option value="{{ $labTest->id }}">{{ $labTest->name }} - Tshs {{ $labTest->price }}</option>
            @endforeach
        </select>
    </div>
</div>