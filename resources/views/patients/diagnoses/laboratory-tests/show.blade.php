@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-2">
                @include('partials.sidebar')
            </div>
            <div class="col-md-10">
                 @include('partials.patient.top_nav_bar')
                         <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title"><srong>{{ $laboratory_test->labTest->name }}</srong>  <a href="/patients/{{$patient->id}}/appointment/{{$appointment->id}}/diagnoses/{{$diagnosis->id}}"class="pull-right fa fa-arrow-left">Back</a></h3>
                            </div>
                                 <div class="panel-body">  
                                <strong>Result</strong>
                                   <div class="well" style="background:#FEFFFC;">{{ $laboratory_test->result }}</div>
                                  
                                <strong>Description</strong>
                                   <div class="well" style="background:#FEFFFC;height:140px;">{{ $laboratory_test->description }}
                                   </div>
                                   <strong>Attachments</strong>
                                   <div class="well" style="background:#FEFFFC;">
                                      <a href="/patients/{{$patient->id}}/appointment/{{$appointment->id}}/diagnoses/{{$diagnosis->id}}/laboratory-tests/{{$laboratory_test->id}}/attachment/view" target="_blank">
                            <i class="fa fa-file"></i>
                             </a>
                                   </div>
                              </div>
                            </div>
                            
            </div>
        </div>
    </div>
    </div>
@endsection
