@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-2">
                @include('partials.sidebar')
            </div>
            <div class="col-md-10">

                <div class="row">
                    <div class="col-sm-10">

                        <div class="panel panel-default">
                            <div class="panel-heading">
                                @include('errors.list')
                                <h3 class="panel-title"><strong>OTHER TREATMENT COST(Option)</strong></h3>
                            </div>

                            <div class="panel-body">
                                <form action="/patients/{{ $patient->id }}/appointment/{{$appointment->id}}/diagnoses/{{ $diagnosis->id }}/prescriptions/{{ $prescription->id }}/othertreatments"
                                      class="form-horizontal" method="post" enctype="multipart/form-data">
                                    {{ csrf_field() }}

                                    <div class="form-group">
                                        <label for="name" class="control-label col-sm-4">Type of service:</label>
                                        <div class="col-sm-6">
                                            <input type="text" name="name" id="servicename" class="form-control" value="">
                                            <span id="servicelist"></span>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="cost" class="control-label col-sm-4">Cost(TSH)</label>
                                        <div class="col-sm-6">
                                            <input type="number" name="cost" id="cost" class="form-control" value="">
                                        </div>
                                    </div>
                                       <div class="form-group">
                                        <label for="description"
                                               class="control-label col-sm-4">Remark(Option)</label>
                                        <div class="col-sm-6">
                                            <textarea name="description" id="description" class="form-control"
                                                      rows="3"></textarea>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="attachment" class="control-label col-sm-4">Attachment</label>
                                        <div class="col-sm-6">
                                            <input type="file" name="attachment">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-sm-2 col-sm-offset-4">
                                            <button type="submit" class="btn btn-primary fa fa-plus">ADD</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <div class="panel-footer">
                                <div class="text-center">
                                    <form action="/patients/{{$patient->id}}/appointment/{{$appointment->id}}/diagnoses/{{$diagnosis->id}}/prescriptions/{{$prescription->id}}/prescription-costs"
                                          method="post">
                                        {{ csrf_field() }}
                                        <a href="/patients/{{$patient->id}}/appointment/{{$appointment->id}}/diagnoses/{{$diagnosis->id}}/prescriptions/{{$prescription->id}}/create/assign-drugs"
                                           class="btn btn-primary fa fa-arrow-left">BACK</a>
                                        <button type="submit" class="btn btn-primary btn">FINISH</button>
                                    </form>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="col-sm-2">
                        @if (Auth::user()->hasRole('doctor'))

                            @include('partials.patient.rightsidebar')
                        @else
                            @include('partials/patient/sidebar')
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

<style>
    #servicelist li{
        background:#eee;
        cursor: pointer;
        padding: 12px;
        opacity:0.9;
    }
</style>
<style>
    #servicelist li{
        background:#eee;
        cursor: pointer;
        padding: 12px;
        opacity:0.9;
    }
</style>
<script src="/js/jquery.min.js"></script>
<!-- <script
  src="http://code.jquery.com/ui/1.12.0/jquery-ui.js"
  integrity="sha256-0YPKAwZP7Mp3ALMRVB2i8GXeEndvCq3eSl/WsAl1Ryk="
  crossorigin="anonymous"></script> -->
<script type="text/javascript">
    //Javascript
    $(document).ready(function(){
     $('#servicename').keyup(function(){
            var query = $(this).val();
            
             if(query!='')
             {
                 var patient_id = {{Request::segment(2)}};

                $.ajax({
                    url:'{!! URL::to('hospitalservice/ajax-services') !!}',
                    method:"GET",
                    data:{query:query,patient_id:patient_id},
                    autoFocus:true,
                    minlength:1,
                  success:function(data)
                    {
                        $('#servicelist').empty();

                        $.each(data, function (index, subcatObj) {
                            $('#servicelist').fadeIn('<li value="'+subcatObj.id+'">'+subcatObj.value +'</li>')
                            $('#servicelist').append('<li value="'+subcatObj.id+'">'+subcatObj.value +'</li>')
                        });
                    }
                });
             }
        });
          $(document).on('click','li',function(){
              $('#servicename').val($(this).text());
              $('#servicelist').fadeOut();  
                var id = $(this).val();
                 var patient_id = {{Request::segment(2)}};
                $.ajax({
                    url:'{!! URL::to('hospitalservice/ajax-services/costs?id=') !!}' + id,
                    method:"GET",
                    data:{patient_id:patient_id},
                    autoFocus:true,
                    minlength:1,
                  success:function(data)
                    {

                        $('#cost').val(data.price).text();   
                    }
                });
                });
        });

</script>
