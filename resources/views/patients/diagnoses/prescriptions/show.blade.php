    @extends('layouts.app')

    @section('content')
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-2">
                    @include('partials.sidebar')
                </div>
                        <div class="col-sm-10">
                             @include('partials.patient.top_nav_bar')
                                  <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h3 class="panel-title">Patient treatments</h3>
                                </div>

                                <div class="panel-body">
                                    <table id="example" class="table table-bordered table-striped table-hover">
                                        <tr>
                                            <td>Disease</td>
                                            <td>{{$prescription->disease}}</td>
                                        </tr>
                                        <tr>
                                            <td>Descriptions</td>
                                            <td>{{$prescription->description}}</td>
                                    </table>

                                </div>
                                  <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h3 class="panel-title">Assigned Medicines</h3>
                                </div>
                                <div class="panel-body">

                                        @if($assigndrugs->count())
                                            <table id="example" class="table table-bordered table-striped table-hover">
                                                <thead>
                                                <tr>
                                                    <th>SN</th>
                                                    <th>DRUG NAME</th>
                                                    <th>DOSE</th>
                                                    <th>DESCRIPTION</th>
                                                    <th>DATE</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                @php $i = 1 @endphp
                                                @foreach($assigndrugs as $assigndrug)
                                                    <tr>
                                                        <td class="text-right">{{ $i++ }}.</td>
                                                        <td>{{ $assigndrug->drugs->name }}</td>
                                                        <td>{{ $assigndrug->dose }}</td>
                                                        <td>{{$assigndrug->descriptions}}</td>
                                                        <td>{{ $assigndrug->created_at->toDateTimeString() }}</td>
                                                    </tr>
                                                @endforeach
                                                </tbody>
                                            </table>
                                        @else
                                            <div class="alert alert-info">No medicine assigned yet</div>
                                        @endif

                                    <a href="/patients/{{ $patient->id}}/appointment/{{$appointment->id}}/diagnoses/{{$diagnosis->id}}/prescriptions"
                                       class="btn btn-primary">go back</a>
                                    <a href="/patients/{{ $patient->id}}/appointment/{{$appointment->id}}/diagnoses/{{ $diagnosis->id}}/prescriptions/create"
                                       class="btn btn-primary">Prescribe</a>
                                </div>
                            </div>
                            </div>
                        </div>
                    </div>         
    @endsection




                       


