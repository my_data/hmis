@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-2">
                @include('partials.sidebar')
            </div>
            <div class="col-md-10">

                <div class="row">
                    <div class="col-sm-10">

                        <div class="panel panel-default">
                            <div class="panel-heading">
                                @include('errors.list')
                                <h3 class="panel-title"><strong>MEDICATION FORM</strong></h3>
                            </div>

                            <div class="panel-body">
                                <form action="/patients/{{$patient->id}}/appointment/{{$appointment->id}}/diagnoses/{{$diagnosis->id}}/prescriptions/{{$prescription->id}}/assign-drugs"
                                      method="post">
                                    {{ csrf_field() }}
                                    @include('patients.diagnoses.prescriptions._form_assign_drug')
                                    <div class="form-group">
                                        <button type="submit" class="btn btn-primary fa fa-plus">ADD</button>
                                    </div>
                                </form>
                            </div>
                            <div class="panel-footer">
                                <div class="text-center">
                                    <a href="/patients/{{$patient->id}}/appointment/{{$appointment->id}}/diagnoses/{{ $diagnosis->id}}/prescriptions/{{ $prescription->id }}/othertreatments"
                                       class="btn btn-primary fa fa-arrow-right">NEXT</a>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="col-sm-2">
                        @if (Auth::user()->hasRole('doctor'))

                            @include('partials.patient.rightsidebar')
                        @else
                            @include('partials/patient/sidebar')
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

   

   
   
   
   