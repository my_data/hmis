@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-2">
                @include('partials.sidebar')
            </div>
            <div class="col-md-10">
                <div class="row">
                </div>
                <div class="row">
                    <div class="col-sm-10">
                         <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title"><srong>{{$imaging->name}}</srong>  <a href="/patients/{{$patient->id}}/appointment/{{$appointment->id}}/diagnoses/{{$diagnosis->id}}"class="pull-right fa fa-arrow-left">Back</a></h3>
                            </div>
                                 <div class="panel-body">  
                                <strong>Result</strong>
                                   <div class="well" style="background:#FEFFFC;">{{ $imaging->result }}</div>
                                  
                                <strong>Description</strong>
                                   <div class="well" style="background:#FEFFFC;">{{ $imaging->description }}
                                   </div>
                                   <strong>Attachments</strong>
                                   <div class="well" style="background:#FEFFFC;">
                                      <a href="/patients/{{$patient->id}}/appointment/{{$appointment->id}}/diagnoses/{{$diagnosis->id}}/imagingservice/{{$imaging->id}}/attachment/view" target="_blank">
                            <i class="fa fa-file"></i>
                             </a>
                                   </div>
                              </div>
                            </div>
                           
                          
                    </div>
                    <div class="col-sm-2">
                        @if (Auth::user()->hasRole('doctor'))

                            @include('partials.patient.rightsidebar')
                        @else
                            @include('partials/patient/sidebar')
                        @endif
                    </div>
                </div>
                 
            </div>
        </div>
    </div>
    </div>
@endsection
