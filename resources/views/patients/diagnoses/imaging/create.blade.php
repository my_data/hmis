@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-2">
                @include('partials.sidebar')
            </div>
             <div class="col-sm-10">
               @include('partials.patient.top_nav_bar')
                        <div class="panel panel-default">
                            <div class="panel-heading">
                       <span id="patientId" value="{{$patient->id}}"></span>
                    <h3 class="panel-title ">IMAGING SERVICES
                       <a href="/patients/{{$patient->id}}/appointment/{{$appointment->id}}/diagnosis/imaging" class="pull-right badge">Click to view previous diagnoses</a>
                    </h3>
                            </div>

                        <div class="panel-body">
                                               @include('errors.list')

  <form action="/patients/{{ $patient->id}}/appointment/{{$appointment->id}}/diagnoses/{{$diagnosis->id}}/imagingpayments/save"

                            method="POST" role="form" class="form-horizontal">

                                      {{ csrf_field() }}

                                    <div class="form-group">
                                  <label for="servicename" class="col-sm-3 control-label">Name</label>
                                        <div class="col-sm-6 col-sm-offset-0">
                                      <input type="text" name="servicename" id="servicename" class="form-control" value="" required="required">
                                 <span id="servicelist"></span>
                                        </div>
                                    </div>
                                        <div class="form-group">
                                        <label for="cost" class="control-label col-sm-3">Price(TSH)</label>
                                        <div class="col-sm-6">
                                      <input type="number" name="cost" id="cost" class="form-control" value=""> 
                                           
                                        </div>
                                    </div>                                    

                                    <div class="form-group">
                                        <label for="results" class="col-sm-3 control-label">Description(Option)</label>
                                        <div class="col-sm-6">
                                            <textarea name="results" id="results" class="form-control"
                                                      rows="5"></textarea>
                                        </div>
                                    </div>

                                    
                                    
                                    <div class="form-group">
                                        <div class="col-sm-6 col-sm-offset-8">
                                            <button type="submit" class="btn btn-primary"><i class="fa fa-plus"></i>&nbsp;ADD</button>
                                        </div>
                                    </div>
                                </form>  
                                    </div>

                                <div class="paneel-footer">
                                    <div class="text-center">
                                          
                                <a href="/patients/{{$patient->id}}/appointment/{{$appointment->id}}/diagnosis/imaging"
                                class="btn btn-primary fa fa-arrow-left">BACK</a>
                                <a href="/patients/{{ $patient->id }}/appointment/{{ $appointment->id }}/diagnosis/imaging" class="btn btn-primary">CLOSE</a>
                                    </div>
                                </div>
                            </div>

                    </div>
                </div>
            </div>
@endsection

<style>
    #servicelist li{
        background:#eee;
        cursor: pointer;
        padding: 12px;
        opacity:0.9;
    }
</style>
<script src="/js/jquery.min.js"></script>
<!-- <script
  src="http://code.jquery.com/ui/1.12.0/jquery-ui.js"
  integrity="sha256-0YPKAwZP7Mp3ALMRVB2i8GXeEndvCq3eSl/WsAl1Ryk="
  crossorigin="anonymous"></script> -->
<script type="text/javascript">
    //Javascript
    $(document).ready(function(){
     $('#servicename').keyup(function(){
            var query = $(this).val();
            
             if(query!='')
             {
                 var patient_id = {{Request::segment(2)}};

                $.ajax({
                    url:'{!! URL::to('hospitalimagingservices/ajax-services') !!}',
                    method:"GET",
                    data:{query:query,patient_id:patient_id},
                    autoFocus:true,
                    minlength:1,
                  success:function(data)
                    {
                        $('#servicelist').empty();

                        $.each(data, function (index, subcatObj) {
                            $('#servicelist').fadeIn('<li value="'+subcatObj.id+'">'+subcatObj.value +'</li>')
                            $('#servicelist').append('<li value="'+subcatObj.id+'">'+subcatObj.value +'</li>')
                        });
                    }
                });
             }
        });
          $(document).on('click','li',function(){
              $('#servicename').val($(this).text());
              $('#servicelist').fadeOut();  
                var id = $(this).val();
                 var patient_id = {{Request::segment(2)}};
                $.ajax({
                    url:'{!! URL::to('hospitalservice/ajax-services/costs?id=') !!}' + id,
                    method:"GET",
                    data:{patient_id:patient_id},
                    autoFocus:true,
                    minlength:1,
                  success:function(data)
                    {

                        $('#cost').val(data.price).text();   
                    }
                });
                });
        });

</script>
