@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-2">
                @include('partials.sidebar')
            </div>
            <div class="col-md-10">
                @include('partials.patient.top_nav_bar')
                                 <div class="panel panel-default">
                            <div class="panel-heading">
                               
                            </div>
                            <div class="panel-body">
                                @if($patient->diagnoses->count())
                                    <table class="table table-bordered table-hover">
                                        <thead>
                                        <tr>
                                            <th>SN</th>
                                            <td>Duration</td>
                                            <th>Date</th>
                                            <th>Full Details</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @php
                                            $i = 1
                                        @endphp
                                        @foreach($patient->diagnoses as $diagnosis)
                                            <tr>
                                                <td>{{ $i++ }}.</td>
                                                <td>{{ $diagnosis->created_at->diffForHumans() }}</td>
                                                <td>{{ $diagnosis->created_at->toDateTimeString() }}</td>
                                                <td>
                                                    <a href="/patients/{{ $patient->id }}/appointment/{{$appointment->id}}/diagnoses/{{ $diagnosis->id }}/imaging">
                                                        Click to view
                                                    </a>
                                                </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                @else
                                    <div class="alert alert-info">
                                        This patient has not undergone any diagnosis yet.
                                    </div>
                                @endif
                            </div>
                        </div> 
                </div>
            </div>
        </div>
    </div>
@endsection
