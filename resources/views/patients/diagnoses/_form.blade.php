<div class="form-group">
    <label for="history">HISTORY OF PRESENTING ILLINESS</label>
    <textarea name="history" id="history" rows="2" class="form-control"></textarea>
    <p class="text-muted">
    	<strong>Tip:</strong>  Write the patient's history of presenting illiness
    </p>
</div>
<div class="form-group">
    <label for="remark">PHYSICAL EXAMINATION (Option)</label>
    <textarea name="physical_examination" id="physical_examination" cols="2" rows="2" class="form-control"></textarea>
    <p class="text-muted">
        <strong>Tip: </strong> Phisical examination.
    </p>
</div>
<div class="form-group">
    <label for="remark">PROVISIONAL DIAGNOSIS</label>
    <textarea name="provisional_diagnosis" id="provisional_diagnosis" cols="2" rows="2" class="form-control"></textarea>
    <p class="text-muted">
    	<strong>Tip: </strong> Write the provisional diagnosis
    </p>
</div>

