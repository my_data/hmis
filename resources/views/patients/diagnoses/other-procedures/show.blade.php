@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-2">
                @include('partials.sidebar')
            </div>
            <div class="col-md-10">
                 @include('partials.patient.top_nav_bar')
                         <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title"><srong>More details</srong>  <a href="/patients/{{$patient->id}}/appointment/{{$appointment->id}}/diagnoses/{{$diagnosis->id}}"class="pull-right fa fa-arrow-left">Back</a></h3>
                            </div>
                                 <div class="panel-body">  
                                <strong>Procedure name</strong>
                                   <div class="well" style="background:#FEFFFC;">{{ $procedure->name }}</div>
                                  
                                <strong>Description</strong>
                                   <div class="well" style="background:#FEFFFC;height:140px;">{{ $procedure->description }}
                                   </div>
                           
                              </div>
                            </div>
                            
            </div>
        </div>
    </div>
    </div>
@endsection
