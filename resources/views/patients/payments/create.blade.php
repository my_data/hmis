@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-2">
                @include('partials.sidebar')
            </div>
            <div class="col-md-10">
                <div class="panel panel-default">
                    <div class="panel-heading">Make payments
                        for {{ $patient->first_name }} {{ $patient->middle_name }} {{ $patient->last_name }}
                        @include('errors.list')
                        </div>

                    <div class="panel-body">
                        <form action="/patients/{{ $patient->id }}/payments/new" method="POST" role="form" class="form-horizontal">
                            {{ csrf_field() }}

                            <div class="form-group">
                                <label for="doctor_id" class="col-md-3">
                                    Select payment type
                                </label>
                                <div class="col-md-4">
                                       <select name="payment_type_id" id="payment_type_id" class="form-control">
                                    <option value="">-Select-</option>
                                    @foreach($paymentTypes as $paymentType)
                                        <option value="{{ $paymentType->id }}">
                                            {{ $paymentType->display_name }} -
                                            {{ $paymentType->amount }}
                                        </option>
                                    @endforeach
                                </select>   
                                </div>
                            </div>
                                  <div class="form-group">
                                <label for="payby" class="col-md-3">
                                    Mode of payment
                                </label>
                                <div class="col-md-4">
                                       <select name="payby" id="payby" class="form-control">
                                    <option value="CASH">CASH</option>
                                     <option value="NSSF">NSSF</option>
                                      <option value="NHIF">NHIF</option>
                                       <option value="TANESCO">TANESCO</option>
                                        <option value="BUNGE">BUNGE</option>
                                </select>   
                                </div>
                            </div>

                             <div class="col-md-4 col-md-offset-4">
                            <button type="submit" class="btn btn-primary">SAVE</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
