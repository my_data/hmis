@extends('layouts.app3')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-2">
                @include('partials.sidebar')
            </div>
            <div class="col-md-10">
                @if (Auth::user()->hasRole('receptionist') || Auth::user()->hasRole('doctor') )
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            All patients for consultations
                        </div>
                         @if (Auth::user()->hasRole('receptionist'))
                        <div class="panel-heading">
                            <a href="/patients/create" class="btn btn-primary">Register New</a>
                        </div>
                         @endif
                        <div class="panel-body">
                            @if($patients->count())
                                <div class="row" style="margin-top: 50px;">
                                    <div class="col-lg-2 col-lg-offset-10 pull-right">
                                        <form action="/patients" method="post" role="search">
                                           {{csrf_field()}}
                                            <div class="input-group">
                                                <input type="text" name="search" id="search" class="form-control" placeholder="Search user">
                                                <span class="input-group-btn">
                                                    <button type="submit" class="btn btn-default">
                                                        <span class="glyphicon glyphicon-search"></span>
                                                    </button>
                                                </span>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                                <table class="table table-bordered table-striped table-hover">
                                    <thead>
                                    <tr>
                                        <th>SN.</th>
                                         <th>PNO.</th>
                                        <th>NAMES</th>
                                        <th>GENDER</th>
                                        <th>DOB</th>
                                        @if (Auth::user()->hasRole('receptionist'))
                                        <th>CONTACTS</th>
                                        @endif
                                        @if (Auth::user()->hasRole('admin'))
                                            <th>Roles</th>
                                            <th>Delete</th>
                                        @endif
                                        @if (Auth::user()->hasRole('doctor'))
                                        <th>VISITED</th>
                                        @endif
                                        @if(!Auth::user()->hasRole('doctor'))
                                        <th>REGISTERED</th>
                                        @endif
                                         @if (Auth::user()->hasRole('receptionist'))
                                        <th>ACTION</th>
                                        @endif
                                           @if(Auth::user()->hasRole('doctor'))
                                        <th>MARK</th>
                                         @endif
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @php
                                    $i=1;
                                    @endphp
                                    @foreach($patients as $patient)
                                                     <tr>
                                            
                                            <td>{{$i++}}</td>
                                            <td>
                                                @php 
                                                $id= $patient->id;
                                                $endstr = substr($id,-0);
                                                if(strlen($endstr) % 2 != 0)
                                                $endstr=$endstr."0";
                                                $chunks = str_split($endstr, 2);

                                                $result = implode('-', $chunks);
                                                if($id <100 )
                                                 $result = "00-00-".$result;
                                                 elseif($id >=100 && $id < 9999)
                                                $result = "00-".$result;
                                                @endphp
                                                {{ $result }}
                                            </td>
                                            <td>
                                                @if(Auth::user()->hasRole('doctor'))
                                                  <a href="/patients/{{ $patient->id }}">
                                                    {{ $patient->first_name }} {{ $patient->middle_name }} {{ $patient->last_name }}
                                                </a>
                                                @else
                                                     <a href="/patients/{{ $patient->id }}">
                                                    {{ $patient->first_name }} {{ $patient->middle_name }} {{ $patient->last_name }}
                                                </a>
                                                @endif
                                            </td>
                                            <td>{{ $patient->gender }}</td>
                                            <td>{{ $patient->dob->toFormattedDateString() }}</td>
                                            @if (Auth::user()->hasRole('receptionist'))
                                            <td>{{ $patient->phone }}</td>
                                            @endif
                                            @if(Auth::user()->hasRole('doctor'))
                                            <td>
                                                  @if($patient->appointments->count())
                                                <span class="badge badge-secondary" style="background:red">New</span>
                                                  @else
                                                     <i class="badge">done</i>
                                                  @endif
                                            </td>
                                            @endif
                                              @if(!Auth::user()->hasRole('doctor'))
                                            <td>{{ $patient->created_at->toDayDateTimeString() }}</td>
                                              @else
                                               @if($patient->appointments->count())
                                                                 <td><a href="/patients/{{ $patient->id }}/read" style="color:green"><i class="fa fa-envelope" aria-hidden="true"></i>&nbsp;unread</a></td>

                                                @else
                                                                 <td><a href="/patients/{{ $patient->id }}/read"><i class="fa fa-envelope-open" aria-hidden="true"></i>&nbsp;Read</a></td>
                                                @endif
                                                 @endif
                                            @if (Auth::user()->hasRole('admin'))
                                                <td>
                                                    @if($patient->roles()->count())
                                                        @foreach($patient->roles as $role)
                                                            <span class="badge">{{ $role->name }}</span>
                                                        @endforeach
                                                    @endif
                                                </td>
                                            @endif
                                               @if (Auth::user()->hasRole('receptionist'))
                                            <td>
                                                <a href="/patients/{{ $patient->id }}/edit"
                                                   class="btn btn-primary btn-sm"><i class="fa fa-edit"></i> Edit</a>
                                            </td>
                                            @endif
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                                <div class="pull-left " style="margin-top: 20px;">Showing {{ $patients->firstItem() }} to {{ $patients->lastItem() }}
                                of total {{$patients->total()}} entries</div>
                              <div class="pull-right">  {{ $patients->links()}}</div>
                            @else
                                <div class="alert alert-info text-center">
                                    No Patient
                                </div>
                            @endif
                            @endif
                        </div>
                    </div>
            </div>
        </div>
    </div>
@endsection
