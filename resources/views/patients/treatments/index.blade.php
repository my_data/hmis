@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-2">
                @include('partials.sidebar')
            </div>
            <div class="col-md-10">
                <div class="row">
                </div>
                <div class="row">
                    <div class="col-sm-10">

                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title">Patient Treatments</h3>
                            </div>

                            <div class="panel-body">
                                @if($treatments->count())
                                    <table class="table table-bordered ">
                                        <thead>
                                        <tr>
                                            <th>SN</th>
                                            <th>Description</th>
                                            <th>Provided</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @php
                                            $i = 1
                                        @endphp
                                        @foreach($treatments as $treatment)
                                            <tr>
                                                <td>{{ $i++ }}.</td>
                                                <td>{{ $treatment->body }}</td>
                                                <td title="{{ $treatment->created_at }}">{{ $treatment->created_at->diffForHumans() }}</td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                @else
                                    <div class="alert alert-info">
                                        This patient has not undergone any diagnosis yet.
                                    </div>
                                @endif
                            </div>
                            <div class="panel-footer">
                                <a href="/patients/{{ $patient->id }}/diagnoses/new" class="btn btn-success">New
                                    Diagnosis</a>
                            </div>
                        </div>

                    </div>
                    <div class="col-sm-2">
                            @if (Auth::user()->hasRole('doctor'))

                       @include('partials.patient.rightsidebar')

                      @endif
                      @if (Auth::user()->hasRole('receptionist'))
                       @include('partials/patient/sidebar')
                      @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
