@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-2">
                @include('partials.sidebar')
            </div>
            <div class="col-md-10">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="well">
                            <h2>
                                {{ $patient->first_name }}
                                {{ $patient->middle_name }}
                                {{ $patient->last_name }}
                            </h2>
                            <p>
                                DOB: {{ $patient->dob->toDateString() }} (Born: {{ $patient->dob->diffForHumans() }})
                            </p>
                            <p>
                                <i class="fa fa-phone"></i> {{ $patient->phone }}
                                <i class="fa fa-envelope"></i> {{ $patient->email }}
                                <i class="fa fa-map-marker"></i> {{ $patient->address }}
                            </p>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-10">

                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title">Patient Appointments <a href="/payments"class="pull-right fa fa-arrow-left">Back</a></h3>
                            </div>

                            <div class="panel-body">
                                @if($patient->appointments->count())
                                    <table class="table table-bordered table-hover">
                                        <thead>
                                        <tr>
                                            <th>SN</th>
                                            <th>Doctor</th>
                                            <th>Total cost(TZSH)</th>
                                            <td>Duration</td>
                                            <th>Date</th>
                                            <td><i class="fa fa-eye"></i>&nbsp;View payments</td>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @php
                                            $i = 1
                                        @endphp
                                        @foreach($appointments as $appointment)
                                            <tr>
                                                <td class="text-right">{{ $i++ }}.</td>
                                                <td>{{ $appointment->doctor->username }}</td>
                                                                                
                                           <td>
                                        @php $amount=0;
                                        foreach($allPayments as $payment)
                                        {

                                         $amount=$payment->wherepatient_id($patient->id)->whereappointment_id($appointment->id)->sum('amount');
                                        }
                                        @endphp
                                        {{number_format($amount)}}
                                        </td>

                                                <td>{{ $appointment->created_at->diffForHumans() }}</td>
                                                      <td>{{ $appointment->created_at->toFormattedDateString() }}</td>
                                                    <td>
                                                        <a href="/patients/{{ $patient->id }}/appointment/{{$appointment->id}}/payments">Click
                                                            to
                                                            View</a>
                                                    </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                @else
                                    <div class="alert alert-info">
                                        This patient has not undergone any diagnosis yet.
                                    </div>
                                @endif
                            </div>
                            <div class="panel-footer">
                                <a href="/patients/{{ $patient->id }}/appointments/create" class="btn btn-success">New
                                    Appointment</a>
                            </div>
                        </div>

                    </div>
                    <div class="col-sm-2">
                        @include('partials/patient/sidebar')
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
