@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-2">
                @include('partials.sidebar')
            </div>
            <div class="col-md-10">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Patient's Management
                        </div>
                        <div class="panel-body">
                        @include('partials.patient.top_nav_bar')          
                        </div>
                    </div>
                    </div>
            </div>
        </div>
    </div>
@endsection
