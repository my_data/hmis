@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-2">
                @include('partials.sidebar')
            </div>
            <div class="col-md-10">
                <div class="row">
                    <div class="col-sm-10">

                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title">
                                    Appointment for
                                    {{ $patient->first_name }}
                                    {{ $patient->middle_name }}
                                    {{ $patient->last_name }}
                                </h3>
                            </div>

                            <div class="panel-body">

                                <p class="lead">
                                    {{ $appointment->description }}
                                </p>

                                <a href="/patients/{{ $patient->id }}/appointment/{{$appointment->id}}/diagnoses/new"
                                   class="btn btn-primary">Diagnose</a>
                            </div>
                            <div class="panel-footer">
                                <i class="fa fa-binoculars"></i> To view patient information click Information on the
                                right
                            </div>
                        </div>

                    </div>
                    <div class="col-sm-2">
                        @if (Auth::user()->hasRole('doctor'))
                            @include('partials.patient.rightsidebar')
                        @else
                            @include('partials/patient/sidebar')
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
