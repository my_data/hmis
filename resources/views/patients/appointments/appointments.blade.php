@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-2">
                @include('partials.sidebar')
            </div>
            <div class="col-md-10">

                @if(Auth::user()->hasRole('doctor'))
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title">
                                All Appointments
                            </h3>
                        </div>
                        <div class="panel-body">
                            @if($appointments->count())
                                <table class="table table-bordered table-hover">
                                    <thead>
                                    <tr>
                                        <th>SN</th>
                                        <th>Appointment</th>
                                        <th>Made</th>
                                        <th>Date</th>
                                        <th>Pay</th>
                                        <th>Click</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @php
                                        $i = 1;
                                    @endphp
                                    @foreach($appointments as $appointment)

                                    @if($appointment->patient)

                                        <tr>
                                            <td class="text-right">{{ $i++ }}.</td>
                                            <td>
                                                Dr.{{ $appointment->doctor->last_name}}
                                            </td>
                                            <td>  {{ $appointment->created_at->diffForHumans() }}</td>
                                            <td>{{ $appointment->created_at->toDayDateTimeString() }}</td>
                                            <td><span class="badge badge-secondary">{{$pmode->name}}</span></td>
                                            <td>
                                                    <a href="/patients/{{$patient->id}}/appointments/{{$appointment->id}}/manage">
                                                <i class="fa fa-eye"></i>&nbsp;View
                                                   </a>
                                            </td>
                                        </tr>
                                        
                                        @endif
                                    @endforeach
                                    </tbody>
                                </table>
                            @else
                                No Appointment
                            @endif
                        </div>
                    </div>
                @endif

                    </div>
            </div>
        </div>
    </div>
@endsection
