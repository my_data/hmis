@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-2">
                @include('partials.sidebar')
            </div>
            <div class="col-md-10">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">New Patient</h3>
                    </div>

                    <div class="panel-body">
                        @include('errors.list')

                        <div class="row">
                            <div class="col-sm-10">
                                <form action="/patients" method="POST" class="form-horizontal" role="form">
                                    {{ csrf_field() }}

                                    @include('patients._form')


                                    <div class="form-group">
                                        <div class="col-sm-7 col-sm-offset-3">
                                            <button type="submit" class="btn btn-primary">Register</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
