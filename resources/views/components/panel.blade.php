<div class="panel panel-default">
	<div class="panel-heading">
		<h3 class="panel-title">{{ $title }}</h3>
	</div>
	<div class="panel-body">
		
        <div class="row">
            @foreach($stats as $stat)
                <div class="col-sm-3 text-center">
                    <div class="well">
                        <h1>{{ number_format($stat->total) }}</h1>
                        @if($stat->modeOfPayment)
                         <small>{{ $stat->modeOfPayment->name }}</small>
                        @else
                        <small>NOT PAID</small>
                        @endif
                    </div>
                </div> 
            @endforeach
        </div>
		
	</div>
</div>