<div class="form-group">
    <label for="name" class="col-sm-4 control-label">Test name</label>
    <div class="col-sm-8">
        <input type="text" name="name" id="name" value="{{ $laboratoryTest->name or old('name') }}"
               class="form-control">
    </div>
</div>

<div class="form-group">
    <label for="amount" class="col-sm-4 control-label">Price</label>
    <div class="col-sm-8">
        <input type="number" name="price" id="price" value="{{ $laboratoryTest->price or old('price') }}"
               class="form-control">
    </div>
</div>

<div class="form-group">
    <label for="amount" class="col-sm-4 control-label">Mode of payment</label>
    <div class="col-sm-8">
    <select name="paymentmode_id" id="paymentmode_id" class="form-control" required="required">
    @foreach($paymentmodes as $paymentmode)
        <option value="{{$paymentmode->id}}">{{$paymentmode->name}}</option>
        @endforeach
    </select>
    </div>
</div>


