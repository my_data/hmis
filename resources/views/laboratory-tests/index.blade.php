@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-2">
                @include('partials.sidebar')
            </div>
            <div class="col-md-10">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <a href="/laboratory-tests/create" class="btn btn-primary">New</a>
                        <a class="btn btn-primary" data-toggle="modal" href='#modal-id'>or Upload</a>
                        <div class="modal fade" id="modal-id">
                            <div class="modal-dialog">

                                <form method="POST" action="/laboratory-tests/upload" enctype="multipart/form-data">
                                    {{ csrf_field() }}

                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal"
                                                    aria-hidden="true">&times;</button>
                                            <h4 class="modal-title">Upload</h4>
                                        </div>
                                        <div class="modal-body">
                                            <div class="form-group">
    <label for="amount" class="control-label">Select mode of payment </label>
    <select name="paymentmode_id" id="paymentmode_id" class="form-control" required="required">
    @foreach($paymentmodes as $paymentmode)
        <option value="{{$paymentmode->id}}">{{$paymentmode->name}}</option>
        @endforeach
    </select>
    </div>
                                            <input type="file" name="user_file">

                                            <br>

                                            <a href="{{ route('download', 'Laboratory Tests.xlsx') }}">Download Sample</a>

                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-default" data-dismiss="modal">Cancel
                                            </button>
                                            <button type="submit" class="btn btn-primary">Upload</button>
                                        </div>
                                    </div>
                                </form>

                            </div>
                        </div>
                    </div>

                    <div class="panel-body">
                        @if($laboratoryTests->count())
                            <table class="table table-striped table-hover">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Name</th>
                                    <th>Price</th>
                                    <th>Mode of payment</th>
                                    <th>Edit</th>
                                    <th>Delete</th>
                                </tr>
                                </thead>
                                <tbody>
                                @php
                                    $i = 1;
                                @endphp
                                @foreach($laboratoryTests as $laboratoryTest)
                                    <tr>
                                        <td>{{ $i++ }}.</td>
                                        <td>
                                            {{ $laboratoryTest->name }}
                                        </td>
                                        <td>

                                            {{ $laboratoryTest->price }}

                                        </td>
                                        <td>{{$laboratoryTest->paymentmode->name}}</td>
                                    
                                        <td>
                                            <a href="/laboratory-tests/{{$laboratoryTest->id}}"
                                               class="btn btn-primary btn-sm"><i class="fa fa-edit"></i> Edit</a>
                                        </td>
                                        <td>
                                            <a class="btn btn-danger btn-sm" data-toggle="modal"
                                               href='#delete-lab-test-{{$laboratoryTest->id}}'><i class="fa fa-trash"></i> Delete</a>
                                            <div class="modal fade" id="delete-lab-test-{{$laboratoryTest->id}}">
                                                <div class="modal-dialog">
                                                    <form method="POST"
                                                          action="/laboratory-tests/{{ $laboratoryTest->id }}">
                                                        {{ csrf_field() }}
                                                        {{ method_field('DELETE') }}

                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <button type="button" class="close" data-dismiss="modal"
                                                                        aria-hidden="true">&times;</button>
                                                                <h4 class="modal-title">Delete</h4>
                                                            </div>
                                                            <div class="modal-body">
                                                                Delete permanently {{ $laboratoryTest->name }}?
                                                            </div>
                                                            <div class="modal-footer">
                                                                <button type="button" class="btn btn-default"
                                                                        data-dismiss="modal">Cancel
                                                                </button>
                                                                <button type="submit" class="btn btn-danger"><i
                                                                            class="fa fa-trash"></i> Delete
                                                                </button>
                                                            </div>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        @else
                            <div class="alert alert-info text-center">
                                No Laboratory Test
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
