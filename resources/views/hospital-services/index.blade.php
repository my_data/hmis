@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-2">
                @include('partials.sidebar')
            </div>
            <div class="col-md-10">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <a href="/hospitalservices/create" class="btn btn-primary">New</a>
                        <a class="btn btn-primary" data-toggle="modal" href='#modal-id'>or Upload</a>
                        <div class="modal fade" id="modal-id">
                            <div class="modal-dialog">

                                <form method="POST" action="hospitalservice/upload" enctype="multipart/form-data">
                                    {{ csrf_field() }}

                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal"
                                                    aria-hidden="true">&times;</button>
                                            <h4 class="modal-title">Upload</h4>
                                        </div>
                                        <div class="modal-body">
                                                                               <div class="form-group">
    <label for="amount" class="control-label">Select mode of payment </label>
    <select name="paymentmode_id" id="paymentmode_id" class="form-control" required="required">
    @foreach($paymentmodes as $paymentmode)
        <option value="{{$paymentmode->id}}">{{$paymentmode->name}}</option>
        @endforeach
    </select>
    </div>
                                            <input type="file" name="user_file">

                                            <br>

                                            <a href="{{ route('download', 'hospitalservice.xlsx') }}">Download Sample</a>

                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-default" data-dismiss="modal">Cancel
                                            </button>
                                            <button type="submit" class="btn btn-primary">Upload</button>
                                        </div>
                                    </div>
                                </form>

                            </div>
                        </div>
                    </div>

                    <div class="panel-body">
                        @if($services->count())
                            <table class="table table-striped table-hover">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Type of service</th>
                                    <th>Price</th>
                                    <th>Payment mode</th>
                                    <th>Edit</th>
                                    <th>Delete</th>
                                </tr>
                                </thead>
                                <tbody>
                                @php
                                    $i = 1;
                                @endphp
                                @foreach($services as $service)
                                    <tr>
                                        <td>{{ $i++ }}.</td>
                                        <td>
                                            {{$service->name }}
                                        </td>
                                        <td>

                                            {{$service->price }}

                                        </td>
                                        <td>
                                            {{$service->paymentmode->name}}
                                        </td>
                                        <td>
                                            <a href="/hospitalservice/{{$service->id}}/edit"
                                               class="btn btn-primary btn-sm"><i class="fa fa-edit"></i> Edit</a>
                                        </td>
                                        <td>
                            <a class="btn btn-danger btn-sm" data-toggle="modal"
                                               href='#delete-hospitalservice'><i class="fa fa-trash"></i> Delete</a>
                                            <div class="modal fade" id="delete-hospitalservice">
                                                <div class="modal-dialog">
                                                    <form method="POST"
                                                          action="hospitalservice/{{$service->id}}">
                                                        {{ csrf_field() }}
                                                        {{ method_field('DELETE') }}

                                                        <div class="modal-content">
                                            <div class="modal-header">
           <button type="button" class="close" data-dismiss="modal"
                aria-hidden="true">&times;</button>
                        <h4 class="modal-title">Delete</h4>
                                                            </div>
                            <div class="modal-body">
                            Delete permanently {{$service->name }}?
                                                            </div>
                                                            <div class="modal-footer">
                                                                <button type="button" class="btn btn-default"
                                                                        data-dismiss="modal">Cancel
                                                </button>
        <button type="submit" class="btn btn-danger"><i
                                    class="fa fa-trash"></i> Delete
                                                                </button>
                                                            </div>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        @else
                            <div class="alert alert-info text-center">
                                No Services/Procedures available
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
