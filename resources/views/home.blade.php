@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-2">
                @include('partials.sidebar')
            </div>
            <div class="col-md-10">

                <div class="panel panel-default">
                    <div class="panel-heading">
					<strong>
					Welcome
					 @if(Auth::user()->hasRole('doctor'))
						 Dr.{{Auth::user()->username}}
					 @elseif(Auth::user()->gender=="Female")
						 Miss {{Auth::user()->first_name}}
					  @elseif(Auth::user()->gender=="Male")
						 Mr {{Auth::user()->first_name}}
                      @else
                       	{{Auth::user()->username}}					  
					 @endif
					 <div class="pull-right">You logged in as
					 {{ Auth::User()->roles->first()->name}}
					 </div>
					</strong>
					</div>

                    <div class="panel-body">
                        @if(Auth::user()->hasRole('admin'))
                        <div class="row">
                            <div class="col-sm-2">
                                <div class="well text-center">
                                    <h1>{{ $patients }}</h1>
                                    <p>Patients</p>
                                </div>
                            </div>
                            <div class="col-sm-2">
                                <div class="well text-center">
                                    <h1>{{ $receptionists }}</h1>
                                    <p>Receptionists</p>
                                </div>
                            </div>  
                            <div class="col-sm-2">
                                <div class="well text-center">
                                    <h1>{{ $doctors }}</h1>
                                    <p>Doctors</p>
                                </div>
                            </div>    
                              <div class="col-sm-2">
                                <div class="well text-center">
                                    <h1>{{ $imaging_tech }}</h1>
                                    <p>Imaging-tech</p>
                                </div>
                            </div>  
                            <div class="col-sm-2">
                                <div class="well text-center">
                                    <h1>{{ $pharmacists }}</h1>
                                    <p>Pharmacists</p>
                                </div>
                            </div>  
                            <div class="col-sm-2">
                                <div class="well text-center">
                                    <h1>{{ $admins }}</h1>
                                    <p>Administrators</p>
                                </div>
                            </div>                             
                            <div class="col-sm-2">
                                <div class="well text-center">
                                    <h1>{{ $laboratoryAttendants }}</h1>
                                    <p>Lab Attendants</p>
                                </div>
                            </div>                                                                                                            
                        </div>
                        @endif

                        @if(Auth::user()->hasRole('receptionist'))
                            <div class="row">
                                <div class="col-sm-2">
                                    <div class="well text-center">
                                        <h1>{{ $patients }}</h1>
                                        <p>Patients</p>
                                    </div>
                                </div>                                
                            </div>
                        @endif   
                        @if(Auth::user()->hasRole('imaging'))
                            <div class="row">
                                <div class="col-sm-2">
                                    <div class="well text-center">
                                        <a href="/imaging/patient/fromdoctor">
                                        <h1>{{ $patients }}</h1>
                                        <p>Patients</p>  
                                        </a>
                                    </div>
                                </div>                                
                            </div>
                        @endif

                        @if(Auth::user()->hasRole('doctor'))
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="well text-center">
                                    <h3>Patients from Reception</h3>
                                    @php $countAppointments = 0; @endphp
                                    @foreach($appointments as $appointment)
                                    @if($appointment->patient)
                                    @php $countAppointments++; @endphp
                                    @endif
                                    @endforeach
                                        <h1>{{ $countAppointments }}</h1>
                                        <a href="/patients/fromreception">
                                            <p>Appointment(s)</p>  
                                        </a> 
                                    </div>
                                </div>                                
                            </div> 
                                      <div class="row">
                                <div class="col-sm-12">
                                    <div class="well text-center">
                                    <h3>Patients from Laboratory</h3>
                                        <h1>{{ $patient_from_lab->count() }}</h1>
                                <a href="/patients/laboratory-results">
                                            <p>Results(s)</p>  
                                        </a> 
                                    </div>
                                </div>                                
                            </div>                           
                        @endif                        

                        @if(Auth::user()->hasRole('laboratory-attendant'))
                            <div class="row">
                                <div class="col-sm-2">
                                    <div class="well text-center">
                                        <a href="patient/fromdoctor">
                                        <h1>{{ $patients }}</h1>
                                        <p>Patients</p>
                                        </a>
                                    </div>
                                </div>                                
                            </div>                            
                        @endif
                        @if(Auth::user()->hasRole('pharmacist'))
                            <div class="row">
                                <div class="col-sm-2">
                                    <div class="well text-center">
                                        <a href="pharmace/patients">
                                              <h1>{{ $pharmacypatients }}</h1>
                                        <p>Patients</p>  
                                        </a>
                                    </div>
                                </div>                                
                            </div>                            
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
