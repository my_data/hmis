@extends('layouts.app2')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-2">
                @include('partials.sidebar')
            </div>
            <div class="col-md-10">
            	<div class="row">
            		   <div class="text-right col-md-12"><button class="btn btn-primary ">EXPORT</button></div>
            	</div>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title text-left">.</h3>
                    </div>

                    <div class="panel-body">
                    	<div class="row  ">
                    		<form action="{{ route('reports.store') }}" method="post" class="form">
                    			{{csrf_field() }}
                    		<label for="from" class="col-sm-1 col-sm-offset-2">From:</label>
                    		<div class="col-sm-2 ">
                    		<input type="text" readonly="readonly" class="date form-control" name="from">
                    		</div>
                    		<label for="to" class="col-sm-1">To:</label>
                    		<div class="col-sm-2">
                    		<input type="text" readonly="readonly" class="date form-control" name="to">
                    		</div>
                    		<div class="col-sm-2">
                    		<input type="submit" class="btn btn-primary" name="save" value="submit" >
                    		</div>
                    		</form>
                          
                    	</div>
                    	@if(!empty($from) && !empty($to))
                    	<div class="row">
                    		<div class="col-sm-6 col-sm-offset-3">
                    			<strong>Date from {{$from}}</strong> &nbsp;<strong> to {{$to}}</strong>
                    		</div>
                    	</div>
                    	@endif

                            <table id="reportTable" class="table table-striped table-hover table-bordered">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Doctor Names</th>
                                    <th>Patients</th>
                                    <th>Amounts(Tsh)</th>
                                    <th>View</th>
                                </tr>
                                </thead>
                                <tbody>

                                </tbody>
                            </table>
                              	<div class="row">
                    		<div class="col-sm-3 col-sm-offset-3 text-right">
                    			<strong>Total patients:{{ $totalPatients }}</strong>
                    		</div>
                    		<div class="col-sm-3 text-right">
                    		<strong>Total Payments(Tsh):{{ number_format($totalPayments) }}</strong>	
                    		</div>
                    	</div>	

                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection


@section("scripts")
    <script type="text/javascript">
        $(function() {
            $('.date').datepicker({

                format: 'yyyy-mm-dd'

            });
        });
       $(document).ready(function() {

       });

        var reportTable = $('#reportTable').DataTable({
            processing: true,
            serverSide: true,
            ajax: '{{route('reports.get-reports')}}',
            columns: [
                {data: 'DT_RowIndex', name: 'id'},
                {data: 'doctor_name', name: 'doctor_name'},
                {data: 'patients', name: 'patients'},
                {data:  'amount',name: 'amount'},
                {data: 'action', name: 'action',orderable: false, searchable: false}

            ],
            select: {
                style: 'multi'
            }
        })
    </script>
    @endsection
