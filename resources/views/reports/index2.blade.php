@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-2">
                @include('partials.sidebar')
            </div>
            <div class="col-md-10">
            	<div class="row">
            		   <div class="text-right col-md-12"><button class="btn btn-primary ">EXPORT</button></div>
            	</div>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title text-left">.</h3>
                    </div>

                    <div class="panel-body">
                    	<div class="row  ">
                    		<form action="{{ route('reports.store') }}" method="post" class="form">
                    			{{csrf_field() }}
                    		<label for="from" class="col-sm-1 col-sm-offset-2">From:</label>
                    		<div class="col-sm-2 ">
                    		<input type="text" readonly="readonly" class="date form-control" name="from">
                    		</div>
                    		<label for="to" class="col-sm-1">To:</label>
                    		<div class="col-sm-2">
                    		<input type="text" readonly="readonly" class="date form-control" name="to">
                    		</div>
                    		<div class="col-sm-2">
                    		<input type="submit" class="btn btn-primary" name="save" value="submit" >
                    		</div>
                    		</form>
                          
                    	</div>
                    	@if(!empty($from) && !empty($to))
                    	<div class="row">
                    		<div class="col-sm-6 col-sm-offset-3">
                    			<strong>Date from {{$from}}</strong> &nbsp;<strong> to {{$to}}</strong>
                    		</div>
                    	</div>
                    	@endif
                        @if($doctors->count())
                            <table class="table table-striped table-hover table-bordered">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Doctor Names</th>
                                    <th>Patients</th>
                                    <th>Amounts(Tsh)</th>
                                    <th>View</th>
                                </tr>
                                </thead>
                                <tbody>
                                @php
                                    $i = 1;
                                    $totalamount=0;
                                @endphp
                                @foreach($doctors as $doctor)
                                    <tr>
                                        <td>{{ $i++ }}.</td>
                                        <td>
                                            <a href="/doctors/{{ $doctor->id }}">
                                                Dr. {{ $doctor->first_name }}
                                                {{ $doctor->middle_name }}
                                                {{ $doctor->last_name }}
                                            </a>
                                        </td>
                                        <td>
                                           {{ $doctor->appointments->count() }}
                                            </a>
                                        </td>
                                        <td>
                                          @foreach($doctor->appointments as $appointment)
                                          @php
                                          $totalamount += $appointment->payments->where('status',1)->sum('amount'); 
                                          @endphp
                                          @endforeach
                                             {{ number_format($totalamount) }}
                                        </td>
                                        <td>
                                        	<a href="#"><i class="fa fa-eye"></i>&nbsp;More details</a>
                                        </td>
                                    </tr>
                                    @php 
                                    $totalamount=0;
                                    @endphp
                                @endforeach
                                </tbody>
                            </table>
                              	<div class="row">
                    		<div class="col-sm-3 col-sm-offset-3 text-right">
                    			<strong>Total patients:{{ $totalPatients }}</strong>
                    		</div>
                    		<div class="col-sm-3 text-right">
                    		<strong>Total Payments(Tsh):{{ number_format($totalPayments) }}</strong>	
                    		</div>
                    	</div>	
                        @else
                            <div class="alert alert-info text-center">
                                No doctor
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection


@section("scripts")
    <script type="text/javascript">

        $('.date').datepicker({

            format: 'yyyy-mm-dd'

        });

        var allPayTable = $('#allPayTable').DataTable({
            processing: true,
            serverSide: true,
            ajax: '{{route('reports.get-reports')}}',
            columns: [
                {data: 'DT_RowIndex', name: 'id'},
                {data: 'doctor_name', name: 'doctor_name'},
                {data: 'patient', name: 'patient'},
                {data:  'amount',name: 'amount'},
                {data: 'action', name: 'action',orderable: false, searchable: false}

            ],
            select: {
                style: 'multi'
            }
        })
    </script>
    @endsection
