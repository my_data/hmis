@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-2">
                @include('partials.sidebar')
            </div>
            <div class="col-md-10">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <a href="/departments/create" class="btn btn-primary">New</a>
                        <a class="btn btn-primary" data-toggle="modal" href='#modal-id'>or Upload</a>
                        <div class="modal fade" id="modal-id">
                            <div class="modal-dialog">

                                <form method="POST" action="{{ route('departments.upload') }}" enctype="multipart/form-data">
                                    {{ csrf_field() }}

                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal"
                                                    aria-hidden="true">&times;</button>
                                            <h4 class="modal-title">Upload</h4>
                                        </div>
                                        <div class="modal-body">
                                            <input type="file" name="user_file">

                                            <br>

                                            <a href="{{ route('download', 'Departments.xlsx') }}">Download Sample</a>

                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-default" data-dismiss="modal">Cancel
                                            </button>
                                            <button type="submit" class="btn btn-primary">Upload</button>
                                        </div>
                                    </div>
                                </form>

                            </div>
                        </div>
                    </div>

                    <div class="panel-body">
                        @if($departments->count())
                            <table class="table table-striped table-hover">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Name</th>
                                    <th>Price</th>
                                    <th>Created</th>
                                    <th>Updated</th>
                                    <th>Edit</th>
                                    <th>Delete</th>
                                </tr>
                                </thead>
                                <tbody>
                                @php
                                    $i = 1;
                                @endphp
                                @foreach($departments as $department)
                                    <tr>
                                        <td>{{ $i++ }}.</td>
                                        <td>
                                            {{ $department->name }}
                                        </td>
                                        <td>

                                            {{ $department->fee }}

                                        </td>
                                        <td>
                                            {{ $department->created_at }}
                                        </td>
                                        <td>
                                            {{ $department->updated_at }}
                                        </td>
                                        <td>
                                            <a href="/departments/{{ $department->id }}/edit"
                                               class="btn btn-primary btn-sm"><i class="fa fa-edit"></i> Edit</a>
                                        </td>
                                        <td>
                                            <a class="btn btn-danger btn-sm" data-toggle="modal"
                                               href='#delete-department-{{ $department->id }}'><i class="fa fa-trash"></i> Delete</a>
                                            <div class="modal fade" id="delete-department-{{ $department->id }}">
                                                <div class="modal-dialog">
                                                    <form method="POST"
                                                          action="/departments/{{ $department->id }}">
                                                        {{ csrf_field() }}
                                                        {{ method_field('DELETE') }}

                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <button type="button" class="close" data-dismiss="modal"
                                                                        aria-hidden="true">&times;</button>
                                                                <h4 class="modal-title">Delete</h4>
                                                            </div>
                                                            <div class="modal-body">
                                                                Delete permanently {{ $department->name }}?
                                                            </div>
                                                            <div class="modal-footer">
                                                                <button type="button" class="btn btn-default"
                                                                        data-dismiss="modal">Cancel
                                                                </button>
                                                                <button type="submit" class="btn btn-danger"><i
                                                                            class="fa fa-trash"></i> Delete
                                                                </button>
                                                            </div>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        @else
                            <div class="alert alert-info text-center">
                                No Laboratory Test
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
