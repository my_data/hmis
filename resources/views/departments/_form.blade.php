<div class="form-group">
    <label for="name" class="col-sm-2 control-label">Department Name:</label>
    <div class="col-sm-10">
        <input type="text" name="name" id="name" value="{{ $department->name or old('name') }}"
               class="form-control">
    </div>
</div>

<div class="form-group">
    <label for="fee" class="col-sm-2 control-label">Consultation Fee:</label>
    <div class="col-sm-10">
        <input type="text" name="fee" id="fee" value="{{ $department->fee or old('fee') }}"
               class="form-control">
    </div>
</div>


