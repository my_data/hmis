@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-2">
                @include('partials.sidebar')
            </div>
            <div class="col-md-10">
                <div class="panel panel-default">
                    <div class="panel-heading">Patient's imaging services</div>

                    <div class="panel-body">
                        @if($patients->count())
                            <table class="table table-bordered table-hover">
                                <thead>
                                <tr>
                                    <th>PATIENT NO.</th>
                                    <th>PATIENT NAMES</th>
                                    <th>REGISTERED</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($patients as $patient)
                           
                                   @if($patient->payments->where('type_of_payment','Diagnosis cost')->where('status','1')->count() >0 )
                                     <tr>
                                      
                                                      <td>
                                                @php 
                                                $id= $patient->id;
                                                $endstr = substr($id,-0);
                                                if(strlen($endstr) % 2 != 0)
                                                $endstr=$endstr."0";
                                                $chunks = str_split($endstr, 2);

                                                $result = implode('-', $chunks);
                                                if($id <100 )
                                                 $result = "00-00-".$result;
                                                 elseif($id >=100 && $id < 9999)
                                                $result = "00-".$result;
                                                @endphp
                                                {{ $result }}
                                            </td>
                                        <td>
                                        <a href="/imaging/patient/{{$patient->id }}/imaging">
                                          {{ $patient->first_name }}
                                            {{ $patient->middle_name }}
                                            {{ $patient->last_name }}  
                                        </a> 
                                        </td>
                                        <td>{{ $patient->created_at->diffForHumans() }}</td>
                                    </tr>
                                   @endif
                              
                                @endforeach
                                </tbody>
                            </table>
                        @else
                            <div class="alert alert-info">
                                No patient's Imaging found
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
