@if($allPayments->count())

    <table class="table table-bordered table-hover">
        <thead>
        <tr>
            <th>SN</th>
            <th>CATEGORY</th>
            <th class="text-right">AMOUNT (TZS)</th>
            <th>PATIENT</th>
            <th>PAYMENT MODE</th>
            <th>DATE</th>
        </tr>
        </thead>
        <tbody>
        @php $i = 1 @endphp
        @foreach($allPayments as $allPayment)
        @if($allPayment->patient)
            <tr>
                <td>{{ $i++ }}.</td>
                <td>{{ $allPayment->type_of_payment}}</td>
                <td class="text-right">{{ number_format($allPayment->amount) }}</td>
                                               <td>
                                                @php 
                                                $id= $allPayment->patient->id;
                                                $endstr = substr($id,-0);
                                                if(strlen($endstr) % 2 != 0)
                                                $endstr=$endstr."0";
                                                $chunks = str_split($endstr, 2);

                                                $result = implode('-', $chunks);
                                                if($id <100 )
                                                 $result = "00-00-".$result;
                                                 elseif($id >=100 && $id < 9999)
                                                $result = "00-".$result;
                                                @endphp
                                                {{ $result }}
                                            </td>
                <td>
                    @if($allPayment->mode_of_payment)
                        {{ $allPayment->modeOfPayment->name }}
                    @else
                    <span class="btn btn-warning btn-sm">Not Paid</span>
                    @endif
                </td>
                <td>{{ $allPayment->created_at }}</td>
            </tr>
            @endif
        @endforeach
        </tbody>
    </table>
@else
    <div class="alert alert-info">
        No Payment yet
    </div>
@endif