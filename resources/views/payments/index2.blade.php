@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-2">
                @include('partials.sidebar')
            </div>
            <div class="col-md-10">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <a class="btn btn-primary" data-toggle="modal" href='#modal-id'><i
                                    class="fa fa-file-excel-o"></i> Export</a>
                        <div class="modal fade" id="modal-id">
                            <div class="modal-dialog">

                                <form method="POST" action="/export">
                                    {{ csrf_field() }}

                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                                                &times;
                                            </button>
                                            <h4 class="modal-title">Export Wizard</h4>
                                        </div>
                                        <div class="modal-body">

                                            <div class="form-group">
                                                <label for="from">From</label>
                                                <input type="date" name="from" id="from"
                                                       value="{{ \Carbon\Carbon::today()->format('Y-m-d') }}"
                                                       class="form-control">
                                            </div>

                                            <div class="form-group">
                                                <label for="from">To</label>
                                                <input type="date" name="to" id="to"
                                                       value="{{ \Carbon\Carbon::now()->format('Y-m-d') }}"
                                                       class="form-control">
                                            </div>

                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-default" data-dismiss="modal">Cancel
                                            </button>
                                            <button type="submit" class="btn btn-primary"><i
                                                        class="fa fa-file-excel-o"></i> Export
                                            </button>
                                        </div>
                                    </div>
                                </form>

                            </div>
                        </div>
                    </div>

                    <div class="panel-body">

                        <div role="tabpanel">
                            <!-- Nav tabs -->
                            <ul class="nav nav-tabs" role="tablist">
                                <li role="presentation" class="active">
                                    <a href="#home" aria-controls="home" role="tab" data-toggle="tab"><i
                                                class="fa fa-th-list"></i> All Payments</a>
                                </li>
                                <li role="presentation">
                                    <a href="#tab" aria-controls="tab" role="tab" data-toggle="tab"><i
                                                class="fa fa-user-circle-o"></i> Group By Patient</a>
                                </li>
                            </ul>

                            <!-- Tab panes -->
                            <div class="tab-content">
                                <div role="tabpanel" class="tab-pane active" id="home">
                                    <br><br>

                                    <form method="GET" action="" class="form-horizontal">
                                        <div class="form-group">
                                            <label for="range" class="col-sm-2 control-label">Filter</label>
                                            <div class="col-sm-4">
                                                <select name="date" id="range" class="form-control">
                                                    <option value="today" {{ app('request')->input('date') == 'today' ? 'selected' : '' }}>
                                                        Today
                                                    </option>
                                                    <option value="yesterday" {{ app('request')->input('date') == 'yesterday' ? 'selected' : '' }}>
                                                        Yesterday
                                                    </option>
                                                    <option value="last-7-days" {{ app('request')->input('date') == 'last-7-days' ? 'selected' : '' }}>
                                                        Last 7 days
                                                    </option>
                                                    <option value="last-14-days" {{ app('request')->input('date') == 'last-14-days' ? 'selected' : '' }}>
                                                        Last 14 days
                                                    </option>
                                                    <option value="last-30-days" {{ app('request')->input('date') == 'last-30-days' ? 'selected' : '' }}>
                                                        Last 30 days
                                                    </option>
                                                </select>
                                            </div>
                                            <div class="col-sm-2">
                                                <button type="submit" class="btn btn-primary btn-sm"><i
                                                            class="fa fa-filter"></i> Appy
                                                </button>
                                            </div>
                                        </div>
                                    </form>

                                    <h4>Payment modes</h4>
                                    <ul class="list-inline">
                                    <li><a href="/payments">All</a></li>
                                    @foreach($paymentmodes as $paymentmode)
                        <li><a href="{{ route('payments.mode', $paymentmode) }}">{{$paymentmode->name}}</a></li>
                                    @endforeach
                                    </ul>

                                    @include('payments.all-payments')

                                </div>
                                <div role="tabpanel" class="tab-pane" id="tab">
                                    <br><br>
                                    @if($allPayments->count())
                                        @include('payments.group-by-patient')
                                    @else
                                        <div class="alert alert-info">
                                            No Payment
                                        </div>
                                    @endif

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection


