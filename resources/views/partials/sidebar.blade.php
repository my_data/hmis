@if (Auth::user()->hasRole('admin'))
    <!-- nav links here -->
    <div class="list-group">
        <a href="/home" class="list-group-item {{ Request::is('home') ? 'active' : '' }}"><i
                    class="fa fa-dashboard fa-fw"></i> Dashboard</a>
        <a href="{{ route('users.index') }}" class="list-group-item {{ Request::is('users') ? 'active' : '' }}"><i class="fa fa-users fa-fw"></i> All Users</a>
        <a href="{{ route('users.administrators') }}" class="list-group-item {{ Request::is('users/administrators') ? 'active' : '' }}"><i class="fa fa-superpowers fa-fw"></i> Administrators</a>
        <a href="{{ route('doctors.index') }}" class="list-group-item {{ Request::is('doctors') ? 'active' : '' }}"><i class="fa fa-stethoscope fa-fw"></i>Manage doctors</a>
        <a href="{{ route('users.pharmacists') }}" class="list-group-item {{ Request::is('users/pharmacists') ? 'active' : '' }}"><i class="fa fa-circle-thin fa-fw"></i> Pharmacists</a>
        <a href="{{ route('users.laboratory-attendants') }}" class="list-group-item {{ Request::is('users/laboratory-attendants') ? 'active' : '' }}"><i class="fa fa-flask fa-fw"></i> Lab Attendants</a>
        <a href="{{ route('users.receptionists') }}" class="list-group-item {{ Request::is('users/receptionists') ? 'active' : '' }}"><i class="fa fa-bell-o fa-fw"></i> Receptionists</a>
        <a href="{{ route('users.patients') }}" class="list-group-item {{ Request::is('users/patients') ? 'active' : '' }}"><i class="fa fa-user-circle-o fa-fw"></i> Patients</a>  <a href="{{ route('users.imaging-tech') }}" class="list-group-item {{ Request::is('users/patients') ? 'active' : '' }}"><i class="fa fa-user-circle-o fa-fw"></i>Imaging-tech</a>
    </div>
    <div class="list-group">
        <a href="#" class="list-group-item active"><i class="fa fa-wrench fa-fw"></i> Settings</a>
        <a href="/departments" class="list-group-item {{ Request::is('departments') ? 'active' : '' }}"><i
                    class="fa fa-building"></i> Departments</a>
        <a href="/payment-modes" class="list-group-item {{ Request::is('payment-mode') ? 'active': '' }}"><i class="fa fa-address-book"></i>&nbsp;&nbsp;Payment mode</a>
    </div>
@endif

@if (Auth::user()->hasRole('receptionist'))
    <!-- nav links here -->
    <div class="list-group">
        <a href="/home" class="list-group-item {{ Request::is('home') ? 'active' : '' }}"><i
                    class="fa fa-dashboard fa-fw"></i> Dashboard</a>
        <a href="/patients" class="list-group-item {{ Request::is('patients') ? 'active' : '' }}"><i
                    class="fa fa-user fa-fw"></i> Patients</a>
        <a href="/appointments" class="list-group-item {{ Request::is('appointments') ? 'active' : '' }}"><i
                    class="fa fa-money fa-fw"></i> Appointments</a> 
        <a href="/payments" class="list-group-item {{ Request::is('payments') ? 'active' : '' }}"><i class="fa fa-money fa-fw"></i> Payments</a>

        <a href="{{ route('payments.summary') }}" class="list-group-item {{ Request::is('payments/summary') ? 'active' : '' }}"><i class="fa fa-line-chart fa-fw"></i> Summary</a>
        <a href="{{ route('reports.index') }}" class="list-group-item {{ Request::is('/reports') ? 'active' : '' }}"><i class="fa fa-folder-open fa-fw"></i> Report</a>

    </div>


@endif
@if (Auth::user()->hasRole('doctor'))
    <!-- nav links here -->
    <div class="list-group">
        <a href="/home" class="list-group-item {{ Request::is('home') ? 'active' : '' }}"><i
                    class="fa fa-dashboard fa-fw"></i> Dashboard</a>
        <a href="/patients" class="list-group-item {{ Request::is('patients') ? 'active' : '' }}"><i
                    class="fa fa-pencil"></i> Appointments</a>
    </div>
@endif

@if (Auth::user()->hasRole('pharmacist'))
    <!-- nav links here -->
    <div class="list-group">
        <a href="/home" class="list-group-item {{ Request::is('home') ? 'active' : '' }}"><i
                    class="fa fa-dashboard fa-fw"></i> Dashboard</a>
        <a href="/medicines" class="list-group-item {{ Request::is('medicines') ? 'active' : '' }}"><i
                    class="fa fa-flask fa-fw"></i> Manage Medicines</a>
         <a href="/hospitalservices" class="list-group-item {{ Request::is('payment-mode') ? 'active': '' }} "><i class="fa fa-tasks"></i>&nbsp;&nbsp;Extra services</a>
        <a href="/pharmace/patients" class="list-group-item {{ Request::is('pharmace/patients') ? 'active' : '' }}"><i
                    class="fa fa-hand-paper-o fa-fw"></i> Prescriptions</a>
    </div>
@endif

@if (Auth::user()->hasRole('laboratory-attendant'))
    <div class="list-group">
        <a href="/" class="list-group-item" {{ Request::is('/') ? 'active' : '' }}><i class="fa fa-dashboard fa-fw"></i>
            Dashboard</a>
  <a href="/laboratory-tests" class="list-group-item {{ Request::is('laboratory-tests') ? 'active' : '' }}"><i
                    class="fa fa-flask fa-fw"></i>Investigations</a>
        <a href="/lab-tests" class="list-group-item"><i class="fa fa-stethoscope fa-fw"></i>Patients</a>
    </div>
@endif
@if (Auth::user()->hasRole('imaging'))
    <div class="list-group">
        <a href="/" class="list-group-item" {{ Request::is('/') ? 'active' : '' }}><i class="fa fa-dashboard fa-fw"></i>
            Dashboard</a>
  <a href="/imagingservices" class="list-group-item {{ Request::is('laboratory-tests') ? 'active' : '' }}"><i class="fa fa-flask fa-fw"></i>Imaging services</a>
        <a href="/imaging/patient/fromdoctor" class="list-group-item"><i class="fa fa-stethoscope fa-fw"></i>Patients</a>
    </div>
@endif

@if(Auth::user()->roles->count())
    <div class="well">
        <strong>Roles</strong>
        <ul>
            @foreach(Auth::user()->roles as $role)
                <li>{{ $role->name }}</li>
            @endforeach
        </ul>
    </div>
@endif
