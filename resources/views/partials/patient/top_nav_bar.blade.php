
                   @if(auth()->user()->roles->first()->name=='doctor')
                    <div class="btn-group btn-group-justified">
                    <a href="/patients/{{$patient->id}}/appointments/{{isset($appointment->id) ? $appointment->id: 0 }}/profile" class="btn btn-primary">Profile</a>
                    <a href="/patients/{{$patient->id}}/appointment/{{$appointment->id}}/diagnoses" class="btn btn-primary">Diagnosis</a>
                    <a href="/patients/{{$patient->id}}/appointment/{{$appointment->id}}/diagnoses/{{ isset($diagnosis->id) ? $diagnosis->id : 0 }}/investigations" class="btn btn-primary">Investigation</a>
                    <a href="/patients/{{$patient->id}}/appointment/{{$appointment->id}}/diagnoses/{{ isset($diagnosis->id) ? $diagnosis->id : 0 }}/imaging" class="btn btn-primary">Imaging</a>
                    <a href="/patients/{{$patient->id}}/appointment/{{$appointment->id}}/diagnoses/{{ isset($diagnosis->id) ? $diagnosis->id : 0 }}/otherprocedures" class="btn btn-primary">Procedures</a>
                     <a href="/patients/{{$patient->id}}/appointment/{{$appointment->id}}/treatments/diagnoses/" class="btn btn-primary">Treatments	</a>                    
                </div>
                @endif
             


