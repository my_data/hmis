<div class="list-group">
    @if (Auth::user()->hasRole('receptionist'))
        <a href="/patients/{{ $patient->id }}" class="list-group-item active"><i class="fa fa-home fa-fw"></i> Overview</a>
        <a href="/patients/{{ $patient->id }}/appointments" class="list-group-item"><i class="fa fa-book fa-fw"></i>
            Appointments</a>
    @endif

    @if (Auth::user()->hasRole('doctor'))
        <a href="/patients/{{ $patient->id }}" class="list-group-item active">Overview</a>
        <a href="/patients/{{ $patient->id }}/diagnoses" class="list-group-item">All Diagnoses</a>
        <a href="/patients/{{ $patient->id }}/treatments" class="list-group-item">All
            Treatments</a>
    @endif


    @if (Auth::user()->hasRole('pharmacist'))
        <a href="/patients/{{ $patient->id }}" class="list-group-item active">Overview</a>
        <a href="/patients/{{ $patient->id }}/prescriptions" class="list-group-item">All
            Prescriptions</a>
        <a href="/patients/{{ $patient->id }}/treatments" class="list-group-item">All
            Treatments</a>
    @endif

    @if (Auth::user()->hasRole('laboratory-attendant'))
        <a href="/patients/{{ $patient->id }}" class="list-group-item active">Overview</a>
        <a href="#" class="list-group-item">All Laboratory Tests</a>
    @endif
</div>