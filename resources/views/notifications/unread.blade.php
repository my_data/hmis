@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-2">
                @include('partials.sidebar')
            </div>
            <div class="col-md-10">

                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">Notifications</h3>
                    </div>
                    <div class="panel-body">
                        @if($notifications->count())
                            <ul class="menu">
                                @foreach($notifications as $notification)
                                    @include('notifications.fragments.' . snake_case(class_basename($notification->type)))
                                @endforeach
                            </ul>
                        @else
                            <div class="alert alert-info">
                                <p class="lead text-center">
                                    <i class="fa fa-info-circle"></i> No Notifications
                                </p>
                            </div>
                        @endif
                    </div>
                </div>

            </div>
        </div>
    </div>
@endsection
