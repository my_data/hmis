<li>
    A lab test was assigned {{ $notification->created_at->diffForHumans() }}.

    <form method="POST" action="/notifications/{{ $notification->id }}" style="display: inline">
        {{ csrf_field() }}
        {{ method_field('DELETE') }}

        <button type="submit" class="btn btn-default btn-sm"><i class="fa fa-bell"></i> Mark as Read</button>

    </form>

</li>
<br><br>