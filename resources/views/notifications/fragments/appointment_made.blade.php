<li>
    An appointment was made {{ $notification->created_at->diffForHumans() }}.

    <a href="/patients/{{ $notification->data['patient_id'] }}/appointments/{{ $notification->data['id'] }}"
       class="btn btn-primary btn-sm"><i class="fa fa-link"></i> View the Appointment</a>

    <form method="POST" action="/notifications/{{ $notification->id }}" style="display: inline">
        {{ csrf_field() }}
        {{ method_field('DELETE') }}

        <button type="submit" class="btn btn-default btn-sm"><i class="fa fa-bell"></i> Mark as Read</button>

    </form>

</li>
<br><br>