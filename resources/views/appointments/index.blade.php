@extends('layouts.app2')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-2">
                @include('partials.sidebar')
            </div>
            <div class="col-md-10">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">All Appointments</h3>
                    </div>

                    <div class="panel-body">

                            <table id="appTable" class="table table-striped table-hover" style="width: 100%;">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Patient</th>
                                    <th>Doctor</th>
                                    <th>Time</th>
                                </tr>
                                </thead>
                                <tbody>

                                </tbody>
                            </table>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section("scripts")
    <script type="text/javascript">
        var appTable = $('#appTable').DataTable({
            processing: true,
            serverSide: true,
            ajax: '{{route('get-appointments')}}',
            columns: [
                {data: 'DT_RowIndex', name: 'id'},
                {data: 'patient_name', name: 'patient_name'},
                {data: 'doctor_name', name: 'doctor_name'},
                {data: 'created_at', name: 'created_at'}
               //{data: 'role', name: 'role', orderable: false, searchable: false},
               // {data: 'action', name: 'action', orderable: false, searchable: false}
            ],
            select: {
                style: 'multi'
            }
        })

    </script>
@endsection
