@extends('layouts.app3')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-2">
                @include('partials.sidebar')
            </div>
            <div class="col-md-10">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">All Appointments</h3>
                    </div>

                    <div class="panel-body">
                        @if($appointments->count())
                            <div class="row" style="margin-top: 50px;">
                                <div class="col-lg-2 col-lg-offset-10 pull-right">
                                    <form action="/appointments" method="post" role="search">
                                        {{csrf_field()}}
                                        <div class="input-group">
                                            <input type="text" name="search" id="search" class="form-control" placeholder="Search user">
                                            <span class="input-group-btn">
                                                    <button type="submit" class="btn btn-default">
                                                        <span class="glyphicon glyphicon-search"></span>
                                                    </button>
                                                </span>
                                        </div>
                                    </form>
                                </div>
                            </div>
                            <table class="table table-striped table-hover">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Patient</th>
                                    <th>Doctor</th>
                                    <th>Time</th>
                                </tr>
                                </thead>
                                <tbody>
                                @php
                                    $i = 1;
                                @endphp
                                @foreach($appointments as $appointment)
                                @if($appointment->patient)
                                    <tr>
                                        <td>{{ $i++ }}.</td>
                                        <td>
                                            <a href="/patients/{{ $appointment->patient->id }}">
                                                {{ $appointment->patient->first_name }}
                                                {{ $appointment->patient->middle_name }}
                                                {{ $appointment->patient->last_name }}
                                            </a>
                                        </td>
                                        <td>
                                            <a href="/doctors/{{ $appointment->doctor->id }}">
                                                {{ $appointment->doctor->first_name }}
                                                {{ $appointment->doctor->middle_name }}
                                                {{ $appointment->doctor->last_name }}
                                            </a>
                                        </td>
                                        <td>
                                            {{ $appointment->created_at->toDayDateTimeString() }}
                                        </td>
                                    </tr>
                                    @endif
                                @endforeach
                                </tbody>
                            </table>
                            <div class="pull-left " style="margin-top: 20px;">Showing {{ $appointments->firstItem() }} to {{ $appointments->lastItem() }}
                                of total {{$appointments->total()}} entries</div>
                            <div class="pull-right">  {{ $appointments->links()}}</div>
                        @else
                            <div class="alert alert-info text-center">
                                No Appointment
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
