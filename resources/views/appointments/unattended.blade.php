@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-2">
                @include('partials.sidebar')
            </div>
            <div class="col-md-10">

                @if(Auth::user()->hasRole('doctor'))
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title">
                                All Appointments
                            </h3>
                        </div>
                        <div class="panel-body">
                            @if($appointments->count())
                                <table class="table table-bordered table-hover">
                                    <thead>
                                    <tr>
                                        <th>SN</th>
                                        <th>Patient Name</th>
                                        <th>Gender</th>
                                        <th>Date of Birth</th>
                                        <th>Made</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @php
                                        $i = 1;
                                    @endphp
                                    @foreach($appointments as $appointment)
                                        <tr>
                                            <td class="text-right">{{ $i++ }}.</td>
                                            <td>
                                                <a href="/patients/{{$appointment->patient->id}}/appointment/{{$appointment->id}}/diagnoses">
                                                    {{ $appointment->patient->first_name }}
                                                    {{ $appointment->patient->middle_name }}
                                                    {{ $appointment->patient->last_name }}
                                                </a>
                                            </td>
                                            <td>{{ $appointment->patient->gender }}</td>
                                            <td>{{ $appointment->patient->dob->toDateString() }}</td>
                                            <td>{{ $appointment->created_at->toDateTimeString() }}</td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            @else
                                No Appointment
                            @endif
                        </div>
                    </div>
                @endif


                @if (Auth::user()->hasRole('receptionist'))
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            All patients for consultations
                        </div>
                        <div class="panel-heading">
                            <a href="/patients/create" class="btn btn-primary">Register New</a>
                        </div>

                        <div class="panel-body">
                            @if($patients->count())
                                <table class="table table-bordered table-striped table-hover">
                                    <thead>
                                    <tr>
                                        <th class="text-right">#</th>
                                        <th>Full name</th>
                                        <th>Gender</th>
                                        <th>Date of Birth</th>
                                        <th>Born</th>
                                        <th>Phone number</th>
                                        @if (Auth::user()->hasRole('admin'))
                                            <th>Roles</th>
                                            <th>Delete</th>
                                        @endif
                                        <th>Registered</th>
                                        <th>Edit</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @php
                                        $i = 1;
                                    @endphp
                                    @foreach($patients as $patient)
                                        <tr>
                                            <td class="text-right">{{ $i++ }}.</td>
                                            <td>
                                                <a href="/patients/{{ $patient->id }}">
                                                    {{ $patient->first_name }} {{ $patient->middle_name }} {{ $patient->last_name }}
                                                </a>
                                            </td>
                                            <td>{{ $patient->gender }}</td>
                                            <td>{{ $patient->dob->toFormattedDateString() }}</td>
                                            <td>{{ $patient->dob->diffForHumans() }}</td>
                                            <td>{{ $patient->phone }}</td>
                                            <td>{{ $patient->created_at->toDayDateTimeString() }}</td>
                                            @if (Auth::user()->hasRole('admin'))
                                                <td>
                                                    @if($patient->roles()->count())
                                                        @foreach($patient->roles as $role)
                                                            <span class="badge">{{ $role->name }}</span>
                                                        @endforeach
                                                    @endif
                                                </td>
                                            @endif
                                            <td>
                                                <a href="/patients/{{ $patient->id }}/edit"
                                                   class="btn btn-primary btn-sm"><i class="fa fa-edit"></i> Edit</a>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            @else
                              <div class="alert alert-info text-center">
                                    No Patient
                                </div>
                            @endif
                            @endif
                        </div>
                    </div>
            </div>
        </div>
    </div>
@endsection
