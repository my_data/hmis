@extends('layouts.app')

@section('content')
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-2">
                @include('partials.sidebar')
            </div>
            <div class="col-md-10">
                <div class="panel panel-default">
                    <div class="panel-heading">Make appointment
                        for {{ $patient->first_name }} {{ $patient->middle_name }} {{ $patient->last_name }}</div>

                    <div class="panel-body">

                        @include('errors.list')

                        <form action="/patients/{{ $patient->id }}/appointments" method="POST" role="form"
                              class="form-horizontal">
                            {{ csrf_field() }}

                            <div class="form-group">
                                <label for="deptId" class="col-md-3">
                                    Select doctor specialist
                                </label>
                                <div class="col-md-4">
                                    <select name="deptId" id="deptId" class="form-control">
                                        <option value="0" disabled="true" selected="true">Select</option>
                                        @foreach($departments as $department)
                                            <option value="{{$department->id}}">
                                                {{ $department->name }}
                                            </option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="doctor" class="col-md-3">
                                    Doctor for consultation
                                </label>
                                <div class="col-md-4">
                                    <select name="doctor" id="doctor" class="form-control">
                                        <option value="0" disabled="true" selected="true">Select</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="payby" class="col-md-3">
                                    Select Mode of payment
                                </label>
                                <div class="col-md-4">
                                    <select name="payby" class="form-control" id="payby">
                                    @foreach($paymentmodes as $paymentmode)
                                    <option value="{{$paymentmode->id}}">{{$paymentmode->name}}</option>
                                    @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="description" class="col-md-3">Notes (Option)</label>
                                <div class="col-md-4">
                                    <textarea name="description" id="description" rows="5"
                                              class="form-control"></textarea>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-4 col-md-offset-3">
                                    <button type="submit" class="btn btn-primary">Create</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

<script src="/js/jquery.min.js"></script>
<script type="text/javascript">
    $(document).ready(function () {
        $(document).on('change', '#deptId', function () {

            var deptId = $(this).val();
            var div = $(this).parent();
            $.ajax({
                type: 'get',
                url: '{!! URL::to('patient/appointments/ajax-doctors') !!}',
                data: {'id': deptId},
                success: function (data) {
                    $('#doctor').empty();
                    $.each(data, function (index, subcatObj) {
                        $('#doctor').append('<option value="' + subcatObj.id + '">' + subcatObj.first_name + '-' + subcatObj.first_name + '-' + subcatObj.last_name + '</option>');
                    });
                },
                error: function () {
                }
            });
        });
    });
</script>
