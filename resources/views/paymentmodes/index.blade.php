@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-2">
                @include('partials.sidebar')
            </div>
            <div class="col-md-10">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <a href="/payment-modes/create" class="btn btn-primary">New</a>
                    </div>

                    <div class="panel-body">
                        @if($paymentmodes->count())
                            <table class="table table-striped table-hover">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Title</th>
                                    <th>Consultation Fee</th>
                                    <th>Created</th>
                                    <th>Updated</th>
                                    <th>Edit</th>
                                    <th>Delete</th>
                                </tr>
                                </thead>
                                <tbody>
                                @php
                                    $i = 1;
                                @endphp
                                @foreach($paymentmodes as $paymentmode)
                                    <tr>
                                        <td>{{ $i++ }}.</td>
                                        <td>
                                            {{ $paymentmode->name }}
                                        </td>
                                        <td>{{$paymentmode->consultation_fee}}</td>
                                        <td>
                                            {{ $paymentmode->created_at }}
                                        </td>
                                        <td>
                                            {{ $paymentmode->updated_at }}
                                        </td>
                                        <td>
                                            <a href="/payment-mode/{{ $paymentmode->id }}/edit"
                                               class="btn btn-primary btn-sm"><i class="fa fa-edit"></i> Edit</a>
                                        </td>
                                        <td>
                                            <a class="btn btn-danger btn-sm" data-toggle="modal"
                                               href='#delete-lab-test'><i class="fa fa-trash"></i> Delete</a>
                                            <div class="modal fade" id="delete-lab-test">
                                                <div class="modal-dialog">
                                                    <form method="POST"
                                                          action="/payment-mode/{{$paymentmode->id }}/delete">
                                                        {{ csrf_field() }}
                                                        {{ method_field('DELETE') }}

                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <button type="button" class="close" data-dismiss="modal"
                                                                        aria-hidden="true">&times;</button>
                                                                <h4 class="modal-title">Delete</h4>
                                                            </div>
                                                            <div class="modal-body">
                                                                Delete permanently {{ $paymentmode->name }}?
                                                            </div>
                                                            <div class="modal-footer">
                                                                <button type="button" class="btn btn-default"
                                                                        data-dismiss="modal">Cancel
                                                                </button>
                                                                <button type="submit" class="btn btn-danger"><i
                                                                            class="fa fa-trash"></i> Delete
                                                                </button>
                                                            </div>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        @else
                            <div class="alert alert-info text-center">
                                No Payment mode available now
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
