@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-2">
                @include('partials.sidebar')
            </div>
            <div class="col-md-9">
                <div class="row">
                       @include('partials.patient.top_nav_bar')
                </div>
                <div class="row">
                             <div class="panel panel-default">
                    <div class="panel-heading">
                    PROVISIONAL DIAGNOSIS
                    <a href="/lab-tests"class="pull-right fa fa-arrow-left">Back</a>
                        
                </div>
                                    @if($patient->diagnoses->count())
                                    <table class="table table-bordered table-hover">
                                        <thead>
                                        <tr>
                                            <th>SN</th>
                                            <th>DOCTOR</th>
                                            <th>DURATION</th>
                                            <th>DATE</th>
                                            <th>Full DETAILS</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @php
                                            $i = 1
                                        @endphp
                                        @foreach($diagnoses as $diagnosis)
                                            <tr>
                                                <td>{{ $i++ }}.</td>
                                                <td>{{$diagnosis->appointment->doctor->last_name}}</td>
                                                <td>{{ $diagnosis->created_at->diffForHumans() }}</td>
                                                <td>{{ $diagnosis->created_at->toDateTimeString() }}</td>
                                                <td>
                                                    <a href="/patients/{{ $patient->id }}/diagnoses/{{ $diagnosis->id }}/lab-tests">
                                                        Click to view
                                                    </a>
                                                </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                @else
                                    <div class="alert alert-info">
                                        This patient has not undergone any diagnosis yet.
                                    </div>
                                @endif
                    <div class="panel-body">
                 
                    </div>
                    </div>
                </div>  
                </div>
            </div>
        </div>
    </div>
@endsection
