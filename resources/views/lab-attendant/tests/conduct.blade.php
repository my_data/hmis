@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-2">
                @include('partials.sidebar')
            </div>
            <div class="col-md-10">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        {{ $lab_test->labTest->name }}
                        @include('errors.list')
                    </div>

                    <div class="panel-body">
                        <form action="/patient/{{$patient->id}}/diagnoses/{{$diagnosis->id}}/lab-tests/{{ $lab_test->id }}/conduct" method="POST" enctype="multipart/form-data">
                            {{ csrf_field() }}

                            <div class="form-group">
                                <label for="result">Result (Required)</label>
                                <input type="text" name="result" id="result" value="{{ old('result') }}"
                                       class="form-control">
                            </div>

                            <div class="form-group">
                                <label for="result">Notes (Option)</label>
                                <textarea name="description" id="description" class="form-control" rows="10"></textarea>
                            </div>
                             <div class="form-group">
                             <label for="attachment">Attachment(Option)</label>
                             <input type="file" name="attachment">
                            </div>
                            <button type="submit" class="btn btn-primary">Submit</button>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
