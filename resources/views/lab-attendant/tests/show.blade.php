@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-2">
                @include('partials.sidebar')
            </div>
            <div class="col-md-9">
                <div class="row">
                             <div class="row">
                    <div class="col-sm-12">
                        <p>PATIENT'S HISTORY OF PRESENTING ILLINESS</p>
                        <div class="well">
                         {{$diagnosis->body}}
                        </div>
                    </div>
                </div> 
                </div>
                <div class="row">
                             <div class="panel panel-default">
                    <div class="panel-heading">LABORATORY INVESTIGATION<a href="/patient/{{$patient->id}}/diagnoses"class="pull-right fa fa-arrow-left">Back</a>
                        </div>

                    <div class="panel-body">
                        @if($lab_tests->count())
                            <table class="table table-bordered table-hover">
                                <thead>
                                <tr>
                                    <th>SN</th>
                                    <th>Test</th>
                                    <th>Result</th>
                                    <th>Action</th>
                                    <th>Conducted</th>
                                    <th>Created</th>
                                </tr>
                                </thead>
                                <tbody>

                                @foreach($lab_tests as $lab_test)
                                    <tr>
                                        <td class="text-right">{{ $lab_test->labTest->id }}</td>
                                        <td>
                                            {{ $lab_test->labTest->name }}
                                        </td>
                                        <td>
                                            @if($lab_test->result)
                                                {{ $lab_test->result }}
                                            @else
                                                Not Conducted
                                            @endif
                                        </td>
                                        <td>
                                            @if($lab_test->is_conducted == false)
                <a href="/patient/{{$patient->id}}/diagnoses/{{$diagnosis->id}}/lab-tests/{{ $lab_test->id }}/conduct"
                                                   class="btn btn-primary btn-sm">
                                                    Conduct
                                                </a>
                                            @else
                                                <span class="label label-success">Conducted</span>
                                            @endif
                                        </td>
                                        <td>{{ $lab_test->updated_at->diffForHumans() }}</td>
                                        <td>{{ $lab_test->created_at->diffForHumans() }}</td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        @else
                            <div class="alert alert-info">
                                No Laboratory tests to conduct
                            </div>
                        @endif
                    </div>
                </div>  
                </div>
            </div>
        </div>
    </div>
@endsection
