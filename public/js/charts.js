$(document).ready(function () {

    var ctx = document.getElementById("myChart").getContext('2d');

    var time_range = document.querySelector("#time_range").value;
    var url = '/api/statistics/payments?' + time_range;

    fetchData(url);

    console.log(url);

    $("#btnApply").click(function () {
        time_range = document.querySelector("#time_range").value;
        url = '/api/statistics/payments?' + time_range;

        console.log(url);

        fetchData(url);
    });

    function fetchData(url) {

        $.ajax({
            url: url,
            data: {
                format: 'json'
            },
            error: function () {
                $('#chartStatusMessage').html("<div class='alert alert-danger text-center'>An error has occurred</div>");
            },
            dataType: 'json',
            success: function (data) {

                var chartLabels = [];
                var chartData = [];

                if (data.length == 0) {
                    $('#chartStatusMessage').html("<div class='alert alert-danger text-center'>No data</div>");
                }

                var totalAmount = 0;

                for (var $i = 0; $i < data.length; $i++) {
                    chartLabels.push(data[$i].day);
                    chartData.push(data[$i].total);
                    totalAmount += data[$i].total;
                }

                $('#totalAmount').text(totalAmount);

                drawChart(chartLabels, chartData);
            },
            type: 'GET'
        });

    }

    function drawChart(labels, data) {

        var myChart = new Chart(ctx, {
            type: 'bar',
            data: {
                labels: labels,
                datasets: [{
                    label: 'Cash Received (Tshs)',
                    data: data,
                    backgroundColor: [
                        'rgba(255, 99, 132, 0.2)',
                        'rgba(54, 162, 235, 0.2)',
                        'rgba(255, 206, 86, 0.2)',
                        'rgba(75, 192, 192, 0.2)',
                        'rgba(153, 102, 255, 0.2)',
                        'rgba(255, 159, 64, 0.2)'
                    ],
                    borderColor: [
                        'rgba(255,99,132,1)',
                        'rgba(54, 162, 235, 1)',
                        'rgba(255, 206, 86, 1)',
                        'rgba(75, 192, 192, 1)',
                        'rgba(153, 102, 255, 1)',
                        'rgba(255, 159, 64, 1)'
                    ],
                    borderWidth: 1
                }]
            },
            options: {
                scales: {
                    yAxes: [{
                        ticks: {
                            beginAtZero: true
                        }
                    }]
                }
            }
        });

    }


});

