<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Psy\Input\CodeArgument;

class Payment extends Model
{
    public function paymentType()
    {
        return $this->belongsTo(PaymentType::class, 'payment_type_id');
    }

    public function patient()
    {
        return $this->belongsTo(patient::class);
    }

    public function scopeToday($query)
    {
        return $query->whereDate('created_at', '=', Carbon::today());
    }

    public function scopeYesterday($query)
    {
        return $query->whereDate('created_at', '=', Carbon::yesterday());
    }

    public function scopePastNDays($query, $days)
    {
        return $query->where('created_at', '>=', Carbon::now()->subDays($days))
            ->where('created_at', '<', Carbon::now());
    }

    public function scopeDateRange($query, $from, $to)
    {
        return $query->where('created_at', '>=', $from)
            ->where('created_at', '<=', $to);
    }

    public function paymentMode()
    {
        $this->belongsTo(Paymentmode::class,'paymentmodes');
    }

    public function modeOfPayment()
    {
        return $this->belongsTo(Paymentmode::class, 'mode_of_payment');
    }
    public function appointmet()
    {
        return $this->belongsTo(Appointment::class);
    }
}
