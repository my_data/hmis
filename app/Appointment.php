<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Appointment extends Model
{
    public function patient()
    {
        return $this->belongsTo(User::class);
    }

    public function doctor()
    {
        return $this->belongsTo(User::class);
    }
    public function payments()
    {
    	return $this->hasMany(Payment::class);
    }
    public function diagnosis()
    {
        return $this->hasMany(Diagnosis::class);
    }
}
