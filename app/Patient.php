<?php

namespace App;

use App\Prescription;
use Illuminate\Database\Eloquent\Model;

class Patient extends Model
{
    protected $table = 'users';

    protected $dates = ['dob'];

    public function diagnoses()
    {
        return $this->hasMany(Diagnosis::class);
    }

    public function treatments()
    {
        return $this->hasMany(Treatment::class);
    }

    public function roles()
    {
        return $this->belongsToMany(Role::class, 'role_user', 'user_id');
    }

    public function appointments()
    {
        return $this->hasMany(Appointment::class);
    }

    public function payments()
    {
        return $this->hasMany(Payment::class);
    }

    public function patient_lab_tests()
    {
        return $this->hasMany(patient_lab_tests::class);
    } 
     public function imagings()
    {
        return $this->hasMany(Imaging::class);
    }

    public function prescriptions()
    {
        return $this->hasMany(Prescription::class);
    }

       public function prescription_payments()
    {
        return $this->hasMany(Prescription_payment::class);
    }


}
