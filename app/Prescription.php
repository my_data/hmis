<?php

namespace App;

use App\Patient;

use Illuminate\Database\Eloquent\Model;

class Prescription extends Model
{
    public function assigndrugs()
    {
        return $this->hasMany(Assigndrug::class);
    }

    public function patient()
    {

        return $this->belongsTo(Patient::class);

    }

    public function drugs()
    {
        return $this->belongsToMany(drug::class, 'assigndrugs', 'prescription_id', 'medicine_id')->withPivot('dose');
    }

    public function otherprescriptions()
    {
     return $this->hasMany(Otherprescription::class);   
    }
    public function prescription_payments(){

        return $this->hasMany(Prescription_payment::class);
        
    }
}
