<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Hospitalservice extends Model
{
    public function paymentmode()
    {
    	return $this->belongsTo(Paymentmode::class);
    }

    public function service_category()
    {
    	return $this->belongsTo(Service_category::class);
    }
}
