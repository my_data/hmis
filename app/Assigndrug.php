<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Assigndrug extends Model
{
    public function prescription()
    {
        return $this->belongsTo(Prescription::class);
    }

    public function drugs(){

    	return $this->hasOne(drug::class,'id','medicine_id');
    }
}
