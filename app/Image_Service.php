<?php

namespace App;

use Illuminate\Database\Eloquent\Model;



class Image_Service extends Model
{
    protected $table = 'hospitalservices';
	
 protected $fillable = ['name', 'price'];

   public function paymentmode()
    {
    	return $this->belongsTo(Paymentmode::class);
    }
}
