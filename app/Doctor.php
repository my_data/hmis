<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Doctor extends Model
{
    protected $table = 'users';

    public function allDoctors($query)
    {
        return $query;
    }
        
    public function roles()
    {
        return $this->belongsToMany(Role::class, 'role_user', 'user_id');
    }
    
    public function departments()
    {
        return $this->belongsToMany(Department::class, 'doctor_departments', 'doctor_id', 'department_id')->withTimestamps();
        
    }

    public function appointments()
    {
        return $this->hasMany(Appointment::class);
    }
}
