<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class Paymentmode extends Model
{
  public function otherprocedures()
  {
  	return $this->hasMany(Otherprocedure::class);
  }
  public function payments()
  {
  	return $this->hasMany(Payment::class,'mode_of_payment');
  }
}
