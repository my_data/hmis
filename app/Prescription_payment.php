<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Prescription_payment extends Model
{
   public function patient()
   {
   	return $this->belongsTo(patient::class);
   }
   public function prescription()
   {
   	return $this->belongsTo(Prescription::class);
   }
}
