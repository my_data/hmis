<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Imaging extends Model
{
       public function diagnosis()
    {
        return $this->belongsTo(Diagnosis::class);
    }

      public function patient()
    {
        return $this->belongsTo(User::class, 'patient_id');
    }
}
