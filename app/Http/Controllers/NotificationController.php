<?php

namespace App\Http\Controllers;

use Carbon\Carbon;

class NotificationController extends Controller
{
    public function unread()
    {
        $notifications = auth()->user()->unreadNotifications;
        return view('notifications.unread', compact('notifications'));
    }

    public function destroy($id)
    {
        $notification = auth()->user()->notifications()->where('id', $id)->firstOrFail();

        $notification->read_at = Carbon::now();

        $notification->save();

        return back();
    }
}
