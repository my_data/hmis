<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Diagnosis;
use App\Appointment;
use App\Diagnosis_payment;
use App\Otherprocedure;
use App\Patient_lab_tests;
use App\Payment;
use App\Imaging;
use App\Paymentmode;
use App\Patient;
use DB;
class Diagnosis_paymentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request,$patient,Appointment $appointment,Diagnosis $diagnosis)
    {
        $patient = Patient::find($patient);
        $paymode = $patient->payments->where('type_of_payment','Consultation')->where('appointment_id',$appointment->id)->first()->mode_of_payment;
        $pmode = Paymentmode::find($paymode);

        $this->validate($request, [
            'lab_testId' => 'required',
        ]);
        //save the new patient labtest
        $labtest = new patient_lab_tests();
        $labtest->lab_testId = $request->input('lab_testId');
        $labtest->diagnosis_id = $diagnosis->id;
        if($pmode->name=="CASH"){$labtest->paid=0;}else{$labtest->paid=1;}
        $labtest->patient_id = $patient->id;
        $labtest->save();

        //save the labtest total payments
        if($pmode->name=="CASH"){
   $patient_lab_tests=Patient_lab_tests::wherediagnosis_id($diagnosis->id)->where('paid',false)->get();
        }else{
 $patient_lab_tests=Patient_lab_tests::wherediagnosis_id($diagnosis->id)->where('paid',true)->get();
        }
        $sum = 0;
        if($patient_lab_tests->count() >0){
          foreach ($patient_lab_tests as $patient_lab_test) {
          $sum += $patient_lab_test->labTest->price;
        }
          if($pmode->name=="CASH"){
       $diagnosis_payments = Diagnosis_payment::wherediagnosis_id($diagnosis->id)->wherecategory("Laboratory investigations")->wherepaid(0)->latest()->first();
          }else{
          $diagnosis_payments = Diagnosis_payment::wherediagnosis_id($diagnosis->id)->wherecategory("Laboratory investigations")->wherepaid(1)->latest()->first();
          }
        if($diagnosis_payments!=null)
        {
        $diagnosis_payments->amount = $sum;
        $diagnosis_payments->category = 'Laboratory investigations';
        $diagnosis_payments->save();
        }else{
         $diagnosis_payments = new Diagnosis_payment();
        $diagnosis_payments->diagnosis_id=$diagnosis->id;
        $diagnosis_payments->category = 'Laboratory investigations';
        $diagnosis_payments->amount = $sum;
        if($pmode->name=="CASH"){$diagnosis_payments->paid=0;}else{$diagnosis_payments->paid=1;}
        $diagnosis_payments->save();
        }
        //return $diagnosis_payments;
        }
         if($pmode->name=="CASH"){
        $diagnosis_payments=Diagnosis_payment::wherediagnosis_id($diagnosis->id)->wherepaid(0)->get();
        $payments = Payment::whereAppointment_id($appointment->id)->wherestatus("0")->latest()->first();
    }else{ 
    	$payments = Payment::whereAppointment_id($appointment->id)->wherestatus("1")->latest()->first();
   $diagnosis_payments=Diagnosis_payment::wherediagnosis_id($diagnosis->id)->wherepaid(1)->get(); 
    }
        $total= $diagnosis_payments->sum('amount');
          if($payments !=null)
                    {
                       
          if($pmode->name=="CASH")
        {

                
                $payments->patient_id = $patient->id;
                $payments->type_of_payment = 'Diagnosis cost';
                $payments->amount = $total;
                $payments->appointment_id = $appointment->id;
                $payments->status ="0";
                $payments->save();
         }else{
         	    if($payments->type_of_payment=="Consultation"){
                                    $payments = new Payment();
                                    $payments->patient_id = $patient->id;
                                    $payments->type_of_payment = 'Diagnosis cost';
                                    $payments->amount = $total;
                                    $payments->mode_of_payment = $pmode->id;
                                    $payments->appointment_id = $appointment->id;
                                    $payments->status ="1";
                                    $payments->save();
         	    }else{
                $payments->patient_id = $patient->id;
                $payments->type_of_payment = 'Diagnosis cost';
                $payments->amount = $total;
                $payments->mode_of_payment = $pmode->id;
                $payments->appointment_id = $appointment->id;
                $payments->status ="1";
                $payments->save();
         	    }
         }
                    }else{

                         if($total > 0){
                            if($pmode->name=="CASH")
                            {

                                    $payments = new Payment();
                                    $payments->patient_id = $patient->id;
                                    $payments->type_of_payment = 'Diagnosis cost';
                                    $payments->amount = $total;
                                    $payments->appointment_id = $appointment->id;
                                    $payments->status ="0";
                                    $payments->save();
                             }else{
                                    $payments = new Payment();
                                    $payments->patient_id = $patient->id;
                                    $payments->type_of_payment = 'Diagnosis cost';
                                    $payments->amount = $total;
                                    $payments->mode_of_payment = $pmode->id;
                                    $payments->appointment_id = $appointment->id;
                                    $payments->status ="1";
                                    $payments->save();
                             }
                             }
                        }  
    flash('Patient investigation is saved successifully..!')->success()->important();
    return redirect()->back();
       }

    public function storeprescriptioncost($pat,$diagnosis)
    {
      return Diagnosis_payment::all();
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
