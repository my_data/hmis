<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Paymentmode;
use App\Hospitalservice;
use App\Service_category;
use DB;
class HospitalservicesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
    $serviceId=Service_category::wherename('SERVICE AND PROCEDURE')->first();
      $services = Hospitalservice::whereService_category_id( $serviceId)->latest()->get();
        $paymentmodes = Paymentmode::all();
       
      return view('hospital-services.index',compact('services','paymentmodes'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
    $paymentmodes = Paymentmode::all();
     return view('hospital-services.create',compact('paymentmodes'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       $serviceId=Service_category::wherename('SERVICE AND PROCEDURE')->first();
        $this->validate($request,[
           'name'=>'required',
           'price'=>'required|numeric',
           'paymentmode'=>'required'
            ]);
      $services = new Hospitalservice();
      $services->service_category_id=$serviceId;
      $services->name = $request->input('name');
      $services->price = $request->input('price');
      $services->paymentmode_id = $request->input('paymentmode');
      $services->save();
      flash($services->name.' is saved successifully')->success()->important();
      return redirect('/hospitalservices');
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
    return 'this is show file';
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)

    {
      $paymentmodes = Paymentmode::all();
      $service = Hospitalservice::find($id);
      return view('hospital-services.edit',compact('paymentmodes','service'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
       $this->validate($request,[
           'name'=>'required',
           'price'=>'required|numeric',
           'paymentmode'=>'required'
            ]);
      $services = Hospitalservice::find($id);
      $services->name = $request->input('name');
      $services->price = $request->input('price');
      $services->paymentmode_id = $request->input('paymentmode');
      $services->save();
      flash($services->name.' is saved successifully')->success()->important();
      return redirect('/hospitalservices'); 
    }
    public function upload(Request $request)
    {

      $serviceId=Service_category::wherename('SERVICE AND PROCEDURE')->first(); 
      $paymentmodes=Paymentmode::all();
      // load the rows
        $collections = \Excel::load($request->file('user_file'), function ($reader) {
        })->get();
        $count = 0;
        $is_sheets=0;
  foreach ($collections as $collection) {
     foreach ($collection as $coll) {
    if (is_object($coll)) {
  $is_sheets=1;
    }else
    {
        $is_sheets=0;
    }
}
}
if($paymentmodes->isNotEmpty())
{
if($is_sheets==1)
{
    $index = $collections->count();
     for($i=0;$i< 1;$i++)
     {
      foreach ($collections[$i] as $row) {
      $services = new Hospitalservice();
      $services->service_category_id=$serviceId;
      $services->name = $row->name;
      $services->price = $row->price;
      $services->paymentmode_id = $request->input('paymentmode_id');
      $services->save();
            $count++;
     }
 }
}if($is_sheets==0){
   foreach ($collections as $row) {
      $services = new Hospitalservice();
      $services->service_category_id=$serviceId;
      $services->name = $row->name;
      $services->price = $row->price;
     $services->paymentmode_id = $request->input('paymentmode_id');
      $services->save();
            $count++;
        }   
}
}else{
flash('You can not upload hospital services and procedures while payment modes is empty,please fill payment modes first to complete process')->warning()->important();
return back();
}
        flash($count . ' items uploaded successfully.')->success()->important();

        return redirect('/hospitalservices');
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
         $service = Hospitalservice::findOrfail($id);
        $service->delete();
        flash('The payment mode ' . $service->name . ' was deleted successifully')->success()->important();
          return back();
    }
}
