<?php

namespace App\Http\Controllers;

use App\PaymentType;
use Illuminate\Http\Request;

class PaymentTypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $paymentTypes = PaymentType::latest()->get();
        return view('payment-types.index', compact('paymentTypes'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('payment-types.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|max:255',
            'amount' => 'required|numeric'
        ]);
        $payment_type = new PaymentType();
        $payment_type->name = $request->input('name');
        $payment_type->amount = $request->input('amount');
        $payment_type->display_name = $request->input('name');
        $payment_type->save();
        return redirect('payment-types');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\PaymentType $paymentType
     * @return \Illuminate\Http\Response
     */
    public function show(PaymentType $paymentType)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param $id
     * @return \Illuminate\Http\Response
     * @internal param PaymentType $paymentType
     */
    public function edit($id)
    {
        $paymentType = PaymentType::findOrFail($id);

        return view('payment-types.edit', compact('paymentType'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param $id
     * @return \Illuminate\Http\Response
     * @internal param PaymentType $paymentType
     */
    public function update(Request $request, $id)
    {
        $payment_type = PaymentType::findOrFail($id);

        $this->validate($request, [
            'name' => 'required|max:255',
            'amount' => 'required|numeric'
        ]);

        $payment_type->name = $request->input('name');
        $payment_type->amount = $request->input('amount');
        $payment_type->display_name = $request->input('name');
        $payment_type->save();

        flash('Updated successfully.')->success()->important();

        return redirect('payment-types');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param $id
     * @return \Illuminate\Http\Response
     * @internal param PaymentType $paymentType
     */
    public function destroy($id)
    {
        $paymentType = PaymentType::findOrFail($id);

        $paymentType->delete();

        flash('Deleted successfully')->success()->important();

        return back();
    }
}
