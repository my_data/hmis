<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Appointment;
use App\Doctor;
use App\Role;
use App\Payment;
use Yajra\DataTables\DataTables;

class ReportController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $doctorIds = Role::whereName('doctor')->first()->users->pluck('id'); 
        $doctors = Doctor::with('appointments')->find($doctorIds);
       // dd($doctors);
        $appointmentIds = Appointment::whereIn('doctor_id',$doctorIds)->pluck('id');
        $totalPatients = Appointment::whereIn('doctor_id',$doctorIds)->count();
        $totalPayments = Payment::whereIn('appointment_id',$appointmentIds)->sum('amount');
        return view('reports.index',compact('totalPayments','totalPatients'));
    }

    public function getJsonReports(){
        $doctorIds = Role::whereName('doctor')->first()->users->pluck('id');
        $doctors = Doctor::with('appointments')->whereIn('id',$doctorIds);
        //dd($doctors);
       // $appointmentIds = Appointment::whereIn('doctor_id',$doctorIds)->pluck('id');
      //  $totalPatients = Appointment::whereIn('doctor_id',$doctorIds)->count();
      //  $totalPayments = Payment::whereIn('appointment_id',$appointmentIds)->sum('amount');


        return DataTables::of($doctors)
            ->addIndexColumn()
            ->addColumn('doctor_name',function ($doctor){
                return  link_to('/doctors/'. $doctor->id,  'Dr.'. $doctor->first_name .' '. $doctor->middle_name. ' '.$doctor->last_name );
            })
            ->addColumn('patients',function ($doctor){
                return number_format( $doctor->appointments->count()) ;
            })
            ->addColumn('amount',function ($doctor){
                $totalamount=0;
                foreach($doctor->appointments as $appointment){
                    $totalamount += $appointment->payments->where('status',1)->sum('amount');

                }
                return number_format( $totalamount) ;
            })
                ->addColumn('action',function ($doctor){
                    return html_entity_decode(link_to('#',' <i class="fa fa-eye"></i>&nbsp;More details') );
                })
           ->escapeColumns('action')
            ->make(true);


    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

     
      $start = $request->input("from");
      $end = $request->input("to");

      if(!empty($start) && !empty($end))
      {
        $from = $start.' 00:00:00';
        $to = $end.' 00:00:00';
        $doctorIds = Role::whereName('doctor')->first()->users->pluck('id'); 
        $doctors = Doctor::with(['appointments'=>function($query) use ($from,$to){

            $query->whereDate('created_at', '>=', $from)->whereDate('created_at', '<=', $to);

        }])->whereIn('id',$doctorIds)->get();
        $appointmentIds = Appointment::whereDate('created_at', '>=', $from)->whereDate('created_at', '<=', $to)->whereIn('doctor_id',$doctorIds)->pluck('id');
        $totalPatients = Appointment::whereDate('created_at', '>=', $from)->whereDate('created_at', '<=', $to)->whereIn('doctor_id',$doctorIds)->count();
        $totalPayments = Payment::whereDate('created_at', '>=', $from)->whereDate('created_at', '<=', $to)->whereIn('appointment_id',$appointmentIds)->where('status',"1")->sum('amount');
        return view('reports.index',compact('doctors','totalPayments','totalPatients','from','to'));

      }else{
        $doctorIds = Role::whereName('doctor')->first()->users->pluck('id'); 
        $doctors = Doctor::with('appointments')->find($doctorIds);
        $appointmentIds = Appointment::whereIn('doctor_id',$doctorIds)->pluck('id');
        $totalPatients = Appointment::whereIn('doctor_id',$doctorIds)->count();
        $totalPayments = Payment::whereIn('appointment_id',$appointmentIds)->sum('amount');
        return view('reports.index',compact('doctors','totalPayments','totalPatients'));
      }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
