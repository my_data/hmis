<?php

namespace App\Http\Controllers;

use DB;
use App\Drug;
use App\Patient;
use App\Diagnosis;
use App\Assigndrug;
use App\Prescription;
use App\Appointment;
use Illuminate\Http\Request;

class PrescriptionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param Diagnosis $diagnosis
     * @return \Illuminate\Http\Response
     */
    public function index(Patient $patient, Appointment $appointment,Diagnosis $diagnosis)
    {
        $prescriptions = $diagnosis->prescriptions()->latest()->get();
        return view('patients.diagnoses.prescriptions.index', compact('prescriptions','patient', 'diagnosis','appointment'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Patient $patient,Appointment $appointment,$id)
    {
        $drugs = Prescription::all();
        $diagnoses= Diagnosis::whereid($id)->first();
    return view('patients.diagnoses.prescriptions.create',compact('patient','diagnoses','appointment'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request,$patientId,Appointment $appointment,$diagnosisId)
    {

        $this->validate($request,[
             'disease'=>'required',
        ]);

        $doctor = auth()->user();
        
        $prescription = new Prescription();
        $prescription->disease = $request->input('disease');
        $prescription->description = $request->input('body');
        $prescription->patient_id = $patientId;
        $prescription->diagnosis_id = $diagnosisId;
        $prescription->doctor_id = $doctor->id;
        $prescription->save();

        return redirect("patients/".$patientId."/appointment/".$appointment->id."/diagnoses/".$diagnosisId."/prescriptions/".$prescription->id."/create/assign-drugs");   
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Prescription $prescription
     * @return \Illuminate\Http\Response
     */
    public function show(Patient $patient, Appointment $appointment,Diagnosis $diagnosis,Prescription $prescription)
    {
        $assigndrugs = Assigndrug::with('drugs')->whereHas('drugs')->where('prescription_id', $prescription->id)->get();
          
        return view('patients.diagnoses.prescriptions.show',compact('patient','diagnosis','prescription','assigndrugs','appointment'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Prescription $prescription
     * @return \Illuminate\Http\Response
     */
    public function edit(Prescription $prescription)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Prescription $prescription
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Prescription $prescription)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Prescription $prescription
     * @return \Illuminate\Http\Response
     */
    public function destroy(Prescription $prescription)
    {
        //
    }
}
