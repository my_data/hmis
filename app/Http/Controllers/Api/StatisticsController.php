<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class StatisticsController extends Controller
{
    public function payments(Request $request)
    {
        $query = DB::table('payments')
            ->select(DB::RAW("SUM(amount) as total, CONCAT(MONTHNAME(created_at), ' ', DATE_FORMAT(created_at, '%D, %Y %H:%i:%s')) as day, created_at"));

        if ($request->has('start') && $request->has('end')) {
            $start = Carbon::parse(request('start'));
            $end = Carbon::parse(request('end'))->addDay(1);

            $query->where('created_at', '>', $start)->where('created_at', '<=', $end);
        }

        $payments = $query->groupBy('day')
            ->groupBy('created_at')
            ->orderBy('created_at', 'asc')
            ->get()->toArray();

        return $payments;
    }
}
