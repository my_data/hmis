<?php

namespace App\Http\Controllers;

use DB;
use Auth;
use App\Role;
use App\Patient;
use App\Payment;
use App\Diagnosis;
use App\PaymentType;
use App\Diagnosis_payment;
use App\Paymentmode;
use App\Appointment;
use Illuminate\Http\Request;
use App\Prescription_payment;
use App\Notifications\LabtestAssigned;

class PatientpaymentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($patId)
    {

        $pmode = Payment::where('patient_id', $patId)->where('type_of_payment', 'consultation')->latest()->first()->mode_of_payment;
        $paymentmode  =Paymentmode::findOrFail($pmode);
          if($paymentmode==null){
            $paymentmode = 'CASH';
        }
        $patient = Patient::latest()->with(['payments' => function ($query) {
            return $query->latest();
        }])->findOrFail($patId);

        return view('patients.payments.index', compact('patient','paymentmode'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Patient $patient)
    {
        $paymentTypes = PaymentType::orderBy('name')->get();
        $amount = PaymentType::get();
        return view('patients.payments.create', compact('patient', 'amount', 'paymentTypes'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Patient $patient)
    {
        return 'here';
        $this->validate($request, [
            'payment_type_id' => 'required',
            'payby' => 'required',
        ]);

        $payments = new Payment();
        $payments->patient_id = $patient->id;
        $payments->staff_id = Auth::user()->id;
        $payments->type_of_payment = $payment_type;
        $payments->status = "1";
        $payments->mode_of_payment = $request->input('payby');
        $payments->amount = $amount;
        $payments->save();
        flash('New payment have added successiful')->success()->important();

        return redirect('patients/' . $patient->id . '/payments');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($patId,Appointment $appointment)
    {
        $pmode = Payment::where('patient_id', $patId)->where('type_of_payment', 'consultation')->latest()->first()->mode_of_payment;
        $paymentmode  =Paymentmode::find($pmode);
        $patient = Patient::latest()->with(['payments' => function ($query) use($appointment) {
            return $query->whereappointment_id($appointment->id)->latest();
        }])->findOrFail($patId);
        
        return view('patients.payments.index', compact('patient','paymentmode','appointment'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
        public function edit(Request $request, Patient $patient,Appointment $appointment,$payment)
    {
   
        $payment = Payment::find($payment);
        $appointment = Appointment::findOrFail($appointment->id);
        $diagnosis = Diagnosis::whereAppointment_id($appointment->id)->latest()->first();

        $payType = Payment::whereid($payment->id)->first()->type_of_payment;

        $pmode = Payment::where('patient_id', $patient->id)->where('type_of_payment', 'consultation')->latest()->first()->mode_of_payment;
        
        if ($payType == 'Prescription cost') {
            DB::table('prescription_payments')
                ->where('patient_id', $patient->id)
                ->update(['paid' => true]);
        }else{
             DB::table('diagnosis_payments')
            ->where('diagnosis_id', $diagnosis->id)
            ->update(['paid' => true]);

             DB::table('imagings')
            ->where('diagnosis_id', $diagnosis->id)
            ->update(['paid' => true]);

            DB::table('patient_lab_tests')
            ->where('diagnosis_id', $diagnosis->id)
            ->update(['paid' => true]);

            DB::table('otherprocedures')
            ->where('diagnosis_id', $diagnosis->id)
            ->update(['paid' => true]);
            
            DB::table('payments')
            ->where('id', $payment->id)
            ->update(['status' => '1', 'mode_of_payment' => $pmode]);
             }

    
        // Fetch all lab attendants
        $laboratoryAttendants = Role::whereName('laboratory-attendant')->first()->users;

        // Notify all
        foreach ($laboratoryAttendants as $laboratoryAttendant) {
            $laboratoryAttendant->notify(new LabtestAssigned());
        }

        flash('Success: Confirmed');


        return back();
    }

    public function viewdetails(Patient $patient, Appointment $appointment,Payment $payment)
    {

        $type_of_payment = $payment->type_of_payment;

        if ($type_of_payment == "Diagnosis cost") {

            $diagnosisId = Diagnosis::wherepatient_id($patient->id)->pluck('id');
            
            $diagnosis_payments = Diagnosis_payment::whereIn('diagnosis_id',$diagnosisId)->get();

            return view('patients.payments.diagnosis_payment', compact('diagnosis_payments',
                'patient','appointment'));

        } else if ($type_of_payment == 'Prescription cost') {

            $prescription_payments = Prescription_payment::wherepatient_id($patient->id)->get();
            return view('patients.payments.prescription_payments', compact('prescription_payments','appointment',
                'patient'));
        } else {
            return back();
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
