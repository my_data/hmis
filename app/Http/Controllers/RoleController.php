<?php

namespace App\Http\Controllers;

use App\Role;
use App\User;
use Illuminate\Http\Request;

class RoleController extends Controller
{
    public function store(Request $request)
    {
        $user = User::where('id',$request['id'])->first();
        $user->roles()->detach();
        if($request['role_admin']){
          $user->roles()->attach(Role::where('name','admin')->first());  
        }
            if($request['role_doctor']){
          $user->roles()->attach(Role::where('name','doctor')->first());  
        }
            if($request['role_receptionist']){
          $user->roles()->attach(Role::where('name','receptionist')->first());  
        }
        if($request['role_laboratory-attendant']){
          $user->roles()->attach(Role::where('name','laboratory-attendant')->first());  
        }
            if($request['role_pharmacist']){
          $user->roles()->attach(Role::where('name','pharmacist')->first());  
        }
        return redirect('/users');
    }
}
