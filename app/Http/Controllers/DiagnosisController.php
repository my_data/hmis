<?php

namespace App\Http\Controllers;

use App\Role;
use App\Test;
use App\Patient;
use App\Diagnosis;
use App\Payment;
use App\Appointment;
use App\patient_lab_tests;
use App\PaymentType;
use App\Imaging;
use App\Paymentmode;
use App\Service_category;
use App\Otherprocedure;
use Illuminate\Http\Request;
use Auth;

class DiagnosisController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param $patientId
     * @return \Illuminate\Http\Response
     * @internal param Patient $patient
     */
    public function index(Patient $patient,Appointment $appointment)
    {
        
        /*$patient = Patient::with(['diagnoses' => function ($query) use($appointment) {
            return $query->where('appointment_id',$appointment->id)->latest();
        }])->findOrFail($patientId);*/
        $patient = Patient::with(['diagnoses'=>function($query){
            return $query->latest();
        }])->find($patient->id);
        $diagnosis = Diagnosis::wherepatient_id($patient->id)->whereappointment_id($appointment->id)->latest()->first();
        return view('patients.diagnoses.index', compact('patient','appointment','diagnosis'));
    }

    public function imaging(Patient $patient,Appointment $appointment)
    {
           $diagnosis = Diagnosis::wherepatient_id($patient->id)->whereappointment_id($appointment->id)->latest()->first();
       
         return view('patients.diagnoses.imaging.index', compact('patient','appointment','diagnosis'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param Patient $patient
     * @return \Illuminate\Http\Response
     */
    public function create(Patient $patient,Appointment $appointment)
    {
        $tests = test::all();
        return view('patients.diagnoses.create', compact('patient', 'tests','appointment'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param Patient $patient
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Patient $patient,Appointment $appointment)
    {
        $this->validate($request, [
            'provisional_diagnosis' => 'required',
        ]);
        //save the new diagnosis
        $diagnosis = new Diagnosis();
        $diagnosis->patient_id = $patient->id;
        $diagnosis->doctor_id = auth()->id();
        $diagnosis->appointment_id = $appointment->id;
        $diagnosis->summary = $request->input('physical_examination');
        $diagnosis->body = $request->input('history');
        $diagnosis->provisional_diagnosis= $request->input('provisional_diagnosis');
        $diagnosis->patient_id = $patient->id;
        $diagnosis->save();

         $appointment = Appointment::find($appointment->id);
         $appointment->did_visit_doctor = true;
         $appointment->save();
        return redirect('patients/' . $patient->id . '/appointments/'.$appointment->id.'/manage');
         
    }
    public function cancel(Request $request, Patient $patient,Appointment $appointment,Diagnosis $diagnosis)
    {
     $diagnosis = Diagnosis::find($diagnosis->id);
     $diagnosis->delete();
      return redirect('/patients/'.$patient->id.'/appointment/'.$appointment->id.'/diagnoses/new');
    }
    public function createLabtest(Patient $patient, Appointment $appointment,$diagnosisId)
    {

        $pmode = Payment::where('patient_id', $patient->id)->where('type_of_payment', 'consultation')->latest()->first()->mode_of_payment;
        $paymode = Paymentmode::find($pmode);

        $serviceId=Service_category::wherename('LAB-TEST')->first();
        if($paymode->name=="CASH"){
            $diagnosis = Diagnosis::with(['labTests'=>function($query){
            return $query->wherepaid(0)->latest();
        }])->findOrFail($diagnosisId);
        }else{
            $diagnosis = Diagnosis::with(['labTests'=>function($query){
            return $query->wherepaid(1)->latest();
        }])->findOrFail($diagnosisId);
        }

        $labTests = Test::whereservice_category_id($serviceId)->wherepaymentmode_id($pmode)->get();

        return view('patients.diagnoses.laboratory-tests.create', compact('patient', 'diagnosis', 'labTests','appointment','paymode'));
    }

    public function createOtherProcedures(patient $patient, Diagnosis $diagnosis)
    {

        $paymentTypes = PaymentType::all();
        return view('patients.diagnoses.other-procedures.create', compact('patient', 'paymentTypes', 'diagnosis'));
    }

    /**
     * @param Request $request
     * @param $patientid
     * @param $diagnosis_id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function removeLabTest(Request $request, Patient $patient, Diagnosis $diagnosis)
    {
        $labTestId = $request->input('labTest');
        $labTest = patient_lab_tests::find($labTestId);

        if (!$labTest) {
            flash('Failed to remove the lab test')->danger();
            return back();
        }

        $labTest->delete();

        flash('Success: Removed.');

        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param Patient $patient
     * @param Diagnosis $diagnosis
     * @return \Illuminate\Http\Response
     * @internal param $diagnosisId
     * @internal param Diagnosis $diagnosis
     */
    public function show(Patient $patient, Appointment $appointment,Diagnosis $diagnosis)
    {
        $procedures = $diagnosis->otherprocedures;
        return view('patients.diagnoses.show', compact('patient', 'diagnosis','appointment','procedures'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Diagnosis $diagnosis
     * @return \Illuminate\Http\Response
     */
    public function edit(Diagnosis $diagnosis)
    {
        //
    }
   
    public function showImaging(Patient $patient, Appointment $appointment,Diagnosis $diagnosis,Imaging $imaging)
    {
        return view('patients.diagnoses.imaging.show', compact('patient', 'diagnosis','appointment','imaging'));
    }
    public function showattachment(Patient $patient, Appointment $appointment,Diagnosis $diagnosis,Imaging $imaging)
    {

         $attachment = storage_path('app/' . $imaging->attachment);
         
         if($attachment != null)
         {
           return response()->file($attachment);
         }else{
            return back();
         }
         
    }


  public function showLab(Patient $patient, Appointment $appointment,Diagnosis $diagnosis,patient_lab_tests $laboratory_test)
    {
        
        return view('patients.diagnoses.laboratory-tests.show', compact('patient', 'diagnosis','appointment','laboratory_test'));
    } 
    public function showprocedure(Patient $patient, Appointment $appointment,Diagnosis $diagnosis,Otherprocedure $procedure)
    {
        
        return view('patients.diagnoses.other-procedures.show', compact('patient', 'diagnosis','appointment','procedure'));
    }
    public function showLabattachment(Patient $patient, Appointment $appointment,Diagnosis $diagnosis,patient_lab_tests $laboratory_test)
    {
         $attachment = storage_path('app/' .$laboratory_test->attachment);
         
         return response()->file($attachment);
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Diagnosis $diagnosis
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Diagnosis $diagnosis)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Diagnosis $diagnosis
     * @return \Illuminate\Http\Response
     */
    public function destroy(Diagnosis $diagnosis)
    {
        //
    }
}
