<?php

namespace App\Http\Controllers;

use App\Diagnosis;
use App\Otherprocedure;
use App\Patient;
use App\Hospitalservice;
use App\Payment;
use App\Appointment;
use App\Service_category;
use App\PaymentMode;
use Illuminate\Http\Request;
use DB;
use Illuminate\Support\Facades\Input;
use Response;
class OtherproceduresController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index(Patient $patient,Appointment $appointment)
    {
          $diagnosis = Diagnosis::wherepatient_id($patient->id)->whereappointment_id($appointment->id)->latest()->first();
        return view('patients.diagnoses.other-procedures.index',compact('patient','appointment','diagnosis'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Patient $patient, Appointment $appointment,Diagnosis $diagnosis)
    {
        $diagnosis = Diagnosis::find($diagnosis->id);
        if($diagnosis !=null){
     $this->patient_id=$patient->id;

        $pmode = Payment::where('patient_id', $patient->id)->where('type_of_payment', 'consultation')->latest()->first()->mode_of_payment;
        $serviceId=Service_category::wherename('SERVICE AND PROCEDURE')->first();

      $services = Hospitalservice::whereservice_category_id($serviceId)->wherepaymentmode_id($pmode)->get();
        $paymentModes = \App\Paymentmode::orderBy('name')->get();

        $patient = Patient::whereid($patient->id)->first();
        return view('patients.diagnoses.other-procedures.create', compact('patient', 'paymentModes','services','appointment','diagnosis'));
        }else{
         return redirect('patients/'.$patient->id.'/appointment/'.$appointment->id.'/diagnoses/otherprocedures');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $patient,Appointment $appointment,Diagnosis $diagnosis)
    {
 
    }

    public function store2(Request $request, $patient, Appointment $appointment,Diagnosis $diagnosis)
    {
        $this->validate($request, [
            'description' => 'required',
            'price' => 'required|numeric',
        ]);


        $otherprocedure = new Otherprocedure();
        $otherprocedure->name = $request->input('servicename');
        $otherprocedure->name = 'Other Procedures and Services';
        $otherprocedure->diagnosis_id = $diagnosis->id;
        $otherprocedure->procedure_category_id = 0;
        if($pmode->name=="CASH"){  $otherprocedure->paid = false;}else{  $otherprocedure->paid = false;}
        $otherprocedure->description = $request->input('description');
        $otherprocedure->save();
        


        // Save in payment
        $payment = new \App\Payment;
        $payment->patient_id = $patient;
        $payment->staff_id = auth()->id();
        $payment->appointment_id = $appointment->id;
        $payment->type_of_payment = 'Procedures and Services';
        $payment->mode_of_payment = $request->input('paymentMode');
        $payment->amount = $request->input('price');
        $payment->save();
        

        flash("Procedure submitted successifully")->success()->important();
        
        return back();

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Otherprocedures $otherprocedures
     * @return \Illuminate\Http\Response
     */
    public function show(Otherprocedures $otherprocedures)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Otherprocedures $otherprocedures
     * @return \Illuminate\Http\Response
     */
    public function edit(Otherprocedures $otherprocedures)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Otherprocedures $otherprocedures
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Otherprocedures $otherprocedures)
    {
        //
    }
    public function loadAjaxdata(Request $request)
    {

     $patient_id = Input::get('patient_id');
     $pmode = Payment::where('patient_id', $patient_id)->where('type_of_payment', 'consultation')->latest()->first()->mode_of_payment;
        $serviceId=Service_category::wherename('SERVICE AND PROCEDURE')->first();
    $term = Input::get('query');

    $results = array();
    
    $queries = Hospitalservice::whereservice_category_id($serviceId)
                                ->wherepaymentmode_id($pmode)       
                               ->where('name','LIKE','%'.$term.'%')
                              ->take(5)->get();
    foreach ($queries as $query)
    {
        $results[] = ['id'=>$query->id,'value' => $query->name,'price'=>$query->price ];
    }
return Response::json($results);
    }
   public function loadAjaxprocedurecost(Request $request)
    {
        $patient_id = $request->get('patient_id');
        if(!$request->has('id')){
            return Response::json([
                'status' => 'error',
                'message' => 'You did not supply id',
            ]);
        }

        $id = $request->only('id')['id'];

        return $hospitalService = Hospitalservice::find($id);

        if(!$hospitalService){
            return Response::json([
                'status' => 'error',
                'message' => 'Not Found'
            ]);
        }

        //return $hospitalService;

    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Otherprocedures $otherprocedures
     * @return \Illuminate\Http\Response
     */
    public function destroy(Otherprocedures $otherprocedures)
    {
        //
    }
}
