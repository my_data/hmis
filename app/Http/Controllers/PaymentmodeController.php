<?php

namespace App\Http\Controllers;

use App\Paymentmode;
use Illuminate\Http\Request;

class PaymentmodeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $paymentmodes = Paymentmode::latest()->get();
        return view('paymentmodes.index', compact('paymentmodes'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('paymentmodes.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'consultationfee'=>'required|numeric'
        ]);
        $paymentmodes = new Paymentmode();
        $paymentmodes->name = $request->input('name');
        $paymentmodes->consultation_fee=$request->input('consultationfee');
        $paymentmodes->save();
        flash('Payment mode added successifully')->success()->important();

        return redirect('payment-modes');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $paymentmode = Paymentmode::findOrfail($id);
        return view('paymentmodes.edit', compact('paymentmode'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required',
            'consultationfee' => 'required',
        ]);
        $paymentmode = Paymentmode::findOrfail($id);
        $paymentmode->name = $request->input('name');
        $paymentmode->consultation_fee = $request->input('consultationfee');
        $paymentmode->save();
        flash('the payment mde updated successifully')->success()->important();
        return redirect('payment-modes');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $paymentmodes = Paymentmode::findOrfail($id);
        $paymentmodes->delete();
        flash('The payment mode ' . $paymentmodes->name . ' was deleted successifully')->success()->important();
        return back();
    }
}
