<?php

namespace App\Http\Controllers;

use App\User;
use App\Role;
use App\Doctor;
use App\Department;
use App\PaymentType;
use Illuminate\Http\Request;

class DoctorController extends Controller
{
    public function index(User $user){
        $doctors =Role::where('name', 'doctor')->first()->users()->with('departments')->get();
        return view('doctors.index',compact('doctors'));
    }

    public function show(Doctor $doctor){

        $deptIds = $doctor->departments()->pluck('department_id');

        $departments = Department::whereNotIn('id', $deptIds)->orderBy('name')->get();
        
        return view('doctors.show', compact('doctor', 'departments'));   
    }
    public function store(Request $request, Doctor $doctor)
    {    
        $this->validate($request, [
            'department' => 'required',
        ]);

        $department = Department::findOrFail($request->input('department'));
        $doctor->departments()->attach($department);

        return back();
    }

    public function removeDepartment(Request $request, $doctorId, $departmentId)
    {
        $doctor = Doctor::findOrFail($doctorId);
        $department = Department::findOrFail($departmentId);

        $doctor->departments()->detach($department);

        flash('Success');

        return back();
    }
}
