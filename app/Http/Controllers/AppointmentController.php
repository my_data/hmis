<?php

namespace App\Http\Controllers;

use App\Appointment;
use App\Doctor;
use App\Notifications\AppointmentMade;
use App\Patient;
use App\Payment;
use App\Department;
use App\PaymentType;
use App\department_doctor;
use App\Paymentmode;
use App\Role;
use DB;
use Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Yajra\DataTables\DataTables;



class AppointmentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param Patient $patient
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request,Patient $patient)
    {

       $allPayments = Payment::get();
       $appointments = Appointment::wherepatient_id($patient->id)->latest()->paginate(20);
       return view('patients.appointments.index', compact('patient','appointments','allPayments'));
    }
    public function appointments(){
          //  $appointments = Appointment::latest()->paginate(7);


        return view('appointments.index');
    }
    public function getJsonAppointments(){
        $appointments = Appointment::latest();
       // dd($appointments);

        return DataTables::of($appointments)
            ->addIndexColumn()
            ->addColumn('patient_name',function($app){
                //dd($appointment->patient->id);
                return link_to("/patients/".$app->patient["id"],$app->patient['first_name'].' '. $app->patient['middle_name'].''. $app->patient['last_name']);// html_entity_decode('<a href="/patients/'.$app->patient['id'].'">'.$app->patient['first_name'].' '. $app->patient['middle_name'].''. $app->patient['last_name'].'</a');
            })
            ->addColumn('doctor_name',function($app){
                return link_to("/doctors/".$app->doctor["id"],$app->doctor['first_name'].' '. $app->doctor['middle_name'].''. $app->doctor['last_name']);// html_entity_decode('<a href="/patients/'.$app->patient['id'].'">'.$app->patient['first_name'].' '. $app->patient['middle_name'].''. $app->patient['last_name'].'</a');

              //  return ' <a href="/doctors/'.$app->doctor['id'].'">'.$app->doctor['first_name'].' '. $app->doctor['middle_name'].''. $app->doctor['last_name'].'</a';

            })
            ->addColumn('created_at',function($app){
                return     $app->created_at->toDayDateTimeString();
            })

            ->make(true);
    }

  public function unattended()
  {
        $appointments = collect([]);
        if(auth()->user()->hasRole('doctor')){

    $appointments = Appointment::where('doctor_id', auth()->id())->where("did_visit_doctor",false)->latest()->get();

    return view('appointments.unattended',compact('appointments'));
}
  }
    /**
     * Show the form for creating a new resource.
     *
     * @param Patient $patient
     * @return \Illuminate\Http\Response
     */
    public function create(Patient $patient)
    {
        $paymentmodes = Paymentmode::all();
        $departments = Department::all();
        $doctors = Role::where('name', 'doctor')->first()->users;

        return view('appointments.create', compact('patient', 'doctors', 'departments','paymentmodes'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param Patient $patient
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Patient $patient)
    {
        $this->validate($request, [
            'doctor' => 'required',
            'payby' => 'required',
        ]);

        $appointment = new Appointment();
        $appointment->patient_id = $patient->id;
        $appointment->doctor_id = $request->input('doctor');
        $appointment->description = $request->input('description');
        $appointment->save();

        $mode_of_payment = Paymentmode::findOrFail($request->input('payby'));

        // make Payment
        $payment = new Payment();
        $payment->patient_id = $patient->id;
        $payment->type_of_payment = 'Consultation';
        $payment->staff_id = Auth::user()->id;
        $payment->status = "1";
        $payment->appointment_id = $appointment->id;
        $payment->mode_of_payment = $mode_of_payment->id;
        $payment->amount = $mode_of_payment->consultation_fee;
        $payment->save();

        flash('Appointment was created successifully')->success()->important();
        
        return redirect('patients/' . $patient->id . '/appointments');
    }

    /**
     * Display the specified resource.
     *
     * @param Patient $patient
     * @param  \App\Appointment $appointment
     * @return \Illuminate\Http\Response
     */
    public function show(Patient $patient, Appointment $appointment)
    {
        
        return view('patients.appointments.show', compact('patient', 'appointment','allPayments'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Appointment $appointment
     * @return \Illuminate\Http\Response
     */
    public function edit(Appointment $appointment)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Appointment $appointment
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Appointment $appointment)
    {
        //
    }

    public function loadAjaxdata(Request $request)
    {
        $departments = DB::table('doctor_departments')
            ->join('users', 'doctor_departments.doctor_id', '=', 'users.id')->get();
        $data = $departments->where('department_id', $request->id);
        return response()->json($data);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Appointment $appointment
     * @return \Illuminate\Http\Response
     */
    public function destroy(Appointment $appointment)
    {
        //
    }
}
