<?php

namespace App\Http\Controllers;

use App\Diagnosis;
use App\Patient;
use App\Role;
use App\User;
use App\Doctor;
use App\Department;
use App\Appointment;
use App\Paymentmode;
use App\Patient_lab_tests;  
use Illuminate\Http\Request;

class PatientController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        if (empty($request->input('search'))) {
            //  $request->input('search');

            $patients = Role::where('name', 'patient')->first()->users()->orderBy('created_at', 'desc')->paginate(20);
        }
        else{
            $search=$request->input('search');
            $patients = Role::where('name', 'patient')->first()->users()
                ->where('users.id', 'LIKE', "%{$search}%")
                ->orWhere('first_name', 'LIKE', "%{$search}%")
                ->orWhere('middle_name', 'LIKE', "%{$search}%")
                ->orWhere('last_name', 'LIKE', "%{$search}%")
                ->orderBy('created_at', 'desc')->paginate(20);

        }
        $appointments = collect([]);
        if (auth()->user()->hasRole('doctor')) {
            if (empty($request->input('search'))) {
                $appointments = auth()->user()->patients()->latest()->paginate(20);
            }else{
                $search=$request->input('search');
               // dd($appointments = auth()->user()->with('patients')->latest()->get()->take(10));
                $appointments = auth()->user()->with(['patients'])->latest()
                    ->where('id', 'LIKE', "%{$search}%")
                    ->orWhere('first_name', 'LIKE', "%{$search}%")
                    ->orWhere('middle_name', 'LIKE', "%{$search}%")
                    ->orWhere('last_name', 'LIKE', "%{$search}%")
                    ->paginate(20);
            }

           $patientIds = $appointments->pluck('patient_id');
           if($patientIds ){
               if (empty($request->input('search'))) {
                   $patients = Patient::whereIn('id', $patientIds)->with(['appointments' => function ($query) {

                       $query->wheredoctor_id(auth()->user()->id)->wheredid_visit_doctor(0)->latest()->first();
                   }])->latest()->paginate(20);
               }else{
                   $patients = Patient::whereIn('id', $patientIds)->with(['appointments' => function ($query) {

                       $query->wheredoctor_id(auth()->user()->id)->wheredid_visit_doctor(0)->latest()->first();
                   }])->latest()
                       ->where('users.id', 'LIKE', "%{$search}%")
                       ->orWhere('first_name', 'LIKE', "%{$search}%")
                       ->orWhere('middle_name', 'LIKE', "%{$search}%")
                       ->orWhere('last_name', 'LIKE', "%{$search}%")
                       ->paginate(20);
               }
           }else{
              $patients = collect([]);
           }

        return view('patients.index', compact('patients', 'appointments')); 
        }
        return view('patients.index', compact('patients', 'appointments'));
      
    }

    public function getJsonPatients(Request $request){

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('patients.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'first_name' => 'required',
            'last_name' => 'required',
            'gender' => 'required',
            'dob' => 'required',
        ]);

        $patient = new Patient();
        $patient->first_name = $request->input('first_name');
        $patient->middle_name = $request->input('middle_name');
        $patient->last_name = $request->input('last_name');
        $patient->username = md5(uniqid(true));
        $patient->gender = $request->input('gender');
        $patient->dob = $request->input('dob');
        $patient->phone = $request->input('phone');
        $patient->email = $request->input('email');
        $patient->address = $request->input('address');
        $patient->password = bcrypt($request->input('last_name'));
        $patient->save();

        $patient->roles()->attach(Role::where('name', 'patient')->first()->id);

        flash('New patient created successfully')->success()->important();

        return redirect('patients/' . $patient->id);
    }

    /**
     * Display the specified resource.
     *
     * @param Patient $patient
     * @return \Illuminate\Http\Response
     * @internal param int $id
     */
    public function show(Patient $patient)
    {
        if (auth()->user()->hasRole('doctor')) {
       $department = auth()->user()->departments->first();
       $doctorIds = $department->doctors->pluck("id");

        $appointments = Appointment::whereIn("doctor_id",$doctorIds)->wherepatient_id($patient->id)->latest()->get();
        foreach($appointments as $appointment)
         {
        $appointment->did_visit_doctor = 1;
        $appointment->save();
        }
          $paymode = $patient->payments->where('type_of_payment','Consultation')->where('appointment_id',$appointment->id)->first()->mode_of_payment;

        $pmode = Paymentmode::find($paymode);
       //return view('patients.show', compact('patient','appointments'));
      return view('patients.appointments.appointments', compact('patient', 'appointments','pmode'));

           }else{
             return view('patients.show', compact('patient'));
           }
    }

    public function showmenus(Patient $patient,Appointment $appointment)
    {    
         
         $diagnosis = Diagnosis::wherepatient_id($patient->id)->whereappointment_id($appointment->id)->latest()->first();
        return view('patients.appointments.appointments2.index', compact('patient','appointment','diagnosis'));
    }
    public function showprofile(Patient $patient,Appointment $appointment)
    {
    $diagnosis = Diagnosis::wherepatient_id($patient->id)->whereappointment_id($appointment->id)->latest()->first();
         return view('patients.show', compact('patient','appointment','diagnosis'));
    }
    public function medicalhistory(Patient $patient,Appointment $appointment)
    {
        return view('patients.appointments.medicalhistory', compact('patient','appointment'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $patient = Patient::findOrFail($id);
        return view('patients.edit', compact('patient'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param Patient $patient
     * @return \Illuminate\Http\Response
     * @internal param int $id
     */
    public function update(Request $request, Patient $patient)
    {
        $this->validate($request, [
            'first_name' => 'required|string',
        ]);

        $patient->first_name = $request->input('first_name');
        $patient->middle_name = $request->input('middle_name');
        $patient->last_name = $request->input('last_name');
        $patient->gender = $request->input('gender');
        $patient->dob = $request->input('dob');
        $patient->phone = $request->input('phone');
        $patient->email = $request->input('email');
        $patient->address = $request->input('address');
        $patient->save();

        flash('Updated successfully.')->success()->important();

        return redirect('patients');
    }
    public function mark(Patient $patient)
    {
      $patient = Patient::find($patient->id);
       $appointments = auth()->user()->patients()->latest()->get();
      foreach($appointments as $appointment)
      {
        $appointment->did_visit_doctor = 1;
        $appointment->save();
      }
      return redirect()->back();
    }
    public function unvisited_to_doctor(){

     $patients = Role::where('name', 'patient')->first()->users()->orderBy('created_at', 'desc')->get();

        $appointments = collect([]);
        if (auth()->user()->hasRole('doctor')) {
         $appointments = auth()->user()->patients()->wheredid_visit_doctor(0)->latest('updated_at')->get();
           $patientIds = $appointments->pluck('patient_id');
           if($patientIds ){
        $patients = Patient::whereIn('id',$patientIds)->with(['appointments'=>
         function($query){ $query->wheredoctor_id(auth()->user()->id)->wheredid_visit_doctor(0)->latest()->first();}])->latest()->get();
           }else{
              $patients = collect([]);
           }
        return view('patients.index', compact('patients', 'appointments')); 
        }
        return view('patients.index', compact('patients', 'appointments'));

    }
    public function labresults()
    {

         $patients = Role::where('name', 'patient')->first()->users()->orderBy('created_at', 'desc')->get();

        $appointments = collect([]);
        if (auth()->user()->hasRole('doctor')) {
            $allappointments = auth()->user()->patients()->orderBy('created_at', 'desc')->get();
        }

        $labtest = Patient_lab_tests::where('result','!=',null)->latest()->get();

        $patientIds = $labtest->pluck('patient_id');

        $appointments =  $allappointments->whereIn('patient_id',$patientIds);

        return view('appointments.lab_results', compact('appointments'));
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
