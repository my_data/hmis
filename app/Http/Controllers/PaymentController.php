<?php

namespace App\Http\Controllers;

use DB;
use App\Patient;
use App\Payment;
use App\Paymentmode;
use Carbon\Carbon;
use Yajra\DataTables\DataTables;

class PaymentController extends Controller
{
    public function index($pmode=null)
    {

        $paymentmodes = Paymentmode::all();
        $pmode_id=null;
        //$pmode=null;
        if (!is_null($pmode)){
            $pmode_id=$pmode;
          //  $pmode=$mode;
        }
      //  dd($pmode);

        return view('payments.index', compact('allPayments','patients', 'paymentmodes','date','pmode_id'));

    }

    public  function getJsonAllPayments($pmode=null){

        if (request('date')) {
            if (request('date') == 'today') {
                $allPayments = Payment::today();//->get();
            }
            if (request('date') == 'yesterday') {
                $allPayments = Payment::yesterday();//->get();
            }
            if (request('date') == 'last-7-days') {
                $allPayments = Payment::pastNDays(7);//->get();
            }
            if (request('date') == 'last-14-days') {
                $allPayments = Payment::pastNDays(14);//->get();
            }
            if (request('date') == 'last-30-days') {
                $allPayments = Payment::pastNDays(30);//->get();
            }
        } else {
            $allPayments = Payment::latest();//->get();
        }

       // $patientIds = $allPayments->pluck('patient_id');
     //   $patients = Patient::whereIn('id', $patientIds)->get();
       // dd($allPayments->where('mode_of_payment', 2));
          if (!is_null($pmode)){
            //  $pmode=request()->get('pmode');
            $allPayments->where('mode_of_payment', $pmode);
          }

        return DataTables::of($allPayments)
            ->addIndexColumn()
            ->addColumn('category',function ($pay){
                   return  $pay->type_of_payment;
            })
            ->addColumn('amount',function ($pay){
                 return number_format($pay->amount) ;
            })
            ->addColumn('patient',function ($pay){
                $id= $pay->patient['id'];
                $endstr = substr($id,-0);
                if(strlen($endstr) % 2 != 0)
                    $endstr=$endstr."0";
                $chunks = str_split($endstr, 2);

                $result = implode('-', $chunks);
                if($id <100 ) {
                    $result = "00-00-" . $result;
                }elseif($id >=100 && $id < 9999){
                    $result = "00-".$result;
                }

                 return $result;

            })
            ->addColumn('payment_mode',function ($pay){
                if($pay->mode_of_payment) {
                    return $pay->modeOfPayment['name'];
                    }else {
                  return '<span class="btn btn-warning btn-sm" > Not Paid </span >';
              }
            })
            ->addColumn('date',function ($pay){
            return  $pay->created_at;
            })
            ->escapeColumns('payment_mode')
            ->make(true);


    }
    public function getJsonGroupByPatientPayments(){
        if (request('date')) {
            if (request('date') == 'today') {
                $allPayments = Payment::today();//->get();
            }
            if (request('date') == 'yesterday') {
                $allPayments = Payment::yesterday();//->get();
            }
            if (request('date') == 'last-7-days') {
                $allPayments = Payment::pastNDays(7);//->get();
            }
            if (request('date') == 'last-14-days') {
                $allPayments = Payment::pastNDays(14);//->get();
            }
            if (request('date') == 'last-30-days') {
                $allPayments = Payment::pastNDays(30);//->get();
            }
        } else {
            $allPayments = Payment::latest();//->get();
        }
         $patientIds = $allPayments->pluck('patient_id');
          $patients = Patient::whereIn('id', $patientIds);//->get();

        return DataTables::of($patients)
            ->addIndexColumn()
            ->addColumn('patno',function ($patient){
                $id= $patient->id;
                $endstr = substr($id,-0);
                if(strlen($endstr) % 2 != 0)
                    $endstr=$endstr."0";
                $chunks = str_split($endstr, 2);

                $result = implode('-', $chunks);
                if($id <100 ) {
                    $result = "00-00-" . $result;
                }elseif($id >=100 && $id < 9999){
                    $result = "00-".$result;
                }

                return link_to('/patients/'.$patient->id, $result);
            })

            ->addColumn('patient_name',function ($patient){


                return link_to('/patients/'.$patient->id,  $patient->first_name .' '. $patient->middle_name.' ' . $patient->last_name );

            })

            ->addColumn('action',function ($patient){
                return  link_to('/patients/'.$patient->id.'/appointments','<i class="fa fa-eye"></i>&nbsp;View Full Details');
            })
            ->escapeColumns('payment_mode','action')
            ->make(true);
    }
    public function paymentMode($pmode){
        $pmode = Paymentmode::findorfail($pmode);
        $paymentmodes = Paymentmode::all();
        $allPayments = Payment::latest()->where('mode_of_payment', $pmode->id)->get();

        $patientIds = $allPayments->pluck('patient_id');
        $patients = Patient::whereIn('id', $patientIds)->get();



        //return view('payments.index', compact('allPayments', 'patients','paymentmodes'));

    }
}
