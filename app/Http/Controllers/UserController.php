<?php

namespace App\Http\Controllers;

use App\Role;
use App\User;
use Illuminate\Http\Request;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (auth()->user()->hasRole('doctor')) {
        $patients = Role::where('name', 'patient')->first()->users()->orderBy('created_at', 'desc')->get();
        $appointments = collect([]);
        $appointments = auth()->user()->patients()->orderBy('created_at', 'desc')->get();
        }
        /*$users = User::latest()->get();*/
        return view('users.users.index');
    }
    public function getJsonUsers(Request $request)
    {
        $columns = array(
            0 => 'id',
            1 => 'full_name',
            2 => 'email',
            3 => 'phone',
            4 => 'gender',
            5 => 'roles'

        );

        /* $users = Role::with(['users' => function ($q) {
             //  $q->paginate(10);
             // $q->select(['users.id','users.first_name','users.last_name']);
         }])->whereName('patient')->first()->users;
 */
        $totatUsers = count(User::latest()->get());
        $totalFiltered = $totatUsers;
        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');
        /* $limit =100;// $request->input('length');
         $start = 0;//$request->input('start');
         $order = 'first_name';//$columns[$request->input('order.0.column')];
         $dir = "asc";//$request->input('order.0.dir');
 */
        if (empty($request->input('search.value'))) {
            $users = User::latest()->offset($start)
                ->limit($limit)
                ->orderBy($order, $dir)->get();


        } else {
            $search = $request->input('search.value');
            $users = User::latest()->where('id', 'LIKE', "%{$search}%")
                ->orWhere('first_name', 'LIKE', "%{$search}%")
                ->orWhere('middle_name', 'LIKE', "%{$search}%")
                ->orWhere('last_name', 'LIKE', "%{$search}%")
                ->offset($start)
                ->limit($limit)
                ->orderBy($order, $dir)->get();


            $totalFiltered = count($users);
        }

        // dd($users);

        $data = array();
        if (!empty($users)) {
            foreach ($users as $user) {
                $show = route('users.show', $user->id);
                $edit = route('users.edit', $user->id);

                $nestedData['id'] = $user->id;
                // $nestedData['first_name'] = $user->first_name;
                // $nestedData['middle_name'] = $users->middle_name;
                //  $nestedData['last_name'] = $users->last_name;
                $nestedData['full_name'] = $user->first_name . ' ' . $user->middle_name . ' ' . $user->last_name;
                $nestedData['email'] = $user->email;
                $nestedData['phone'] = $user->phone;
                $nestedData['gender'] = $user->gender;
                $html="";
                if($user->roles()->count()){
                    foreach($user->roles as $role) {
                        $html.= '<span class="badge" >'.$role->name.'</span >';
                    }
                }
                $nestedData['roles'] = $html;
                $nestedData['action_edit'] ="<a href='{$edit}' class='btn btn-primary btn-sm'>Edit</a>";
                $nestedData['action_delete'] = "<a class=\"btn btn-danger btn-sm\" data-toggle=\"modal\"
                                               href=\"#modal-id\"><i class=\"fa fa-trash\"></i> Delete</a>
                                            <div class=\"modal fade\" id=\"modal-id\">
                                                <div class=\"modal-dialog\">
                                                    <form method=\"POST\" action=\"/users/".$user->id."\">
                                                      '".csrf_field()."'
                                                       .'". method_field("DELETE").".'



                                                        <div class=\"modal-content\">
                                                            <div class=\"modal-header\">
                                                                <button type=\"button\" class=\"close\" data-dismiss=\"modal\"
                                                                        aria-hidden=\"true\">&times;</button>
                                                                <h4 class=\"modal-title\">
                                                                    Delete  $user->first_name  $user->middle_name  $user->last_name </h4>
                                                            </div>
                                                            <div class=\"modal-body\">
                                                                Delete
                                                                permanently  $user->first_name  $user->middle_name  $user->last_name 
                                                                ?
                                                            </div>
                                                            <div class=\"modal-footer\">
                                                                <button type=\"button\" class=\"btn btn-default\"
                                                                        data-dismiss=\"modal\">Cancel
                                                                </button>
                                                                <button type=\"submit\" class=\"btn btn-danger\"><i
                                                                            class=\"fa fa-trash\"></i> Delete
                                                                </button>
                                                            </div>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>";
                $data[] = $nestedData;

            }
        }


        $json_data = array(
            "draw" => intval($request->input('draw')),
            "recordsTotal" => intval($totatUsers),
            "recordsFiltered" => intval($totalFiltered),
            "data" => $data
        );

        echo json_encode($json_data);
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $roles = Role::orderby('name')->get();
        return view('users.create', compact('roles'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'first_name' => 'required',
            'last_name' => 'required',
            'gender' => 'required',
            'role_id' => 'required',
        ]);

        $user = new User();
        $user->first_name = $request->input('first_name');
        $user->middle_name = $request->input('middle_name');
        $user->last_name = $request->input('last_name');
        $user->username = strtolower($request->input('last_name'));
        $user->gender = $request->input('gender');
        $user->dob = $request->input('dob');
        $user->phone = $request->input('phone');
        $user->email = $request->input('email');
        $user->address = $request->input('address');
        $user->password = bcrypt("123456");
        $user->save();

        $role = Role::find($roleId = $request->only('role_id'))->first();
        $role->users()->attach($user);        

        flash('User is added successifully !')->success();

        return redirect('users');
    }

    /**
     * Display the specified resource.
     *
     * @param User $user
     * @return \Illuminate\Http\Response
     * @internal param int $id
     */
    public function show(User $user)
    {
        $roles = Role::all();
        return view('users.show', compact('user', 'roles'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param User $user
     * @return \Illuminate\Http\Response
     * @internal param int $id
     */
    public function edit(User $user)
    {
        $roles = Role::orderby('name')->get();
        return view('users.edit', compact('user', 'roles'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param User $user
     * @return \Illuminate\Http\Response
     * @internal param int $id
     */
    public function update(Request $request, User $user)
    {
        $this->validate($request, [
            'first_name' => 'required|string',
            'middle_name' => 'required|string',
            'last_name' => 'required|string',
            'gender' => 'required',
            'dob' => 'date',
        ]);

        $user->first_name = $request->input('first_name');
        $user->middle_name = $request->input('middle_name');
        $user->last_name = $request->input('last_name');
        $user->username = strtolower($request->input('last_name'));
        $user->gender = $request->input('gender');
        $user->dob = $request->input('dob');
        $user->phone = $request->input('phone');
        $user->email = $request->input('email');
        $user->address = $request->input('address');
        $user->password = bcrypt("123456");
        $user->save();

        flash('Success!')->success();

        return redirect('users');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param User $user
     * @return \Illuminate\Http\Response
     * @internal param int $id
     */
    public function destroy(User $user)
    {
        $user->delete();

        flash('Success')->success()->important();

        return back();
    }
    
    public function resetpassword($userId){

     $user = User::whereId($userId)->first();
     $user->password=bcrypt("123456");
     $user->save();
     flash('Password changed successifully,now your password is 123456 but you may change it')->success()->important();
     return redirect()->back();

    }
    public function administrators()
    {
        $users = Role::with('users')->whereName('admin')->first()->users;
        return view('users.index', compact('users'));
    }

    public function doctors()
    {
        $users = Role::with('users')->whereName('doctor')->first()->users;
        return view('users.index', compact('users'));
    } 


    public function imaging()
    {
        $users = Role::with('users')->whereName('imaging')->first()->users;
        return view('users.index', compact('users'));
    } 

    public function pharmacists()
    {
        $users = Role::with('users')->whereName('pharmacist')->first()->users;
        return view('users.index', compact('users'));     
    } 

    public function laboratoryAttendants()
    {
        $users = Role::with('users')->whereName('laboratory-attendant')->first()->users;
        return view('users.index', compact('users'));     
    }

    public function receptionists()
    {
        $users = Role::with('users')->whereName('receptionist')->first()->users;
        return view('users.index', compact('users'));
    }

    public function patients()
    {
       // $users = Role::with('users')->whereName('patient')->first()->users;
        return view('users.patients.index');

    }
    public function getJsonPatients(Request $request)
    {
        $columns = array(
            0 => 'id',
            1 => 'full_name',
            2 => 'email',
            3 => 'phone',
            4 => 'gender',
            5 => 'roles'

        );

        /* $users = Role::with(['users' => function ($q) {
             //  $q->paginate(10);
             // $q->select(['users.id','users.first_name','users.last_name']);
         }])->whereName('patient')->first()->users;
 */
        $totatUsers = count(Role::with(['users' => function ($q) {
            //  $q->paginate(10);
            // $q->select(['users.id','users.first_name','users.last_name']);
        }])->whereName('patient')->first()->users);
        $totalFiltered = $totatUsers;
        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');
        /* $limit =100;// $request->input('length');
         $start = 0;//$request->input('start');
         $order = 'first_name';//$columns[$request->input('order.0.column')];
         $dir = "asc";//$request->input('order.0.dir');
 */
        if (empty($request->input('search.value'))) {
            $users = Role::with(['users' => function ($q) use ($start, $limit, $order, $dir) {
                $q->offset($start)
                    ->limit($limit)
                    ->orderBy($order, $dir);
            }])->whereName('patient')->first()->users;

        } else {
            $search = $request->input('search.value');

            $users = Role::with(['users' => function ($q) use ($search, $start, $limit, $order, $dir) {
                $q->where('id', 'LIKE', "%{$search}%")
                    ->orWhere('first_name', 'LIKE', "%{$search}%")
                    ->orWhere('middle_name', 'LIKE', "%{$search}%")
                    ->orWhere('last_name', 'LIKE', "%{$search}%")
                    ->offset($start)
                    ->limit($limit)
                    ->orderBy($order, $dir);
            }])->whereName('patient')->first()->users;


            $totalFiltered = count(Role::with(['users' => function ($q) use ($search) {
                $q->where('id', 'LIKE', "%{$search}%")
                    ->orWhere('first_name', 'LIKE', "%{$search}%")
                    ->orWhere('middle_name', 'LIKE', "%{$search}%")
                    ->orWhere('last_name', 'LIKE', "%{$search}%");
            }])->whereName('patient')->first()->users
            );
        }

        // dd($users);

        $data = array();
        if (!empty($users)) {
            foreach ($users as $user) {
                $edit = route('users.edit', $user->id);
                $delete = route('users.destroy', $user->id);

                $nestedData['id'] = $user->id;
                // $nestedData['first_name'] = $user->first_name;
                // $nestedData['middle_name'] = $users->middle_name;
                //  $nestedData['last_name'] = $users->last_name;
                $nestedData['full_name'] = $user->first_name . ' ' . $user->middle_name . ' ' . $user->last_name;
                $nestedData['email'] = $user->email;
                $nestedData['phone'] = $user->phone;
                $nestedData['gender'] = $user->gender;
                $html="";
                if($user->roles()->count()){
                    foreach($user->roles as $role) {
                        $html.= '<span class="badge" >'.$role->name.'</span >';
                    }
                }
                $nestedData['roles'] = $html;
                $nestedData['action_edit'] ="<a href='{$edit}' class='btn btn-primary btn-sm'>Edit</a>";
                $nestedData['action_delete'] = "<a class=\"btn btn-danger btn-sm\" data-toggle=\"modal\"
                                               href=\"#modal-id\"><i class=\"fa fa-trash\"></i> Delete</a>
                                            <div class=\"modal fade\" id=\"modal-id\">
                                                <div class=\"modal-dialog\">
                                                    <form method=\"POST\" action=\"/users/".$user->id."\">
                                                      '".csrf_field()."'
                                                       .'". method_field("DELETE").".'



                                                        <div class=\"modal-content\">
                                                            <div class=\"modal-header\">
                                                                <button type=\"button\" class=\"close\" data-dismiss=\"modal\"
                                                                        aria-hidden=\"true\">&times;</button>
                                                                <h4 class=\"modal-title\">
                                                                    Delete  $user->first_name  $user->middle_name  $user->last_name </h4>
                                                            </div>
                                                            <div class=\"modal-body\">
                                                                Delete
                                                                permanently  $user->first_name  $user->middle_name  $user->last_name 
                                                                ?
                                                            </div>
                                                            <div class=\"modal-footer\">
                                                                <button type=\"button\" class=\"btn btn-default\"
                                                                        data-dismiss=\"modal\">Cancel
                                                                </button>
                                                                <button type=\"submit\" class=\"btn btn-danger\"><i
                                                                            class=\"fa fa-trash\"></i> Delete
                                                                </button>
                                                            </div>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>";

                $data[] = $nestedData;

            }
        }


        $json_data = array(
            "draw" => intval($request->input('draw')),
            "recordsTotal" => intval($totatUsers),
            "recordsFiltered" => intval($totalFiltered),
            "data" => $data
        );

        echo json_encode($json_data);

    }
}
