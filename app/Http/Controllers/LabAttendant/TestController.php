<?php

namespace App\Http\Controllers\LabAttendant;

use App\Patient_lab_tests;
use App\Test;
use Illuminate\Http\Request;
use DB;
use App\patient;
use App\Diagnosis;
use App\Http\Controllers\Controller;

class TestController extends Controller
{
    public function index()
    {
        if (!auth()->user()->hasRole('laboratory-attendant')) {
            return redirect('login');
        }
        
      // $patients = Patient::with(['payments' => function($query) {
      //   return $query->whereDate('created_at', '=', DB::RAW('CURDATE()'));
      // }])->get();
        $patients = Patient::with('payments')->has('payments')->latest()->get();
        
        return view('lab-attendant.tests.index', compact('patients'));
    }
 public function fromdoctor()
    {
        if (!auth()->user()->hasRole('laboratory-attendant')) {
            return redirect('login');
        }
        
      // $patients = Patient::with(['payments' => function($query) {
      //   return $query->whereDate('created_at', '=', DB::RAW('CURDATE()'));
      // }])->get();
        $lab_tests = Patient_lab_tests::where('result',null)->get();
        $patientIds = $lab_tests->pluck('patient_id');
        $patients = Patient::with('payments')->has('payments')->whereIn('id',$patientIds)->get();
        
        return view('lab-attendant.tests.index', compact('patients'));
    }
    public function show(Patient $patient,Diagnosis $diagnosis){
        if (!auth()->user()->hasRole('laboratory-attendant')) {
            return redirect('login');
        }

        $lab_tests = Patient_lab_tests::with('patient')->where('patient_id',$patient->id)->wherediagnosis_id($diagnosis->id)->latest()->get();

        return view('lab-attendant.tests.show', compact('lab_tests','patient','diagnosis'));
    }

    public function showDiagnosis(patient $patient)
    {
      $diagnosisIds = Patient_lab_tests::where('patient_id',$patient->id)->pluck('diagnosis_id');

         $diagnoses = Diagnosis::latest()->has('labTests')->find($diagnosisIds);

      return view('lab-attendant.tests.diagnosis',compact('diagnoses','patient'));
    }

    public function conduct(Patient $patient,Diagnosis $diagnosis,$id)
    {
      $lab_test = Patient_lab_tests::findOrFail($id);

        return view('lab-attendant.tests.conduct', compact('lab_test','patient','diagnosis'));
    }

    public function storeResult(Request $request,Patient $patient,Diagnosis $diagnosis,$id)
    {
        $this->validate($request, [
            'result' => 'required',
        ]);
        if($request->hasFile('attachment'))
        {
         $attachment_path=$request->attachment->store('Laboratory');
        }else
        {
        $attachment_path = "";
        }
        $lab_test = Patient_lab_tests::findOrFail($id);
        $lab_test->result = $request->input('result');
        $lab_test->description = $request->input('description');
        $lab_test->attachment =  $attachment_path;
        $lab_test->is_conducted = true;
        $lab_test->lab_attendant_id = auth()->id();
        $lab_test->save();

        flash('Success')->success()->important();

        return redirect('patients/'.$patient->id.'/diagnoses/'.$diagnosis->id.'/lab-tests');
    }
}
