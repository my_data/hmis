<?php

namespace App\Http\Controllers;

use App\Payment;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Excel;

class ExportExcelFileController extends Controller
{
    public function export(Request $request)
    {
        $this->validate($request, [
            'from' => 'required|date',
            'to' => 'required|date',
        ]);

        $from = Carbon::parse(request('from'));
        $to = Carbon::parse(request('to'));

        //return $payments = Payment::whereBetween("created_at",[$from,$to])->get();

         $payments = DB::table('payments')
         ->join("paymentmodes","paymentmodes.id","=","payments.mode_of_payment")
         ->select(DB::RAW("payments.id as 'SN',patient_id as 'PATIENT ID', paymentmodes.name as 'MODE OF PAYMENTS',amount as 'PAID AMOUNT', type_of_payment as 'PAYMENT FOR', date(payments.created_at) as 'DATE' "))
                ->orderBy('payments.created_at', 'asc')->get();

    $payments = json_decode( json_encode($payments), true);

        \Excel::create('Report', function ($excel) use ($payments) {
            $excel->sheet('Report', function ($sheet) use ($payments) {
                $sheet->fromModel($payments);
            });
        })->download('xlsx');

        return back();
    }
}
