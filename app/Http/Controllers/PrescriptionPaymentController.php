<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Prescription;
use App\Diagnosis;
use App\Prescription_payment;
use App\Payment;
use App\Patient;
use App\Paymentmode;
use App\Appointment;
class PrescriptionPaymentController extends Controller
{
   public function store(Patient $patient,Appointment $appointment,Diagnosis $diagnosis,Prescription $prescription)
   {
   	   $paymode = $patient->payments->where('type_of_payment','Consultation')->where('appointment_id',$appointment->id)->first()->mode_of_payment;
        $pmode = Paymentmode::find($paymode);
   	  //save the assigned drugs total cost
     $assigneddrugs=$prescription->assigndrugs;
        $sum = 0;
        if($assigneddrugs->count() >0){
          foreach ($assigneddrugs as  $assigneddrug) {
          $sum += $assigneddrug->drugs->price;
        }
        $prescriptionPayment = new Prescription_payment();
        $prescriptionPayment->diagnosis_id=$diagnosis->id;
        $prescriptionPayment->category = 'assigned drugs';
        $prescriptionPayment->patient_id=$patient->id;
        $prescriptionPayment->prescription_id=$prescription->id;
        $prescriptionPayment->cost = $sum;
        if($pmode->name != "CASH")
         {
          $prescriptionPayment->paid = 1;
         }
        $prescriptionPayment->save();
        }
         //save other prescription treatment payments
    
    if($prescription->otherprescriptions->count() > 0){
       $totalamount=0;
       foreach ($prescription->otherprescriptions as $otherprescription) {
        $totalamount+=$otherprescription->cost;
    
    }
         $prescriptionPayment = new Prescription_payment();
        $prescriptionPayment->diagnosis_id=$diagnosis->id;
        $prescriptionPayment->category = 'Other treatments';
        $prescriptionPayment->patient_id=$patient->id;
        $prescriptionPayment->prescription_id=$prescription->id;
        $prescriptionPayment->cost = $totalamount;
        if($pmode->name !="CASH")
         {
          $prescriptionPayment->paid = 1;
         }
        $prescriptionPayment->save();
    }
     $prescriptionPayments=Prescription_payment::wherediagnosis_id($diagnosis->id)->
     where('prescription_id',$prescription->id)->get();

    $total=$prescriptionPayments->sum('cost');
 if($pmode->name=="CASH")
  {
    $payments = new Payment();
    $payments->patient_id = $patient->id;
    $payments->type_of_payment = 'Prescription cost';
    $payments->amount = $total;
    $payments->appointment_id = $appointment->id;
    $payments->status ="0";
    $payments->save();
  }else{
    $payments = new Payment();
    $payments->patient_id = $patient->id;
    $payments->type_of_payment = 'Prescription cost';
    $payments->amount = $total;
    $payments->mode_of_payment = $pmode->id;
    $payments->mode_of_payment = $pmode->id;
    $payments->appointment_id = $appointment->id;
    $payments->status ="1";
    $payments->save();
  }
    flash('Patient prescriptions saved successifully..!')->success()->important();
   	return redirect('/patients/'.$patient->id.'/appointment/'.$appointment->id.'/diagnoses/'.$diagnosis->id.'/prescriptions');
   }
}
