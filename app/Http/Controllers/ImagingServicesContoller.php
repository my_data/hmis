<?php

namespace App\Http\Controllers;

use App\Diagnosis;
use App\Otherprocedure;
use App\Patient;
use App\Hospitalservice;
use App\Payment;
use App\Appointment;
use App\Service_category;
use App\Image_Service;
use App\PaymentMode;
use App\Imaging;
use Illuminate\Http\Request;
use DB;
use Illuminate\Support\Facades\Input;
use Response;


class ImagingServicesContoller extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $serviceId=Service_category::wherename('IMAGING-SERVICE')->first();
        $imagings = Image_Service::whereService_category_id( $serviceId)->latest    ()->get();
        $paymentmodes = Paymentmode::all();
        return view('imaging.index', compact('imagings','paymentmodes'));
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
   public function create(Patient $patient, Appointment $appointment,Diagnosis $diagnosis)
    {
        $diagnosis = Diagnosis::find($diagnosis->id);
        if($diagnosis !=null){
      $pmode = Payment::where('patient_id', $patient->id)->where('type_of_payment', 'consultation')->latest()->first()->mode_of_payment;
        $serviceId=Service_category::wherename('SERVICE AND PROCEDURE')->first();

      $services = Hospitalservice::whereservice_category_id($serviceId)->wherepaymentmode_id($pmode)->get();
        $paymentModes = \App\Paymentmode::orderBy('name')->get();

        $patient = Patient::whereid($patient->id)->first();
        return view('patients.diagnoses.imaging.create', compact('patient', 'paymentModes','services','appointment','diagnosis'));
        }else{
            return redirect('patients/'.$patient->id.'/appointment/'.$appointment->id.'/diagnosis/imaging');
        }
        $this->patient_id=$patient->id;
    }

      public function createimaging(Patient $patient, Diagnosis $diagnosis)
    {
        $paymentmodes = Paymentmode::all();

        return view('imaging.create',compact('paymentmodes'));
    }

        public function storeimaging(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'price' => 'required|numeric'
        ]);

        $serviceId=Service_category::wherename('IMAGING-SERVICE')->first(); 

        $imaging = new Image_Service();
        $imaging->service_category_id=$serviceId;
        $imaging->name = $request->input('name');
        $imaging->price = $request->input('price');
        $imaging->paymentmode_id = $request->input('paymentmode_id');
        $imaging->save();
        
        flash('Success')->success();

        return redirect('/imagingservices');
    }
 

    public function fromdoctor()
    {
        if (!auth()->user()->hasRole('imaging')) {
            return redirect('login');
        }
        
      // $patients = Patient::with(['payments' => function($query) {
      //   return $query->whereDate('created_at', '=', DB::RAW('CURDATE()'));
      // }])->get();
      $imagings = Imaging::latest()->get();
        $patientIds = $imagings->pluck('patient_id');
        $patients = Patient::with('payments')->has('payments')->whereIn('id',$patientIds)->get();
        
        return view('imaging.patients.index', compact('patients'));
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */ 
    public function store(Request $request, $patient,Appointment $appointment,Diagnosis $diagnosis)
    {
    
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Patient $patient){
        if (!auth()->user()->hasRole('imaging')) {
            return redirect('login');
        }

        $imagings = Imaging::with('patient')->where('patient_id',$patient->id)->latest()->get();

        return view('imaging.patients.show', compact('imagings','patient'));
    }

    public function conduct(Patient $patient,$id)
    {

      $imaging = Imaging::findOrFail($id);

        return view('imaging.patients.conduct', compact('imaging','patient'));
    }

    public function storeResult(Request $request,Patient $patient,$id)
    {
        $this->validate($request, [
            'result' => 'required',
        ]);

        if($request->hasFile('attachment'))
        {
         $attachment_path=$request->attachment->store('Imaging');
        }else{
              $attachment_path="";
        }
        $imaging = Imaging::findOrFail($id);
        $imaging->result = $request->input('result');
        $imaging->attachment =  $attachment_path;
        $imaging->description = $request->input('description');
        $imaging->is_conducted = true;
        $imaging->save();

        flash('Success')->success()->important();
         $attachment = storage_path('app/' . $imaging->attachment);
       

        return redirect('imaging/patient/'.$patient->id.'/imaging');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

      public function editimaging($imaging)
    {
        $imaging = Image_Service::find($imaging);
         $paymentmodes = Paymentmode::all();
        return view('imaging.edit', compact('imaging','paymentmodes'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required',
            'price' => 'required|numeric',
        ]);

        $imaging = Image_Service::findOrFail($id);
        $imaging->name = $request->input('name');
        $imaging->price = $request->input('price');
        $imaging->paymentmode_id = $request->input('paymentmode_id');
        $imaging->save();

        flash('Updated successfully.')->success();

        return redirect('imagingservices');
    }

public function destroy($id)
    {
        $imaging = Image_Service::findOrFail($id);
        $imaging->delete();

        flash('Deleted successfully.')->success();

        return back();
    }

    public function upload(Request $request)
    {
        $serviceId=Service_category::wherename('IMAGING-SERVICE')->first(); 
        // load the rows
        $collections = \Excel::load($request->file('user_file'), function ($reader) {
        })->get();
        $count = 0;
        $is_sheets=0;
  foreach ($collections as $collection) {
     foreach ($collection as $coll) {
    if (is_object($coll)) {
  $is_sheets=1;
    }else
    {
        $is_sheets=0;
    }
}
}

if($is_sheets==1)
{
    $index = $collections->count();
     for($i=0;$i< 1;$i++)
     {
      foreach ($collections[$i] as $row) {
            $imaging = new Image_Service();
            $imaging->service_category_id = $serviceId;
            $imaging->name = $row->name;
            $imaging->price = $row->price;
            $imaging->paymentmode_id = $request->input('paymentmode_id');
            $imaging->save();
            $count++;
     }
 }
}if($is_sheets==0){
   foreach ($collections as $row) {
            $imaging = new Image_Service();
            $imaging->service_category_id = $serviceId;
            $imaging->name = $row->name;
            $imaging->price = $row->price;
            $imaging->paymentmode_id = $request->input('paymentmode_id');;
            $imaging->save();
            $count++;
        }   
}

        flash($count . ' items uploaded successfully.')->success();

        return redirect('imagingservices');
    }

       public function loadAjaxdata(Request $request)
    {

     $patient_id = Input::get('patient_id');
     $pmode = Payment::where('patient_id', $patient_id)->where('type_of_payment', 'consultation')->latest()->first()->mode_of_payment;
        $serviceId=Service_category::wherename('IMAGING-SERVICE')->first();
    $term = Input::get('query');

    $results = array();
    
    $queries = Hospitalservice::whereservice_category_id($serviceId)
                                ->wherepaymentmode_id($pmode)       
                               ->where('name','LIKE','%'.$term.'%')
                              ->take(5)->get();
    foreach ($queries as $query)
    {
        $results[] = ['id'=>$query->id,'value' => $query->name,'price'=>$query->price ];
    }
return Response::json($results);
    }
}
