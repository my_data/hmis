<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Diagnosis;
use App\Appointment;
use App\Diagnosis_payment;
use App\Otherprocedure;
use App\Patient_lab_tests;
use App\Payment;
use App\Imaging;
use App\Paymentmode;
use App\Patient;
use DB;

class SaveOtherProceduresController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
     public function store(Request $request,Patient $patient,Appointment $appointment,Diagnosis $diagnosis)
    {

        $patient = Patient::find($patient->id);
        $paymode = $patient->payments->where('type_of_payment','Consultation')->where('appointment_id',$appointment->id)->first()->mode_of_payment;
        $pmode = Paymentmode::find($paymode);  
        $this->validate($request, [
            'servicename' => 'required  ',
            'cost'=>'required|numeric',
            'results' => 'required',
        ]);
        $otherprocedure = new Otherprocedure();
        $otherprocedure->name = $request->input('servicename');
        $otherprocedure->diagnosis_id = $diagnosis->id;
        $otherprocedure->procedure_category_id = 0;
        if($pmode->name=="CASH"){  $otherprocedure->paid = false;}else{  $otherprocedure->paid = true;}
        $otherprocedure->amount=$request->input('cost');
        $otherprocedure->description = $request->input('results');
        $otherprocedure->save();

        //save the Imaging total cost
        if($pmode->name=="CASH"){
        $otherprocedures=Otherprocedure::wherediagnosis_id($diagnosis->id)->wherepaid(0)->get();
        }else{
        $otherprocedures=Otherprocedure::wherediagnosis_id($diagnosis->id)->wherepaid(1)->get();
        }
        $sum = 0;
        if($otherprocedures->count() >0){
          foreach ($otherprocedures as $otherprocedures) { 
          $sum += $otherprocedures ->amount;
        }
        if($pmode->name=="CASH"){
    $diagnosis_payments = Diagnosis_payment::where('diagnosis_id',$diagnosis->id)->wherecategory("Other procedures")->wherepaid(false)->latest()->first();   
        }else{
    $diagnosis_payments = Diagnosis_payment::where('diagnosis_id',$diagnosis->id)->wherecategory("Other procedures")->wherepaid(true)->latest()->first();
        }
    
        if($diagnosis_payments !=null)
        {
        $diagnosis_payments->amount = $sum;
        if($pmode->name=="CASH"){$diagnosis_payments->paid=false;}else{$diagnosis_payments->paid=true;}
        $diagnosis_payments->save();    
        }else{
        $diagnosis_payments = new Diagnosis_payment();
        $diagnosis_payments->diagnosis_id=$diagnosis->id;
        $diagnosis_payments->category = 'Other procedures';
        if($pmode->name=="CASH"){
         $diagnosis_payments->paid=false;
        }else{
         $diagnosis_payments->paid=true;
        }
        $diagnosis_payments->amount = $sum;
        $diagnosis_payments->save();
        }
        
    if($pmode->name=="CASH"){
    $total= Diagnosis_payment::wherediagnosis_id($diagnosis->id)->wherepaid(false)->sum('amount');
    $payments = Payment::whereAppointment_id($appointment->id)->wherestatus("0")->latest()->first();
    }else{
    $total= Diagnosis_payment::wherediagnosis_id($diagnosis->id)->wherepaid(true)->sum('amount');
    $payments = Payment::whereAppointment_id($appointment->id)->wherestatus("10")->latest()->first();
    } 
           if($payments !=null)
                    {
                       
          if($pmode->name=="CASH")
          {

                
                $payments->patient_id = $patient->id;
                $payments->type_of_payment = 'Diagnosis cost';
                $payments->amount = $total;
                $payments->appointment_id = $appointment->id;
                $payments->status ="0";
                $payments->save();
         }else{
                if($payments->type_of_payment=="Consultation"){
                      $payments = new Payment();
                                    $payments->patient_id = $patient->id;
                                    $payments->type_of_payment = 'Diagnosis cost';
                                    $payments->amount = $total;
                                    $payments->mode_of_payment = $pmode->id;
                                    $payments->appointment_id = $appointment->id;
                                    $payments->status ="1";
                                    $payments->save();
                }else{
                $payments->patient_id = $patient->id;
                $payments->type_of_payment = 'Diagnosis cost';
                $payments->amount = $total;
                $payments->mode_of_payment = $pmode->id;
                $payments->appointment_id = $appointment->id;
                $payments->status ="1";
                $payments->save();
                }
         }
                    }else{

                         if($total > 0){
                            if($pmode->name=="CASH")
                            {

                                    $payments = new Payment();
                                    $payments->patient_id = $patient->id;
                                    $payments->type_of_payment = 'Diagnosis cost';
                                    $payments->amount = $total;
                                    $payments->appointment_id = $appointment->id;
                                    $payments->status ="0";
                                    $payments->save();
                             }else{
                                    $payments = new Payment();
                                    $payments->patient_id = $patient->id;
                                    $payments->type_of_payment = 'Diagnosis cost';
                                    $payments->amount = $total;
                                    $payments->mode_of_payment = $pmode->id;
                                    $payments->appointment_id = $appointment->id;
                                    $payments->status ="1";
                                    $payments->save();
                             }
                             }
                        } 

  flash('Other procedures saved successiful..!')->success()->important();
  return redirect()->back();

         }else{
    flash('Other procedures not yet found..!')->warning()->important();
      return redirect()->back();
         }
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
