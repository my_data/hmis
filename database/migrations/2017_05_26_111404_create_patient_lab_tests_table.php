<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePatientLabTestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('patient_lab_tests', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('lab_testId')->unsigned();
            $table->integer('diagnosis_id')->unsigned();
            $table->string('result')->nullable();
            $table->text('description')->nullable();
            $table->string('attachment')->nullable();
            $table->boolean('is_conducted')->default(false);
            $table->integer('patient_id')->unsigned()->nullable();
            $table->integer('lab_attendant_id')->unsigned()->nullable();
            $table->boolean('paid')->default(false)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('patient_lab_tests');
    }
}
