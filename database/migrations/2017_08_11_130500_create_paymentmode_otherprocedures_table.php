<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePaymentmodeOtherproceduresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('paymentmodes_otherprocedures', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('otherprocedure_id')->unsigned();
            $table->integer('paymentmode_id')->unsigned();
            $table->double('amount');
            $table->timestamps();
            $table->foreign('otherprocedure_id')->references('id')->on('paymentmodes')
            ->onUpdate('cascade');
            $table->foreign('paymentmode_id')->references('id')->on('paymentmodes')
            ->onUpdate('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('paymentmodes_otherprocedures');
    }
}
