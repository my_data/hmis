<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\User::class, function (Faker\Generator $faker) {
    static $password;
    $gender = ['Female', 'Male'];

    return [
        'first_name' => $faker->firstName,
        'last_name' => $faker->lastName,
        'username' => $faker->unique()->word,
        'gender' => $gender[$faker->numberBetween(0, 1)],
        'dob' => $faker->date(),
        'email' => $faker->unique()->safeEmail,
        'phone' => $faker->unique()->phoneNumber,
        'address' => $faker->address,
        'password' => $password ?: $password = bcrypt('secret'),
        'remember_token' => str_random(10),
    ];
});

$factory->define(App\Appointment::class, function (Faker\Generator $faker) {

    return [
        'patient_id' => function () {
            return factory('App\User')->create()->id;
        },
        'doctor_id' => function () {
            return factory('App\User')->create()->id;
        },
        'description' => $faker->paragraph,
    ];
});

$factory->define(App\PaymentType::class, function (Faker\Generator $faker) {
    $display_name = ucfirst($faker->word);
    $name = str_slug($display_name, '-');

    return [
        'name' => $name,
        'display_name' => $display_name,
        'amount' => $faker->randomNumber(4),
        'description' => $faker->paragraph,
    ];
});

$factory->define(App\Payment::class, function (Faker\Generator $faker) {

    $typeOfPayment = ['consultation', 'lab test', 'prescription', 'other'];
    $modeOfPayment = ['CASH', 'NHIF', 'NSSF', 'BUNGE', 'TANESCO', 'JUBELEE', 'PPF', 'STRATEGY', 'OTHERS'];

    return [
        'patient_id' => function () {
            return factory('App\User')->create()->id;
        },
        'staff_id' => function () {
            return factory('App\User')->create()->id;
        },
        'type_of_payment' => $typeOfPayment[$faker->numberBetween(0, count($typeOfPayment) - 1)],
        'mode_of_payment' => $modeOfPayment[$faker->numberBetween(0, count($modeOfPayment) - 1)],
        'amount' => $faker->numberBetween(1500, 500000),
    ];
});

$factory->define(App\Diagnosis::class, function (Faker\Generator $faker) {

    return [
        'patient_id' => function () {
            return factory('App\User')->create()->id;
        },
        'doctor_id' => function () {
            return factory('App\User')->create()->id;
        },
        'summary' => $faker->sentence,
        'body' => $faker->paragraph,
    ];
});

$factory->define(App\Department::class, function (Faker\Generator $faker) {

    return [
        'name' => $faker->sentence,
        'fee' => $faker->randomNumber(4),
    ];
});

$factory->define(App\Drug::class, function (Faker\Generator $faker) {

    return [
        'name' => ucfirst($faker->word),
        'price' => $faker->randomNumber(4),
    ];
});

$factory->define(App\Test::class, function (Faker\Generator $faker) {

    return [
        'name' => $faker->sentence,
        'price' => $faker->randomNumber(4),
    ];
});

$factory->define(App\Prescription::class, function(Faker\Generator $faker) {
    return  [
        'disease' => ucfirst($faker->word),
        'description' => $faker->paragraph,
        'patient_id' => function () {
            return factory('App\User')->create()->id;
        },
        'doctor_id' => function () {
            return factory('App\User')->create()->id;
        },
        'diagnosis_id' => function () {
            return factory('App\Diagnosis')->create()->id;
        },
    ];
});

$factory->define(App\Assigndrug::class, function (Faker\Generator $faker) {
    return [
        'prescription_id' => function () {
            return factory('App\Prescription')->create()->id;
        },
        'medicine_id' => function () {
            return factory('App\Drug')->create()->id;
        },
        'done' => true,
        'staff_id' => function () {
            return factory('App\User')->create()->id;
        },
        'dose' => $faker->randomNumber,
        'description' => $faker->paragraph,
    ];
});

$factory->define(App\Paymentmode::class, function (Faker\Generator $faker) {
    return [
        'name' => strtoupper($faker->word),
        'consultation_fee' => $faker->integer
    ];
});