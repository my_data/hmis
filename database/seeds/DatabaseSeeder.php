<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       $this->call(RolesTableSeeder::class);

       $this->call(Service_categoryTableSeeder::class);
       
       $this->call(UsersTableSeeder::class);

       $this->call(PaymentmodesTableSeeder::class);

       $this->call(DoctorsTableSeeder::class);
       $this->call(PharmacistsTableSeeder::class);
       $this->call(ReceptionistsTableSeeder::class);
       $this->call(LaboratoryAttendantsTableSeeder::class);
       $this->call(PatientsTableSeeder::class);
       
       $this->call(DrugsTableSeeder::class);
       
       $this->call(DiagnosesTableSeeder::class);
       $this->call(PrescriptionTableSeeder::class);
       
       $this->call(PaymentsTableSeeder::class);
       $this->call(AppointmentsTableSeeder::class);
       
       $this->call(AssigndrugTableSeeder::class);
       
       $this->call(DepartmentsTableSeeder::class);

    }
}
