<?php

use App\Role;
use App\Prescription;
use Illuminate\Database\Seeder;

class AssigndrugTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$receptionist = Role::whereName('receptionist')->firstOrFail()->users()->first();

    	$prescriptions = Prescription::get();

    	$prescriptions->each(function ($prescription) use ($receptionist) {
        	factory('App\Assigndrug')->create([
        		'prescription_id' => $prescription->id,
        		'staff_id' => $receptionist->id,
        	]);
    	});
    }
}
