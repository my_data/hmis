<?php

use Illuminate\Database\Seeder;

class DoctorsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $doctorRole = App\Role::whereName('doctor')->firstOrFail();

        $user1 = factory('App\User')->create(['username' => 'doctor-1']);
        $user2 = factory('App\User')->create(['username' => 'doctor-2']);
        
    	$doctorRole->users()->attach($user1);
    	$doctorRole->users()->attach($user2);
    }
}
