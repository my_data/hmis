<?php

use App\Paymentmode;
use Illuminate\Database\Seeder;

class PaymentmodesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	DB::table('paymentmodes')->delete();

    	Paymentmode::create(['name' => 'CASH', 'consultation_fee' => 20000]);
    	Paymentmode::create(['name' => 'NHIF', 'consultation_fee' => 20000]);
    	Paymentmode::create(['name' => 'NSSF', 'consultation_fee' => 20000]);
    	Paymentmode::create(['name' => 'BUNGE', 'consultation_fee' => 20000]);
    	Paymentmode::create(['name' => 'TANESCO', 'consultation_fee' => 30000]);
    	Paymentmode::create(['name' => 'JUBELEE', 'consultation_fee' => 20000]);
    	Paymentmode::create(['name' => 'PPF', 'consultation_fee' => 20000]);
    	Paymentmode::create(['name' => 'STRATEGIS', 'consultation_fee' => 20000]);
    	Paymentmode::create(['name' => 'OTHERS', 'consultation_fee' => 20000]);
    }
}
