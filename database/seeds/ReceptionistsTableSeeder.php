<?php

use Illuminate\Database\Seeder;

class ReceptionistsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $role = App\Role::whereName('receptionist')->firstOrFail();

        $user1 = factory('App\User')->create(['username' => 'receptionist-1']);
        $user2 = factory('App\User')->create(['username' => 'receptionist-2']);
        
    	$role->users()->attach($user1);
    	$role->users()->attach($user2);
    }
}
