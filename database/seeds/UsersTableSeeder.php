<?php

use App\Role;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      $user = factory('App\User')->create(['username' => 'admin']);

        $user->roles()->attach(Role::first());
    }
}
