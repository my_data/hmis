<?php

use Illuminate\Database\Seeder;

class DiagnosesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $patients = App\Role::whereName('patient')->firstOrFail()->users;
        $doctor = App\Role::whereName('doctor')->firstOrFail()->users()->first();

        $patients->each(function($patient) use ($doctor) {
        	factory('App\Diagnosis')->create([
        		'patient_id' => $patient->id,
        		'doctor_id' => $doctor->id,
        	]);
        });        
    }
}
