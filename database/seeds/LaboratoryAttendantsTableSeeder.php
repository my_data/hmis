<?php

use Illuminate\Database\Seeder;

class LaboratoryAttendantsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $role = App\Role::whereName('laboratory-attendant')->firstOrFail();

        $user1 = factory('App\User')->create(['username' => 'lab-attendant-1']);
        $user2 = factory('App\User')->create(['username' => 'lab-attendant-2']);
        
    	$role->users()->attach($user1);
    	$role->users()->attach($user2);
    }
}
