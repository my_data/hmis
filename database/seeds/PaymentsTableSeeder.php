<?php

use Illuminate\Database\Seeder;

class PaymentsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $receptionist = App\Role::whereName('receptionist')->firstOrFail()->users()->first();
        $patients = App\Role::whereName('patient')->firstOrFail()->users;

        $patients->each(function($patient) use ($receptionist) {
        	factory('App\Payment')->create([
        		'patient_id' => $patient->id,
        		'staff_id' => $receptionist->id,
        	]);
        });
    }
}
