<?php

use App\Role;
use App\Paymentmode;
use App\Patient;
Route::get('table', function () {
    return view('table');
});
Route::group(['middleware'=>'revalidate'],function(){
    Route::middleware('auth')->get('/', function () {
        return redirect()->home();
    });

    Route::get('/patients/appointments', 'AppointmentController@unattended');

    Route::get('patient/appointments/ajax-doctors', 'AppointmentController@loadAjaxdata');

    Route::get('hospitalservice/ajax-services', 'OtherproceduresController@loadAjaxdata');

    Route::get('hospitalimagingservices/ajax-services', 'ImagingServicesContoller@loadAjaxdata');

    Route::get('hospitalservice/ajax-services/costs', 'OtherproceduresController@loadAjaxprocedurecost');
    Auth::routes();
    Route::get('/home', 'HomeController@index')->name('home');

    Route::group(['middleware' => 'auth'], function () {

        Route::get('/patients/fromreception', 'PatientController@unvisited_to_doctor');
        Route::get('account', 'AccountController@index');
        Route::patch('account', 'AccountController@updateAccount');
        Route::get('account/settings/password', 'AccountController@password');
        Route::post('account/settings/password', 'AccountController@changePassword');

        Route::get('/patients/laboratory-results','PatientController@labresults')->name('laboratory-patients');
        Route::post('laboratory-tests/upload', 'LaboratoryTestController@upload');
        Route::resource('laboratory-tests', 'LaboratoryTestController');

        Route::resource('patients', 'PatientController');
        Route::post('patients', 'PatientController@index');


        Route::get('/patients/{patient}/appointments/{appointment}/manage','PatientController@showmenus');
        Route::get('/patients/{patient}/appointment/{appointment}/diagnoses/otherprocedures', 'OtherproceduresController@index');
        Route::get('/patients/{patient}/appointment/{appointment}/diagnosis/imaging','DiagnosisController@imaging');

        Route::get('patients/{patient}/appointment/{appointment}/medical-history', 'PatientController@medicalhistory');

        Route::get('users/administrators', 'UserController@administrators')->name('users.administrators');
        Route::get('users/doctors', 'UserController@doctors')->name('users.doctors');
        Route::get('users/pharmacists', 'UserController@pharmacists')->name('users.pharmacists');
        Route::get('users/laboratory-attendants', 'UserController@laboratoryAttendants')->name('users.laboratory-attendants');
        Route::get('users/receptionists', 'UserController@receptionists')->name('users.receptionists');
        Route::get('users/patients', 'UserController@patients')->name('users.patients');
        Route::get('users/get-patients', 'UserController@getJsonPatients')->name('users.get-patients');
        Route::get('users/get-users', 'UserController@getJsonUsers')->name('users.get-users');
        Route::get('users/imaging-tech', 'UserController@imaging')->name('users.imaging-tech');

        Route::resource('users', 'UserController');
        Route::get('users/create', 'UserController@create')->name('users.create');

        Route::post('users/new', 'UserController@store');
        Route::post('users/user/{user}/resetpassword', 'UserController@resetpassword');

        Route::post('users/new', 'UserController@store')->name('users.store');
        Route::post('users/{user}/roles', 'RoleController@store');


        Route::get('doctors/', 'DoctorController@index')->name('doctors.index');
        Route::get('doctors/{doctor}', 'DoctorController@show');
        Route::post('/doctors/{doctor}/departments', 'DoctorController@store');
        Route::patch('/doctors/{doctor}/departments/{department}', 'DoctorController@removeDepartment');

        Route::resource('appointments', 'AppointmentController');

        Route::get('payment-types', 'PaymentTypeController@index');
        Route::get('payment-types/create', 'PaymentTypeController@create');
        Route::post('payment-types/new', 'PaymentTypeController@store');
        Route::get('payment-types/{id}/edit', 'PaymentTypeController@edit');
        Route::patch('payment-types/{id}', 'PaymentTypeController@update');
        Route::delete('payment-types/{id}', 'PaymentTypeController@destroy');

        Route::get('/payment-modes', 'PaymentmodeController@index');
        Route::get('/payment-modes/create', 'PaymentmodeController@create');
        Route::post('payment-modes/store', 'PaymentmodeController@store');
        Route::delete('/payment-mode/{id}/delete', 'PaymentmodeController@destroy');
        Route::get('/payment-mode/{id}/edit', 'PaymentmodeController@edit');
        Route::patch('/payment-mode/{id}','PaymentmodeController@update');
        // For Receptionist
        Route::get('payments/summary', function() {

            // Today
            $todayPaymentStats = \App\Payment::with('modeOfPayment')
                ->select('mode_of_payment', DB::RAW('SUM(amount) as total'))
                ->whereDate('created_at', DB::RAW('CURDATE()'))
                ->groupBy('mode_of_payment')
                ->get();

            // Last 7 days
            $last7DaysPaymentStats = \App\Payment::with('modeOfPayment')
                ->select('mode_of_payment', DB::RAW('SUM(amount) as total'))
                ->whereDate('created_at', '>=', \Carbon\Carbon::now()->subWeek())
                ->groupBy('mode_of_payment')
                ->get();

            // All
            $allPaymentStats = \App\Payment::with('modeOfPayment')
                ->select('mode_of_payment', DB::RAW('SUM(amount) as total'))
                ->groupBy('mode_of_payment')
                ->get();

            return view('payments.summary', compact('todayPaymentStats', 'last7DaysPaymentStats', 'allPaymentStats'));
        })->name('payments.summary');



        Route::get('payments/get-all-payments/{pmode?}', 'PaymentController@getJsonAllPayments')->name('payments.get-all-payments');
        Route::get('payments/get-group-by-patient-payments', 'PaymentController@getJsonGroupByPatientPayments')->name('payments.get-group-by-patient-payments');
        Route::get('payments/{pmode?}', 'PaymentController@index')->name('payments.index');

      //  Route::resource('payments', 'PaymentController');
       // Route::post('payments/{mode?}/{pmode?}', 'PaymentController@index')->name('payments.index');

        Route::post('patients/{patient}/appointment/{appointment}/payments/outstands/{payments}', 'patientpaymentController@edit');
        Route::get('patients/{patient}/payments', 'PatientpaymentController@index');
        Route::get('patients/{patient}/appointment/{appointment}/payments', 'PatientpaymentController@show');

        Route::get('patients/{patient}/appointment/{appointment}/payment/{payment}/details', 'patientpaymentController@viewdetails');
        Route::post('patients/{patient}/payments/new', 'patientpaymentController@store');
        Route::get('patients/{patient}/appointment/{appointment}/diagnoses/treatments', 'TreatmentController@showDiagnosis');
        Route::get('patients/{patient}/appointments', 'AppointmentController@index');
        Route::get('patients/{patient}/appointments/create', 'AppointmentController@create');
        Route::get('patients/{patient}/appointments/{appointment}', 'AppointmentController@show');
        Route::get('patients/{patient}/payments/create', 'patientpaymentController@create');
        Route::post('patients/{patient}/appointments', 'AppointmentController@store');

        Route::get('patients/{patient}/read', 'PatientController@mark');

        Route::get('patients/{patient}/appointment/{appointment}/diagnoses', 'DiagnosisController@index');
        Route::get('patients/{patient}/appointment/{appointment}/diagnoses/investigations', 'LaboratoryTestController@showdiagnosis');

        Route::get('patients/{patient}/appointment/{appointment}/treatments/diagnoses', 'TreatmentController@diagnoses');
        Route::get('patients/{patient}/appointment/{appointment}/diagnoses/0/investigations', 'LaboratoryTestController@allLabtest');
        Route::get('patients/{patient}/appointment/{appointment}/diagnoses/{diagnoses}/laboratory-tests', 'DiagnosisController@createLabtest');

        Route::get('/patients/{patient}/appointment/{appointment}/diagnoses/{diagnosis}/investigations', 'LaboratoryTestController@allLabtest');
        Route::get('patients/{patient}/appointment/{appointment}/diagnoses/{diagnosis}/laboratory-tests/{laboratory_test}/view', 'DiagnosisController@showLab');

        Route::get('patients/{patient}/appointment/{appointment}/diagnoses/{diagnosis}/procedure/{procedure}/view', 'DiagnosisController@showprocedure');

        Route::get('patients/{patient}/appointment/{appointment}/diagnoses/{diagnosis}/laboratory-tests/{laboratory_test}/attachment/view', 'DiagnosisController@showLabattachment');

        Route::get('patients/{patient}/appointment/{appointment}/diagnoses/{diagnosis}/imagingservice/{imaging}/attachment', 'DiagnosisController@showImaging');
        Route::get('patients/{patient}/appointment/{appointment}/diagnoses/{diagnosis}/imagingservice/{imaging}/attachment/view', 'DiagnosisController@showattachment');

        Route::post('patients/{patient}/appointment/{appointment}/diagnoses/{diagnosis}/laboratory-tests/store', 'Diagnosis_paymentController@store');

        Route::post('patients/{patient}/diagnoses/{diagnoses}/laboratory-tests/remove', 'DiagnosisController@removeLabtest');

        Route::get('patients/{patient}/appointment/{appointment}/diagnoses/new', 'DiagnosisController@create');
        Route::get('patients/{patient}/appointment/{appointment}/diagnoses/{diagnosis}', 'DiagnosisController@show');

        Route::post('patients/{patient}/appointment/{appointment}/diagnoses/save', 'DiagnosisController@store');
        Route::post('/patients/{patient}/appointment/{appointment}/diagnoses/{diagnosis}/cancel', 'DiagnosisController@cancel');

        Route::get('patients/{patient}/diagnoses/{diagnosis}', 'DiagnosisController@show');

        Route::get('patients/{patient}/appointment/{appointment}/diagnoses/{diagnosis}/prescriptions/create', 'PrescriptionController@create');
        Route::post('patients/{patient}/diagnoses/{diagnosis}/prescriptions/storecost', 'Diagnosis_paymentController@storeprescriptioncost');

        Route::get('/patients/{patient}/appointment/{appointment}/diagnoses/0/otherprocedures', 'OtherproceduresController@create');
        Route::get('/patients/{patient}/appointment/{appointment}/diagnoses/{diagnosis}/otherprocedures', 'OtherproceduresController@create');
        Route::get('/patients/{patient}/appointment/{appointment}/diagnoses/otherprocedures', 'OtherproceduresController@index');

        Route::post('/patients/{patient}/appointment/{appointment}/diagnoses/{diagnosis}/otherprocedures', 'OtherproceduresController@store');
        Route::get('/patients/{patient}/appointment/{appointment}/diagnoses/0/imaging', 'ImagingServicesContoller@create');
        Route::get('/patients/{patient}/appointment/{appointment}/diagnoses/{diagnosis}/imaging', 'ImagingServicesContoller@create');
        Route::post('/patients/{patient}/appointment/{appointment}/diagnoses/{diagnosis}/imaging', 'ImagingServicesContoller@store');

        Route::get('/patients/{patient}/appointments/{appointment}/profile', 'PatientController@showprofile');

        Route::post('/patients/{patient}/diagnoses/{diagnosis}/otherprocedures2', 'OtherproceduresController@store2');
        Route::get('/patients/{patient}/appointment/{appointment}/diagnoses/{diagnosis}/prescriptions/', 'PrescriptionController@index');
        Route::get('patients/{patient}/appointment/{appointment}/diagnoses/{diagnosis}/prescriptions/{prescription}', 'PrescriptionController@show');

        Route::post('patients/{patient}/appointment/{appointment}/diagnoses/{diagnosis}/prescriptions/new', 'PrescriptionController@store');
        Route::get('patients/{patient}/appointment/{appointment}/diagnoses/{diagnosis}/prescriptions/{prescription}/create/assign-drugs', 'AssigndrugsController@create');

        Route::post('patients/{patient}/appointment/{appointment}/diagnoses/{diagnosis}/prescriptions/{prescription}/assign-drugs', 'AssigndrugsController@store');

        Route::get('patients/{patient}/appointment/{appointment}/diagnoses/{diagnosis}/prescriptions/{prescription}/othertreatments', 'OtherprescriptionController@create');
        Route::post('patients/{patient}/appointment/{appointment}/diagnoses/{diagnosis}/prescriptions/{prescription}/othertreatments', 'OtherprescriptionController@store');

        Route::post('patients/{patient}/appointment/{appointment}/diagnoses/{diagnosis}/prescriptions/{prescription}/prescription-costs', 'PrescriptionPaymentController@store');

        Route::post('/patients/{patient}/appointment/{appointment}/diagnoses/{diagnosis}/payments/save',

            'Diagnosis_paymentController@store');
        Route::post('/patients/{patient}/appointment/{appointment}/diagnoses/{diagnosis}/imagingpayments/save',

            'SaveImagingsController@store');
        Route::post('/patients/{patient}/appointment/{appointment}/diagnoses/{diagnosis}/otherprocedures/save',

            'SaveOtherProceduresController@store');
        Route::post('medicines/upload', 'DrugController@upload');
        Route::get('medicines', 'DrugController@index');
        Route::get('medicines/{medicine}/edit', 'DrugController@edit');
        Route::patch('medicines/{medicine}', 'DrugController@update');
        Route::get('medicines/create', 'DrugController@create');
        Route::post('medicines/new', 'DrugController@store');
        Route::delete('medicines/{medicine}', 'DrugController@destroy');

        Route::post('departments/upload', 'DepartmentController@upload')->name('departments.upload');
        Route::get('departments', 'DepartmentController@index');
        Route::get('departments/{department}/edit', 'DepartmentController@edit');
        Route::patch('departments/{departments}', 'DepartmentController@update');
        Route::get('departments/create', 'DepartmentController@create');
        Route::post('departments/new', 'DepartmentController@store');
        Route::delete('departments/{departments}', 'DepartmentController@destroy');

        Route::get('pharmace/patients', 'Pharmacist\PrescriptionController@index');
        Route::get('pharmace/patient/{patient}/prescriptions', 'pharmacist\PrescriptionController@show');
        Route::get('pharmace/patient/{patient}/prescription/{prescription}', 'pharmacist\PrescriptionController@showdrugs');
        Route::get('pharmace/patient/{patient}/prescription/{jprescription}/drug/{drug}/description', 'pharmacist\PrescriptionController@detailsdrugs');
        Route::get('/download/{filename}', 'DownloadController@index')->name('download');
        Route::post('/pharmace/patient/{patient}/prescription/{prescription}/drug/{drug}/assign','pharmacist\PrescriptionController@done');

        Route::get('lab-tests', 'LabAttendant\TestController@index');
        Route::get('patient/fromdoctor', 'LabAttendant\TestController@fromdoctor');


        Route::get('patient/{patient}/diagnoses', 'LabAttendant\TestController@showDiagnosis');

        Route::get('patients/{patient}/diagnoses/{diagnosis}/lab-tests', 'LabAttendant\TestController@show');

        Route::get('/patient/{patient}/diagnoses/{diagnosis}/lab-tests/{id}/conduct', 'LabAttendant\TestController@conduct');
        Route::post('patient/{patient}/diagnoses/{diagnosis}/lab-tests/{id}/conduct', 'LabAttendant\TestController@storeResult');
        Route::get('api/category-dropdown', 'ApiController@categoryDropDownData');
        Route::post('export', 'ExportExcelFileController@export');
        Route::get('notifications', 'NotificationController@unread');
        Route::delete('notifications/{notification}', 'NotificationController@destroy');

        Route::get('/hospitalservices','HospitalservicesController@index');
        Route::get('/hospitalservices/create','HospitalservicesController@create');
        Route::get('/hospitalservice/{service}/edit','HospitalservicesController@edit');
        Route::post('/hospitalservice/store','HospitalservicesController@store');
        Route::patch('hospitalservice/{service}','HospitalservicesController@update');
        Route::delete('hospitalservice/{service}','HospitalservicesController@destroy');
        Route::post('hospitalservice/upload','HospitalservicesController@upload');
    });


    Route::get('/imagingservices', 'ImagingServicesContoller@index');
    Route::get('/imagingservices/create', 'ImagingServicesContoller@createimaging');
    Route::get('imagingservices/{imagingservices}', 'ImagingServicesContoller@editimaging');
    Route::patch('imagingservices/{imagingservices}', 'ImagingServicesContoller@update');
    Route::post('/imagingservices/save', 'ImagingServicesContoller@storeimaging');
    Route::post('imagingservices/upload', 'ImagingServicesContoller@upload');
    Route::delete('imagingservices/{imagingservices}', 'ImagingServicesContoller@destroy');
    Route::get('imaging/patient/fromdoctor', 'ImagingServicesContoller@fromdoctor');
    Route::get('imaging/patient/{patient}/imaging', 'ImagingServicesContoller@show');
    Route::get('patient/{patient}/imagingservices/{imaging}/conduct', 'ImagingServicesContoller@conduct');
    Route::post('patient/{patient}/imaging/{imaging}/conduct', 'ImagingServicesContoller@storeResult');


    Route::get('reports/get-reports','ReportController@getJsonReports')->name('reports.get-reports');
    Route::get('reports/','ReportController@index')->name('reports.index');
    Route::post('reports/','ReportController@store')->name('reports.store');


    Route::group(['middleware' => 'auth'], function () {
        Route::get('appointments','AppointmentController@appointments' )->name('appointments');
        Route::get('get-appointments','AppointmentController@getJsonAppointments' )->name('get-appointments');

       Route::get('payments/mode/{mode}', function ($pmode) {
            $pmode = Paymentmode::findorfail($pmode);
            $paymentmodes = Paymentmode::all();
            $allPayments = App\Payment::latest()->where('mode_of_payment', $pmode->id)->get();

            $patientIds = $allPayments->pluck('patient_id');
            $patients = Patient::whereIn('id', $patientIds)->get();

            return view('payments.index', compact('allPayments', 'patients','paymentmodes'));
        })->name('payments.mode');

      //  Route::get('payments/mode/{mode}', 'PaymentController@paymentMode')->name('payments.mode');
    });


});
