# Hospital Management Information System

A platform for managing hospital records.

## Table of contents

- [Quick start](#quick-start)
- [Bugs and feature requests](#bugs-and-feature-requests)
- [Documentation](#documentation)
- [Contributing](#contributing)
- [Versioning](#versioning)
- [Creators](#creators)
- [Copyright and license](#copyright-and-license)

## Quick start

### Requirements

- [composer](https://getcomposer.org/)
- [laravel](https://www.laravel.com/docs/5.4/installation): >= 5.4
- [nodejs](https://nodejs.org/en/download/): >= 4.5

### Getting started

- Install composer dependencies:	`$ composer install`

- Install nodejs dependencies:	`$ npm install`

- Run npm run dev (or npm run dev): `$ npm run dev`

- Migrations & Seeding: `$ php artisan migrate --seed`

- Use artisan command or apache to serve the app:	`$ php artisan serve`

### Database Backups (For Windows only)

The database is backedup every day at 13:00. To backup database, follow these instructions

- Make a copy of file `cp crons.bat.example` to `crons.bat`

```
cp crons.bat.example crons.bat
```

- In `crons.bat` file, edit the file and put correct php and project paths

- Create a basic task in Windows that executes `crons.bat` file every day at 13:00
  [Read here on how to do it](https://www.digitalcitizen.life/how-create-task-basic-task-wizard)

- That batch script will make dump of the database and put it in storage/dumps/ directory. 
  From there you can configure any cloud storage service for example Dropbox or Google Drive to
  synchronize the dumps to the cloud.

### What's included

The directories and files, are Laravel application structure [laravel file directory structure](https://laravel.com/docs/5.4/structure#the-storage-directory).

The `resource` and `app` directory are continuously changing for the time being since there is no clear
design pattern to follow. But we are slowly refactoring the existing code base to follow the suggested [design patterns](http://www.laravelbestpractices.com/).

## Bugs and feature requests
Have a bug or a feature request?, [please open a new issue](https://bitbucket.org/mikaeel/hmis/issues/new).

## Documentation

> This is work in progress.


## Contributing

If you want to contribute, fork this project, make changes and open a pull request.

## Versioning

student-records is maintained under [the Semantic Versioning guidelines](http://semver.org/).
See [the Releases section of our GitHub project](https://github.com/joshuamabina/student-records/releases) for changelogs for each release version of student-records.

## Copyright and license

This product is proprietary.